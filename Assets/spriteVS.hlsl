uniform float4x4 g_transform_worldToView;
uniform float4x4 g_transform_viewToScreen;
uniform float4x4 g_transform_modelToWorld;


void main( in const float3 i_position_model : POSITION, in const float3 i_color : COLOR0,
	in const float2 i_uv : TEXCOORD0, in const float3 i_normal : NORMAL0,
	out float4 o_position_screen : POSITION, out float3 o_color : COLOR0,
	out float2 o_uv : TEXCOORD0, out float3 o_normal : NORMAL )
{
	{
		o_position_screen = float4(i_position_model, 1.0);
	}

	{
		o_color = i_color;
	}

	{
		o_uv = i_uv;
	}

	{
		o_normal = i_normal;
	}
}
