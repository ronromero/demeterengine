return
{
    constants =
	{
		g_colorModifier = { 1.0, 1.0, 1.0 },
	},
	shaders = 
	{
		vertexShader = "data/vertexShader.shd",
		fragmentShader = "data/fragmentShader.shd",
	},
}