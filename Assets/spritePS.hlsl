texture2D g_color_texture;
sampler2D g_color_sampler;

void main( in const float2 i_uv : TEXCOORD0, in const float3 i_color_perVertex : COLOR0,
	in const float3 i_normal : NORMAL0, out float4 o_color : COLOR0 )
{
	float4 color_sample = tex2D( g_color_sampler, i_uv ).rgba;
	o_color = color_sample * float4( i_color_perVertex , 1.0 );
}
