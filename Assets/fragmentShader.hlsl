uniform float3 g_ambient_color = { 0.2, 0.2, 0.2 };
uniform float3 g_diffuse_color = { 0.0, 0.0, 0.2 };
uniform float3 g_diffuse_dir = { 0.0, -1.0, 0.0 };

uniform float3 g_color_perMaterial = { 1.0, 1.0, 1.0 };

texture2D g_color_texture;
sampler2D g_color_sampler;


void main( in const float2 i_uv : TEXCOORD0, in const float3 i_color_perVertex : COLOR0,
	in const float3 i_normal : NORMAL0, out float4 o_color : COLOR0 )
{
	float3 color_sample = tex2D( g_color_sampler, i_uv ).rgb;

	float3 ambient = color_sample * g_color_perMaterial * i_color_perVertex;
	float3 normal = normalize( i_normal );
	float3 diffuse = saturate( dot( normal, -g_diffuse_dir ) );
	float3 diffuse_lighting = diffuse * g_diffuse_color;

	float3 color_lit = ambient * ( diffuse_lighting + g_ambient_color );

	o_color = float4( color_lit, 1.0 );
}
