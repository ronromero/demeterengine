uniform float4x4 g_transform_worldToView;
uniform float4x4 g_transform_viewToScreen;
uniform float4x4 g_transform_modelToWorld;


void main( in const float3 i_position_model : POSITION, in const float3 i_color : COLOR0,
	in const float2 i_uv : TEXCOORD0, in const float3 i_normal : NORMAL0,
	out float4 o_position_screen : POSITION, out float3 o_color : COLOR0,
	out float2 o_uv : TEXCOORD0, out float3 o_normal : NORMAL )
{
	{
		float4 position_world = mul( float4(i_position_model, 1.0), g_transform_modelToWorld );
		float4 position_view = mul( position_world, g_transform_worldToView );
		o_position_screen = mul( position_view, g_transform_viewToScreen );
	}

	{
		o_color = i_color;
	}

	{
		o_uv = i_uv;
	}

	{
		float3x3 g_rotation_modelToWorld =
		{
			g_transform_modelToWorld[ 0 ].xyz,
			g_transform_modelToWorld[ 1 ].xyz,
			g_transform_modelToWorld[ 2 ].xyz
		};
		o_normal = mul( i_normal, g_rotation_modelToWorld );
	}
}
