uniform float4x4 g_transform_worldToView;
uniform float4x4 g_transform_viewToScreen;

void main( in const float3 i_position_world : POSITION, in const float3 i_color : COLOR0,
	out float4 o_position_screen : POSITION, out float3 o_color : COLOR0 )
{
	float4 position_world = float4( i_position_world, 1.0 );
	float4 position_view = mul( position_world, g_transform_worldToView );
	o_position_screen = mul( position_view, g_transform_viewToScreen );
	o_color = i_color;
}