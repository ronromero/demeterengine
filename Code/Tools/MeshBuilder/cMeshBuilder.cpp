#include "cMeshBuilder.h"

#include <Parrhasius/Exports/Mesh.h>
#include <BuilderHelper/Exports/json.h>
#include <LuaLib/Exports/Includes.h>

#include <sstream>
#include <iostream>
#include <fstream>

namespace
{
	bool JsonToVertexBuffer( json::Array vertexBuffer, Parrhasius::_MeshData *oMesh )
	{
		if( oMesh->numVertices != vertexBuffer.size() )
		{
			std::cerr << "Expected " << oMesh->numVertices << " vertices instead of "
				<< vertexBuffer.size() << std::endl;
			return false;
		}
		oMesh->vertexBuffer = new Parrhasius::sVertex[ oMesh->numVertices ];
		for( unsigned int i = 0; i < vertexBuffer.size(); i++ )
		{
			oMesh->vertexBuffer[ i ].x = vertexBuffer[ i ][ "x" ].ToFloat();
			oMesh->vertexBuffer[ i ].y = vertexBuffer[ i ][ "y" ].ToFloat();
			oMesh->vertexBuffer[ i ].z = vertexBuffer[ i ][ "z" ].ToFloat();
			int r = vertexBuffer[ i ][ "r" ].ToInt();
			int g = vertexBuffer[ i ][ "g" ].ToInt();
			int b = vertexBuffer[ i ][ "b" ].ToInt();
			oMesh->vertexBuffer[ i ].color = D3DCOLOR_XRGB( r, g, b );
			oMesh->vertexBuffer[ i ].u = vertexBuffer[ i ][ "u" ].ToFloat();
			oMesh->vertexBuffer[ i ].v = vertexBuffer[ i ][ "v" ].ToFloat();
			oMesh->vertexBuffer[ i ].nx = vertexBuffer[ i ][ "nx" ].ToFloat();
			oMesh->vertexBuffer[ i ].ny = vertexBuffer[ i ][ "ny" ].ToFloat();
			oMesh->vertexBuffer[ i ].nz = vertexBuffer[ i ][ "nz" ].ToFloat();
		}
		return true;
	}

	bool JsonToIndexBuffer( json::Array indexBuffer, Parrhasius::_MeshData *oMesh )
	{
		if( oMesh->numIndices != indexBuffer.size() )
		{
			std::cerr << "Expected " << oMesh->numIndices << " vertices instead of "
				<< indexBuffer.size() << std::endl;
			return false;
		}
		oMesh->indexBuffer = new uint32_t[ oMesh->numIndices ];
		for( unsigned int i = 0; i < indexBuffer.size(); i++ )
		{
			oMesh->indexBuffer[ i ] = indexBuffer[ i ].ToInt();
		}
		return true;
	}
}

bool eae6320::cMeshBuilder::Build( const std::vector<const std::string>& iArguments )
{
	bool wereThereErrors = false;

	std::ifstream fs;
	fs.open( m_path_source, std::fstream::in );
	if( !fs )
	{
		wereThereErrors = true;
		std::cerr << "Could not open " << m_path_source << std::endl;
	}
	else
	{
		int bufferSize = 0;
		while( fs.good() )
		{
			char c = fs.get();
			bufferSize++;
		}
		fs.clear();
		fs.seekg( 0, std::ios::beg );
		char* buffer = new char[ bufferSize ];
		fs.read( buffer, bufferSize );
		fs.close();

		std::string data( buffer, bufferSize - 1 );
		delete buffer;

		json::Value readData = json::Deserialize( data );
		if( readData.GetType() == json::NULLVal )
		{
			std::cerr << "file is not a valid JSON file " << m_path_source << std::endl;
			wereThereErrors = true;
		}
		else
		{
			Parrhasius::_MeshData mesh;
			if( readData.HasKey( "numVertices" ) )
			{
				mesh.numVertices = readData[ "numVertices" ].ToInt();
			}
			else
			{
				wereThereErrors = true;
			}
			if( readData.HasKey( "numIndices" ) )
			{
				mesh.numIndices = readData[ "numIndices" ].ToInt();
			}
			else
			{
				wereThereErrors = true;
				std::cerr << "Expected key " << "numIndices" << " in " << m_path_source << std::endl;
			}
			if( !readData.HasKey( "vertices" ) ||
				!JsonToVertexBuffer( readData[ "vertices" ].ToArray(), &mesh ) )
			{
				wereThereErrors = true;
				std::cerr << "Error parsing vertices in " << m_path_source << std::endl;
			}
			if( !readData.HasKey( "indices" ) ||
				!JsonToIndexBuffer( readData[ "indices" ].ToArray(), &mesh ) )
			{
				wereThereErrors = true;
				std::cerr << "Error parsing indices in " << m_path_source << std::endl;
			}
			if( !mesh.Write( m_path_target ) )
			{
				wereThereErrors = true;
			}
		}
	}

	return !wereThereErrors;
}