#ifndef _MESHBUILDER_H_
#define _MESHBUILDER_H_

#include <BuilderHelper/Exports/cbBuilder.h>

namespace eae6320
{
	class cMeshBuilder : public cbBuilder
	{
	public:
		virtual bool Build( const std::vector<const std::string>& );
	};
}

#endif //_MESHBUILDER_H_