#ifndef EAE6320_CGENERICBUILDER_H
#define EAE6320_CGENERICBUILDER_H

#include <BuilderHelper/Exports/cbBuilder.h>

namespace eae6320
{
	class cGenericBuilder : public cbBuilder
	{
	public:
		virtual bool Build( const std::vector<const std::string>& );
	};
}

#endif	// EAE6320_CGENERICBUILDER_H
