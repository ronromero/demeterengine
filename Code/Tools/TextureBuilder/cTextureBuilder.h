#ifndef _TEXTUREBUILDER_H
#define _TEXTUREBUILDER_H

#include <BuilderHelper/Exports/cbBuilder.h>

namespace eae6320
{
	class cTextureBuilder : public cbBuilder
	{
	public:
		virtual bool Build( const std::vector<const std::string>& );
	};

}
#endif
