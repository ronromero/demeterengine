#ifndef EAE6320_ASSETBUILDER_H
#define EAE6320_ASSETBUILDER_H

#include <cstdint>
#include <string>

bool BuildAssets( const char* i_relativePath );

#endif	// EAE6320_ASSETBUILDER_H
