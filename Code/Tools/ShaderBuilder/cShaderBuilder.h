#ifndef _SHADERBUILDER_H
#define _SHADERBUILDER_H

#include <BuilderHelper/Exports/cbBuilder.h>

namespace eae6320
{
	class cShaderBuilder : public cbBuilder
	{
	public:
		virtual bool Build( const std::vector<const std::string>& );
	};
}

#endif