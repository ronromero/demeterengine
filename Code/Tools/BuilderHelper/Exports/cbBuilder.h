#ifndef _BUILDERHELPER_CBBUILDER_H_
#define _BUILDERHELPER_CBBUILDER_H_

#include <cstdlib>
#include <string>
#include <vector>

#include "UtilityFunctions.h"

namespace eae6320
{
	template<class builder_t>
	int Build( char** i_arguments, const unsigned int i_argumentCount )
	{
		builder_t builder;
		return builder.ParseCommandArgumentsAndBuild( i_arguments, i_argumentCount ) ? EXIT_SUCCESS : EXIT_FAILURE;
	}

	class cbBuilder
	{
	public:
		bool ParseCommandArgumentsAndBuild( char** i_arguments, const unsigned int i_argumentCount );
		virtual bool Build( const std::vector<const std::string>& i_optionalArguments ) = 0;
		cbBuilder();

	protected:
		const char* m_path_source;
		const char* m_path_target;
	};
}

#endif	// _BUILDERHELPER_CBBUILDER_H_
