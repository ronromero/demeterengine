#ifndef _MONSTERCHASE_LEVEL_H_
#define _MONSTERCHASE_LEVEL_H_

namespace MonsterChase
{
	enum ETile
	{
		EMPTY,
		PELLET,
		POWER_PELLET,
		WALL,
		PACMAN_SPAWN,
		GHOST_SPAWN
	};

	class Level
	{
	public:
		Level( const int iSize );
		~Level( void );

	//private:
		ETile *_tiles;
	};
}
#endif