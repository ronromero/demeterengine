#define _TRACK_MEMORY_LEAKS_
#define _PROFILER_ENABLED

#include <Windows.h>
#include <Demeter/Exports/DemeterEngine.h>
#include <Demeter/Exports/DEngineSettings.h>
#include "EAE6320/EAE6320Scene.h"
#include "Game.h"

#if defined(_DEBUG) && defined(_TRACK_MEMORY_LEAKS_)
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define MEMDBG_START _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF )
#define	MEMDBG_END _CrtDumpMemoryLeaks()
#else
#define MEMDBG_START void(0)
#define MEMDBG_END void(0)
#endif

#if defined(_PROFILE) && defined(_PROFILER_ENABLED)
#include <Demeter/Exports/DProfiler.h>
#include <Demeter/Exports/ScopedTimer.h>
#define PRINT_PROFILE_RESULTS PROFILE_PRINT_RESULTS()
#else
#define PRINT_PROFILE_RESULTS void(0)
#endif

int APIENTRY WinMain( HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	PSTR lpCmdLine,
	int nCmdShow )
{
	MEMDBG_START;

	Demeter::DEngineSettings settings;
	Demeter::LoadDEngineSettings( hInstance, lpCmdLine, nCmdShow, settings );
	Demeter::DEngine engine( settings );
	if( engine.Initialize() )
	{
		engine.AddScene( std::shared_ptr<Demeter::DScene>( new MonsterChase::Game() ) );
		//engine.AddScene( std::shared_ptr<Demeter::DScene>( new MonsterChase::EAE6320Scene() ) );
		engine.Run();
	}
	else
	{
		//TODO: show error message
	}
	PRINT_PROFILE_RESULTS;
	return engine.ShutDown();
	MEMDBG_END;
}