#pragma once

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DVector.h>
#include "Level.h"

namespace MonsterChase
{
	class MCGameObject : public Demeter::DGameObject
	{
	public:
		ETile McType;
		unsigned int Points;

		MCGameObject( Demeter::DVec3f iPos, Demeter::DVec3f iScale, Demeter::DVec3f iRot,
			ETile type, unsigned int iPoints );
		~MCGameObject( void );
	};

}