#ifndef _MONSTERCHASE_PLAYERCONTROLLER_H_
#define _MONSTERCHASE_PLAYERCONTROLLER_H_

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DGameComponent.h>

namespace MonsterChase
{
	class PlayerController : public Demeter::DController
	{
	public:
		PlayerController( void );
		~PlayerController( void );
		void HandleInput( void );

	private:
		PlayerController( const PlayerController& );
		PlayerController& operator=(const PlayerController&);
	};
}

#endif //_MONSTERCHASE_PLAYERCONTROLLER_H_