#ifndef _MONSTERCHASE_CAMERACONTROLLER_H_
#define _MONSTERCHASE_CAMERACONTROLLER_H_

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DGameComponent.h>
#include <Demeter/Exports/Camera.h>

namespace MonsterChase
{
	class CameraController : public Demeter::DController
	{
	public:
		CameraController( void );
		~CameraController( void );
		void HandleInput( void );

	private:
		//Demeter::Camera *_camera;

		CameraController( const CameraController& );
		CameraController& operator=(const CameraController&);
	};
}

#endif //_MONSTERCHASE_CAMERACONTROLLER_H_