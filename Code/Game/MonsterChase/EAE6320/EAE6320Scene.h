#ifndef _MONSTERCHASE_EAE6320_H_
#define _MONSTERCHASE_EAE6320_H_

#include <Demeter/Exports/DScene.h>
#include <Demeter/Exports/DContentManager.h>

namespace MonsterChase
{
	class EAE6320Scene : public Demeter::DScene
	{
	public:
		EAE6320Scene( void );
		~EAE6320Scene( void );

		void Load( IDirect3DDevice9 *iDevice,
			std::shared_ptr<Demeter::DContentManager> iContentManager );
		void HandleInput( void );
		void Update( void );
		void Cleanup( void );

	private:

		EAE6320Scene( const EAE6320Scene& );
		EAE6320Scene& operator=(const EAE6320Scene&);
	};
}

#endif //_MONSTERCHASE_EAE6320_H_