#include "CameraController.h"
#include <Demeter/Exports/UserInput.h>
#include <Demeter/Exports/Time.h>

using namespace Demeter;

namespace MonsterChase
{
	CameraController::CameraController( void )
		: DController( NULL )
	{}

	CameraController::~CameraController( void )
	{}

	void CameraController::HandleInput( void )
	{
		if( GameObject == NULL ) return;
		DVec3f cameraAngleOffset( 0.0f, 0.0f, 0.0f );
		if( eae6320::UserInput::IsKeyPressed( 'U' ) )
		{ // yaw
			cameraAngleOffset.X() -= 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'O' ) )
		{
			cameraAngleOffset.X() += 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'Y' ) )
		{ // pitch
			cameraAngleOffset.Y() -= 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'H' ) )
		{
			cameraAngleOffset.Y() += 1.0f;
		}
		float camAngleUnitsPerSecond = 3.14159265f / 8.0f;
		cameraAngleOffset *= (camAngleUnitsPerSecond * eae6320::Time::GetSecondsElapsedThisFrame());

		GameObject->_transform->Rotation += cameraAngleOffset;

		D3DXVECTOR3 cameraOffset( 0.0f, 0.0f, 0.0f );
		if( eae6320::UserInput::IsKeyPressed( 'I' ) )
		{
			cameraOffset.z += 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'K' ) )
		{
			cameraOffset.z -= 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'J' ) )
		{
			cameraOffset.x -= 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'L' ) )
		{
			cameraOffset.x += 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( 'P' ) )
		{
			cameraOffset.y += 1.0f;
		}
		if( eae6320::UserInput::IsKeyPressed( VK_OEM_1 ) )
		{
			cameraOffset.y -= 1.0f;
		}
		float camUnitsPerSecond = 1.0f;
		cameraOffset *= (camUnitsPerSecond * eae6320::Time::GetSecondsElapsedThisFrame());

		D3DXVECTOR4 trans;
		D3DXMATRIX rot;
		D3DXMatrixRotationYawPitchRoll( &rot, GameObject->_transform->Rotation.X(),
			GameObject->_transform->Rotation.Y(), GameObject->_transform->Rotation.Z() );
		D3DXVec3Transform( &trans, &cameraOffset, &rot );

		GameObject->_transform->Position += DVec3f( trans.x, trans.y, trans.z );
	}
}