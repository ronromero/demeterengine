#include "PlayerController.h"
#include <Demeter/Exports/UserInput.h>
#include <Demeter/Exports/Time.h>

using namespace Demeter;

namespace MonsterChase
{
	PlayerController::PlayerController( void )
		: DController( NULL )
	{}

	PlayerController::~PlayerController( void )
	{}

	void PlayerController::HandleInput( void )
	{
		if( GameObject == NULL || GameObject->_rigidBody == NULL ) return;

		DVec3f offset( 0.0f, 0.0f, 0.0f );
		{
			if( eae6320::UserInput::IsKeyPressed( 'W' ) )
			{
				offset.Z() += 1.0f;
			}
			if( eae6320::UserInput::IsKeyPressed( 'S' ) )
			{
				offset.Z() -= 1.0f;
			}
			if( eae6320::UserInput::IsKeyPressed( 'A' ) )
			{
				offset.X() -= 1.0f;
			}
			if( eae6320::UserInput::IsKeyPressed( 'D' ) )
			{
				offset.X() += 1.0f;
			}
			const float unitsPerSecond = 40;
			offset *= (unitsPerSecond * eae6320::Time::GetSecondsElapsedThisFrame());
			GameObject->_rigidBody->Force = offset;
		}
	}
}