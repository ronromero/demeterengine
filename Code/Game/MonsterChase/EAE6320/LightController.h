#ifndef _MONSTERCHASE_LIGHTCONTROLLER_H_
#define _MONSTERCHASE_LIGHTCONTROLLER_H_

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DGameComponent.h>
#include <Demeter/Exports/Light.h>

namespace MonsterChase
{
	class LightController : public Demeter::DController
	{
	public:
		LightController( void );
		~LightController( void );
		void HandleInput( void );

	private:
		LightController( const LightController& );
		LightController& operator=(const LightController&);
	};
}
#endif // _MONSTERCHASE_LIGHTCONTROLLER_H_