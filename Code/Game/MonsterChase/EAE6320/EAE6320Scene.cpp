#include "EAE6320Scene.h"
#include "CameraController.h"
#include "PlayerController.h"
#include "LightController.h"

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DRenderable.h>
#include <Demeter/Exports/DContentManager.h>
#include <Demeter/Exports/DEngineSettings.h>
#include <Demeter/Exports/Time.h>
#include <Parrhasius/Exports/PEngineSettings.h>
#include <Parrhasius/Exports/Sprite.h>
#include <Parrhasius/Exports/Rect.h>

using namespace Demeter;

namespace MonsterChase
{
	namespace
	{
		Parrhasius::Sprite *s_sprite;
		Parrhasius::Rect s_runningAnim[ 5 ];
		float s_animTimer = 0;
	}

	EAE6320Scene::EAE6320Scene( void )
	{
		float oneOverSpritesPerSheet = 1.0f / 5.0f;
		for( int i = 0; i < 5; ++i )
		{
			s_runningAnim[ i ] = Parrhasius::Rect( i * oneOverSpritesPerSheet, 0, oneOverSpritesPerSheet, 1.0f );
		}
	}

	EAE6320Scene::~EAE6320Scene( void )
	{}

	void EAE6320Scene::Load( IDirect3DDevice9 *iDevice,
		std::shared_ptr<DContentManager> iContentMgr )
	{
		{// add objects
			{
				std::shared_ptr<DGameObject> obj( DGameObject::Instantiate(
					DVec3f::Zero, DVec3f::One, DVec3f::Zero ) );
				obj->AddComponent( new DRenderable( "data/planeMaterial.matl", "data/plane.mesh", "data/green_fire_1.dds" ) );
				AddActor( obj );
			}
			{
				std::shared_ptr<DGameObject> obj( DGameObject::Instantiate(
					DVec3f( 0, 1, 0 ), DVec3f::One * 0.1f, DVec3f::Zero ) );
				obj->AddComponent( new DRenderable( "data/cubeMaterial.matl", "data/mcube.mesh", "data/MiLxX99ia.dds" ) );
				obj->AddComponent( new MonsterChase::PlayerController() );
				AddActor( obj );
			}
			{
				std::shared_ptr<DGameObject> obj( DGameObject::Instantiate(
					DVec3f::One, DVec3f::One * 0.5f, DVec3f::Zero ) );
				obj->AddComponent( new DRenderable( "data/pyramidMaterial.matl", "data/pyramid.mesh", "data/illuminati_eye.dds" ) );
				AddActor( obj );
			}
			{
				std::shared_ptr<DGameObject> obj( DGameObject::Instantiate(
					DVec3f( -1, 1, 1 ), DVec3f::One * 0.5f, DVec3f::Zero ) );
				obj->AddComponent( new DRenderable( "data/cubeMaterial.matl", "data/pyramid.mesh", "data/MiLxX99ia.dds" ) );
				AddActor( obj );
			}
		}
		{ //lights
			float *aColor = new float[ 3 ] { 0.016f, 0.016f, 0.016f };
			float *dColor = new float[ 3 ] { 1.0f, 1.0f, 1.0f };
			DVec3f dDir( 0.0f, -1.0f, 0.0f );
			Light *obj = new Light( aColor, dColor, dDir );
			obj->AddComponent( new MonsterChase::LightController() );
			AddLight( obj );
			delete aColor;
			delete dColor;
		}
		{// add camera
			float fovy = D3DXToRadian( 60.0f );
			float aspect = 16.0f / 9.0f;
			float zNear = 0.01f;
			float zFar = 1000;
			Camera *obj = new Camera(
				DVec3f( 0.2f, 0.5f, -1 ), DVec3f( 0, DMathf::PI / 8.0f, 0 ),
				fovy, aspect, zNear, zFar );
			obj->AddComponent( new MonsterChase::CameraController() );
			AddCamera( obj );
		}
		{ // add sprites
			{
				float width = 64;
				float height = 64;
				float nWidth = (width / iContentMgr->_settings->RenderSettings->Width);
				float nHeight = (height / iContentMgr->_settings->RenderSettings->Height);

				s_sprite = new Parrhasius::Sprite( "data/CampFireFinished.dds",
					Parrhasius::Rect( nWidth - 1, nHeight - 1, nWidth, nHeight ) );
				iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( s_sprite ) );
			}
			{
				float width = 111;
				float height = 111;
				float nWidth = (width / iContentMgr->_settings->RenderSettings->Width);
				float nHeight = (height / iContentMgr->_settings->RenderSettings->Height);

				Parrhasius::Sprite *sprite2 = new Parrhasius::Sprite( "data/dragon.dds",
					Parrhasius::Rect( 1 - nWidth * 0.5f, 1 - nHeight * 0.5f, nWidth, nHeight ) );
				sprite2->SetColor( D3DCOLOR_ARGB( 255, 204, 0, 0 ) );
				iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( sprite2 ) );
			}
		}
		DScene::Load( iDevice, iContentMgr );
	}

	void EAE6320Scene::HandleInput( void )
	{
		DScene::HandleInput();
	}

	void EAE6320Scene::Update( void )
	{
		s_animTimer += eae6320::Time::GetSecondsElapsedThisFrame() * 6;
		int idx = ((int)s_animTimer % 5);
		s_sprite->SetTexCoords( &s_runningAnim[ idx ] );
		if( s_animTimer > 5 )
		{
			s_animTimer -= 5;
		}
	}

	void EAE6320Scene::Cleanup( void )
	{

	}
}