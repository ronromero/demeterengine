#pragma once

#include <Parrhasius/Exports/Sprite.h>
#include <Parrhasius/Exports/Rect.h>
#include <Demeter/Exports/DContentManager.h>

namespace MonsterChase
{
	class Scoreboard
	{
	public:
		Scoreboard( const char* iPath );
		~Scoreboard( void );

		void Load( std::shared_ptr<Demeter::DContentManager> iContentMgr );
		void UpdateScore( const int iAmt );
		void Reset( void );
		const unsigned int Score( void ) const;

	private:
		unsigned int _score;
		const char* _path;
		Parrhasius::Sprite *_ones;
		Parrhasius::Sprite *_tens;
		Parrhasius::Sprite *_hundys;
		Parrhasius::Sprite *_thous;
		Parrhasius::Rect _numbers[ 10 ];
	};

}