#include "Game.h"
#include "Level.h"
#include "EAE6320/PlayerController.h"
#include "MCGameObject.h"
#include "Scoreboard.h"

#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DRenderable.h>
#include <Demeter/Exports/DEngineSettings.h>
#include <Demeter/Exports/Light.h>
#include <Demeter/Exports/Camera.h>
#include <Demeter/Exports/DVector.h>
#include <Demeter/Exports/HashedString.h>
#include <Demeter/Exports/UserInput.h>
#include <Demeter/Exports/Time.h>

#include <Parrhasius/Exports/PEngineSettings.h>
#include <Parrhasius/Exports/Rect.h>
#include <Parrhasius/Exports/Sprite.h>

using namespace Demeter;

namespace MonsterChase
{
	Game::Game( void )
	{
		_scoreboard = new Scoreboard( "data/font_number_sprite.dds" );

		_level = new Level( 81 ); //TODO: load from file
		_level->_tiles[ 0 ] = ETile::WALL;
		_level->_tiles[ 1 ] = ETile::WALL;
		_level->_tiles[ 2 ] = ETile::WALL;
		_level->_tiles[ 3 ] = ETile::WALL;
		_level->_tiles[ 4 ] = ETile::WALL;
		_level->_tiles[ 5 ] = ETile::WALL;
		_level->_tiles[ 6 ] = ETile::WALL;
		_level->_tiles[ 7 ] = ETile::WALL;
		_level->_tiles[ 8 ] = ETile::WALL;

		_level->_tiles[ 9 ] = ETile::WALL;
		_level->_tiles[ 10 ] = ETile::POWER_PELLET;
		_level->_tiles[ 11 ] = ETile::PELLET;
		_level->_tiles[ 12 ] = ETile::PELLET;
		_level->_tiles[ 13 ] = ETile::PELLET;
		_level->_tiles[ 14 ] = ETile::PELLET;
		_level->_tiles[ 15 ] = ETile::PELLET;
		_level->_tiles[ 16 ] = ETile::POWER_PELLET;
		_level->_tiles[ 17 ] = ETile::WALL;

		_level->_tiles[ 18 ] = ETile::WALL;
		_level->_tiles[ 19 ] = ETile::PELLET;
		_level->_tiles[ 20 ] = ETile::WALL;
		_level->_tiles[ 21 ] = ETile::WALL;
		_level->_tiles[ 22 ] = ETile::PELLET;
		_level->_tiles[ 23 ] = ETile::WALL;
		_level->_tiles[ 24 ] = ETile::WALL;
		_level->_tiles[ 25 ] = ETile::PELLET;
		_level->_tiles[ 26 ] = ETile::WALL;

		_level->_tiles[ 27 ] = ETile::EMPTY;
		_level->_tiles[ 28 ] = ETile::PELLET;
		_level->_tiles[ 29 ] = ETile::WALL;
		_level->_tiles[ 30 ] = ETile::PELLET;
		_level->_tiles[ 31 ] = ETile::PELLET;
		_level->_tiles[ 32 ] = ETile::PELLET;
		_level->_tiles[ 33 ] = ETile::WALL;
		_level->_tiles[ 34 ] = ETile::PELLET;
		_level->_tiles[ 35 ] = ETile::EMPTY;

		_level->_tiles[ 36 ] = ETile::WALL;
		_level->_tiles[ 37 ] = ETile::PELLET;
		_level->_tiles[ 38 ] = ETile::PELLET;
		_level->_tiles[ 39 ] = ETile::PELLET;
		_level->_tiles[ 40 ] = ETile::GHOST_SPAWN;
		_level->_tiles[ 41 ] = ETile::PELLET;
		_level->_tiles[ 42 ] = ETile::PELLET;
		_level->_tiles[ 43 ] = ETile::PELLET;
		_level->_tiles[ 44 ] = ETile::WALL;

		_level->_tiles[ 45 ] = ETile::EMPTY;
		_level->_tiles[ 46 ] = ETile::PELLET;
		_level->_tiles[ 47 ] = ETile::WALL;
		_level->_tiles[ 48 ] = ETile::PELLET;
		_level->_tiles[ 49 ] = ETile::PELLET;
		_level->_tiles[ 50 ] = ETile::PELLET;
		_level->_tiles[ 51 ] = ETile::WALL;
		_level->_tiles[ 52 ] = ETile::PELLET;
		_level->_tiles[ 53 ] = ETile::EMPTY;

		_level->_tiles[ 54 ] = ETile::WALL;
		_level->_tiles[ 55 ] = ETile::PELLET;
		_level->_tiles[ 56 ] = ETile::WALL;
		_level->_tiles[ 57 ] = ETile::WALL;
		_level->_tiles[ 58 ] = ETile::PACMAN_SPAWN;
		_level->_tiles[ 59 ] = ETile::WALL;
		_level->_tiles[ 60 ] = ETile::WALL;
		_level->_tiles[ 61 ] = ETile::PELLET;
		_level->_tiles[ 62 ] = ETile::WALL;

		_level->_tiles[ 63 ] = ETile::WALL;
		_level->_tiles[ 64 ] = ETile::POWER_PELLET;
		_level->_tiles[ 65 ] = ETile::PELLET;
		_level->_tiles[ 66 ] = ETile::PELLET;
		_level->_tiles[ 67 ] = ETile::PELLET;
		_level->_tiles[ 68 ] = ETile::PELLET;
		_level->_tiles[ 69 ] = ETile::PELLET;
		_level->_tiles[ 70 ] = ETile::POWER_PELLET;
		_level->_tiles[ 71 ] = ETile::WALL;

		_level->_tiles[ 72 ] = ETile::WALL;
		_level->_tiles[ 73 ] = ETile::WALL;
		_level->_tiles[ 74 ] = ETile::WALL;
		_level->_tiles[ 75 ] = ETile::WALL;
		_level->_tiles[ 76 ] = ETile::WALL;
		_level->_tiles[ 77 ] = ETile::WALL;
		_level->_tiles[ 78 ] = ETile::WALL;
		_level->_tiles[ 79 ] = ETile::WALL;
		_level->_tiles[ 80 ] = ETile::WALL;
	}

	Game::~Game( void )
	{
		delete _level;
		delete _scoreboard;
	}

	void Game::Load( IDirect3DDevice9 *iDevice,
		std::shared_ptr<Demeter::DContentManager> iContentMgr )
	{
		_contentMgr = iContentMgr;
		//std::shared_ptr<Parrhasius::Texture> t1 = iContentMgr->LoadTexture( iDevice, "data / MiLxX99ia.dds" );
		//std::shared_ptr<Parrhasius::Texture> t2 = iContentMgr->LoadTexture( iDevice, "data/illuminati_eye.dds" );
		//std::shared_ptr<Parrhasius::Texture> t3 = iContentMgr->LoadTexture( iDevice, "data/green_fire_1.dds" );
		//std::shared_ptr<Parrhasius::Texture> t4 = iContentMgr->LoadTexture( iDevice, "data/dragon.dds" );
		//std::shared_ptr<Parrhasius::Texture> t5 = iContentMgr->LoadTexture( iDevice, "data/font_number_sprite.dds" );

		//std::shared_ptr<Parrhasius::Material> m1 = iContentMgr->LoadMaterial( iDevice, "data/pyramidMaterial.matl" );
		//std::shared_ptr<Parrhasius::Material> m2 = iContentMgr->LoadMaterial( iDevice, "data/cubeMaterial.matl" );
		//std::shared_ptr<Parrhasius::Material> m3 = iContentMgr->LoadMaterial( iDevice, "data/planeMaterial.matl" );

		//std::shared_ptr<Parrhasius::Mesh> o1 = iContentMgr->LoadMesh( iDevice, "data/mcube.mesh" );
		//std::shared_ptr<Parrhasius::Mesh> o2 = iContentMgr->LoadMesh( iDevice, "data/pyramid.mesh" );
		//std::shared_ptr<Parrhasius::Mesh> o3 = iContentMgr->LoadMesh( iDevice, "data/plane.mesh" );

		{
			float col = 0;
			float row = 0;
			for( int i = 0; i < 81; ++i )
			{
				row = -(float)(i / 9);	// rows
				col = (float)(i % 9);	// cols
				if( _level->_tiles[ i ] == ETile::WALL )
				{
					std::shared_ptr<DGameObject> obj( new MCGameObject( DVec3f( col, 0.5f, row ),
						DVec3f::One, DVec3f::Zero, ETile::WALL, 0 ) );
					obj->AddComponent( new DRenderable( "data/cubeMaterial.matl", "data/mcube.mesh", "data/dragon.dds" ) );
					obj->AddComponent( new DSphereCollider( DVec3f::Zero, 0.5f, 1, 12 ) ); // wall(1), pellet(2), ghost(4), pacman(8)
					obj->Register( "OnCollision", this );
					AddActor( obj );
				}
				else if( _level->_tiles[ i ] == ETile::PELLET )
				{
					std::shared_ptr<DGameObject> obj( new MCGameObject( DVec3f( col, 0.5f, row ),
						DVec3f::One * 0.2f, DVec3f::Zero, ETile::PELLET, 10 ) );
					obj->AddComponent( new DRenderable( "data/pyramidMaterial.matl", "data/pyramid.mesh", "data/illuminati_eye.dds" ) );
					obj->AddComponent( new DSphereCollider( DVec3f::Zero, 0.1f, 2, 8 ) ); // wall(1), pellet(2), ghost(4), pacman(8)
					obj->Register( "OnCollision", this );
					AddActor( obj );
				}
				else if( _level->_tiles[ i ] == ETile::POWER_PELLET )
				{
					std::shared_ptr<DGameObject> obj( new MCGameObject( DVec3f( col, 0.5f, row ),
						DVec3f::One * 0.4f, DVec3f::Zero, ETile::POWER_PELLET, 50 ) );
					obj->AddComponent( new DRenderable( "data/pyramidMaterial.matl", "data/mcube.mesh", "data/illuminati_eye.dds" ) );
					obj->AddComponent( new DSphereCollider( DVec3f::Zero, 0.2f, 2, 8 ) );// wall(1), pellet(2), ghost(4), pacman(8)
					obj->Register( "OnCollision", this );
					AddActor( obj );
				}
				else if( _level->_tiles[ i ] == ETile::PACMAN_SPAWN )
				{
					_startPos = DVec3f( col, 0.5f, row );
					_pacman = new MCGameObject( _startPos,
						DVec3f::One * 0.6f, DVec3f::Zero, ETile::PACMAN_SPAWN, 0 );
					std::shared_ptr<DGameObject> obj( _pacman );
					obj->AddComponent( new DRenderable( "data/cubeMaterial.matl", "data/mcube.mesh", "data/MiLxX99ia.dds" ) );
					obj->AddComponent( new PlayerController() );
					obj->AddComponent( new DSphereCollider( DVec3f::Zero, 0.3f, 8, 7 ) ); // wall(1), pellet(2), ghost(4), pacman(8)
					obj->AddComponent( new DRigidBody( 1 ) );
					AddActor( obj );
				}
				else if( _level->_tiles[ i ] == ETile::GHOST_SPAWN )
				{
					std::shared_ptr<DGameObject> obj( new MCGameObject( DVec3f( col, 0.5f, row ),
						DVec3f::One, DVec3f::Zero, ETile::WALL, 0 ) );
					obj->AddComponent( new DRenderable( "data/cubeMaterial.matl", "data/mcube.mesh", "data/dragon.dds" ) );
					obj->AddComponent( new DSphereCollider( DVec3f::Zero, 0.5f, 1, 12 ) ); // wall(1), pellet(2), ghost(4), pacman(8)
					obj->Register( "OnCollision", this );
					AddActor( obj );
				}
			}
		}

		{
			{ // floor
				std::shared_ptr<DGameObject> obj( DGameObject::Instantiate(
					DVec3f( 4.0f, 0, -4.0f ), DVec3f::One * 4.5f, DVec3f::Zero ) );
				obj->AddComponent( new DRenderable( "data/planeMaterial.matl", "data/plane.mesh", "data/green_fire_1.dds" ) );
				AddActor( obj );
			}
		}

		{ //lights
			float *aColor = new float[ 3 ] { 0.016f, 0.016f, 0.016f };
			float *dColor = new float[ 3 ] { 1.0f, 1.0f, 1.0f };
			DVec3f dDir( 0.1f, -1.0f, 0.2f );
			AddLight( new Light( aColor, dColor, dDir ) );
			delete aColor;
			delete dColor;
		}
		{// add camera
			float fovy = D3DXToRadian( 60.0f );
			float aspect = 16.0f / 9.0f;
			float zNear = 0.01f;
			float zFar = 1000;
			AddCamera( new Camera(
				DVec3f( 4, 9.0f, -6.0f ), DVec3f( 0, D3DXToRadian( 80 ), 0 ),
				fovy, aspect, zNear, zFar ) );
		}
			{ // lives tally
				float width = 111;
				float height = 111;
				float nWidth = (width / iContentMgr->_settings->RenderSettings->Width);
				float nHeight = (height / iContentMgr->_settings->RenderSettings->Height);
				float hWidth = nWidth * 0.5f;
				float hHeight = nHeight * 0.5f;
				{
					Parrhasius::Sprite *sprite2 = new Parrhasius::Sprite( "data/dragon.dds",
						Parrhasius::Rect( hWidth - 1, hHeight - 1, nWidth, nHeight ) );
					sprite2->SetColor( D3DCOLOR_ARGB( 255, 204, 45, 0 ) );
					iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( sprite2 ) );
				}
				{
					Parrhasius::Sprite *sprite2 = new Parrhasius::Sprite( "data/dragon.dds",
						Parrhasius::Rect( hWidth + nWidth - 1, hHeight - 1, nWidth, nHeight ) );
					sprite2->SetColor( D3DCOLOR_ARGB( 255, 204, 45, 0 ) );
					iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( sprite2 ) );
				}
			}
		{ // score counter
			_scoreboard->Load( iContentMgr );
		}
		DScene::Load( iDevice, iContentMgr );
	}

	void Game::HandleInput( void )
	{
		DScene::HandleInput();
		if( eae6320::UserInput::IsKeyPressed( VK_RETURN ) )
		{
			_scoreboard->Reset();
			Reset();
		}
	}

	void Game::Update( DReal dt )
	{
		DScene::Update( dt );

		if( _pacman->_transform->Position.X() < 0 )
		{
			DVec3f old = _pacman->_transform->Position;
			_pacman->_transform->Position = DVec3f( 8.0f, old.Y(), old.Z() );
		}
		if( _pacman->_transform->Position.X() > 8.0f )
		{
			DVec3f old = _pacman->_transform->Position;
			_pacman->_transform->Position = DVec3f( 0.0f, old.Y(), old.Z() );
		}

		if( _scoreboard->Score() == 510 )
		{
			Reset();
		}
	}

	void Game::ProcessMessage( const Demeter::HashedString &iMessage, DObject &iObject,
		void* iObjectData )
	{
		if( iMessage.Get() == HashedString::Hash( "OnCollision" ) )
		{
			MCGameObject *obj = (MCGameObject*)&iObject;
			if( obj->McType == ETile::PELLET || obj->McType == ETile::POWER_PELLET )
			{
				_scoreboard->UpdateScore( obj->Points );
				obj->_renderable->IsEnabled = false;
				obj->_collider->IsEnabled = false;
			}
			else if( obj->McType == ETile::WALL )
			{
				HitInfo *hitInfo = (HitInfo*)iObjectData;
				DRigidBody *rigidBody = hitInfo->OtherGameObject->_rigidBody;
				if( rigidBody )
				{
					hitInfo->OtherGameObject->_transform->Position -= rigidBody->Velocity * eae6320::Time::GetSecondsElapsedThisFrame();
					rigidBody->Velocity = DVec3f::Zero;
				}
			}
		}
	}

	void Game::Reset( void )
	{
		_scoreboard->Reset();
		_pacman->_transform->Position = _startPos;

		std::map<unsigned int, DGameObjectPtr>::iterator it;
		for( it = _idToActorMap.begin();
			it != _idToActorMap.end();
			++it )
		{
			DController *controller = it->second->_controller;
			if( controller )
			{
				controller->IsEnabled = true;
			}
			DRenderable *renderable = it->second->_renderable;
			if( renderable )
			{
				renderable->IsEnabled = true;
			}
			DCollider *collidable = it->second->_collider;
			if( collidable )
			{
				collidable->IsEnabled = true;
			}
			DRigidBody *rigidBody = it->second->_rigidBody;
			if( rigidBody )
			{
				rigidBody->IsEnabled = true;
				rigidBody->Velocity = DVec3f::Zero;
				rigidBody->Force = DVec3f::Zero;
			}
		}
	}

	void Game::Cleanup( void )
	{}
}