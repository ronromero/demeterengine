#ifndef _MONSTERCHASE_GAME_H_
#define _MONSTERCHASE_GAME_H_

#include <Demeter/Exports/DScene.h>
#include <Demeter/Exports/DGameObject.h>
#include <Demeter/Exports/DContentManager.h>
#include <Demeter/Exports/IObserver.h>

namespace MonsterChase
{
	class Level;
	class Scoreboard;

	class Game : public Demeter::DScene, public Demeter::IObserver
	{
	public:
		Game( void );
		~Game( void );

		void Load( IDirect3DDevice9 *iDevice, std::shared_ptr<Demeter::DContentManager> iContentManager );
		void HandleInput( void );
		void Update( DReal dt );
		void ProcessMessage( const Demeter::HashedString &iMessage, Demeter::DObject &iObject,
			void* iObjectData );
		void Cleanup( void );

		void Reset( void );

	private:

		Level *_level;
		Scoreboard *_scoreboard;
		Demeter::DVec3f _startPos;
		Demeter::DGameObject *_pacman;

		Game( const Game& );
		Game& operator=(const Game&);
	};
}
#endif //_MONSTERCHASE_GAME_H_