#include "Scoreboard.h"
#include <Demeter/Exports/DEngineSettings.h>
#include <Parrhasius/Exports/PEngineSettings.h>
#include <Parrhasius/Exports/Sprite.h>
#include <vector>

using namespace Demeter;
using namespace Parrhasius;

namespace MonsterChase
{
	Scoreboard::Scoreboard( const char* iPath ) : _score( 0 ), _path( iPath ),
		_ones( NULL ), _tens( NULL ), _hundys( NULL ), _thous( NULL )
	{
		for( int i = 0; i < 10; ++i )
		{
			_numbers[ i ].width = 0.1f;
			_numbers[ i ].height = 1.0f;
			_numbers[ i ].x = 0.1f * i;
			_numbers[ i ].y = 0;
		}
	}

	void Scoreboard::UpdateScore( const int iAmt )
	{
		_score += iAmt;
		if( iAmt > 9999 ) { _score = 9999; }
		_ones->SetTexCoords( &_numbers[ _score % 10 ] );
		_tens->SetTexCoords( &_numbers[ (_score / 10) % 10 ] );
		_hundys->SetTexCoords( &_numbers[ (_score / 100) % 10 ] );
		_thous->SetTexCoords( &_numbers[ (_score / 1000) % 10 ] );
	}

	const unsigned int Scoreboard::Score( void ) const
	{
		return _score;
	}

	void Scoreboard::Reset( void )
	{
		_score = 0;
		UpdateScore( 0 );
	}

	void Scoreboard::Load( std::shared_ptr<Demeter::DContentManager> iContentMgr )
	{
		float width = 90;
		float height = 90;
		float nWidth = (width / iContentMgr->_settings->RenderSettings->Width);
		float nHeight = (height / iContentMgr->_settings->RenderSettings->Height);
		float hWidth = nWidth * 0.5f;
		float hHeight = nHeight * 0.5f;
		D3DCOLOR color = D3DCOLOR_ARGB( 255, 255, 255, 255 );
		{
			_ones = new Parrhasius::Sprite( "data/font_number_sprite.dds",
				Parrhasius::Rect( 1 - hWidth - 0 * nWidth, 1 - hHeight, nWidth, nHeight ) );
			_ones->SetColor( color );
			_tens = new Parrhasius::Sprite( "data/font_number_sprite.dds",
				Parrhasius::Rect( 1 - hWidth - 1 * nWidth, 1 - hHeight, nWidth, nHeight ) );
			_tens->SetColor( color );
			_hundys = new Parrhasius::Sprite( "data/font_number_sprite.dds",
				Parrhasius::Rect( 1 - hWidth - 2 * nWidth, 1 - hHeight, nWidth, nHeight ) );
			_hundys->SetColor( color );
			_thous = new Parrhasius::Sprite( "data/font_number_sprite.dds",
				Parrhasius::Rect( 1 - hWidth - 3 * nWidth, 1 - hHeight, nWidth, nHeight ) );
			_thous->SetColor( color );
			iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( _ones ) );
			iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( _tens ) );
			iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( _hundys ) );
			iContentMgr->_sprites.push_back( std::shared_ptr<Parrhasius::Sprite>( _thous ) );
			Reset();
		}
	}

	Scoreboard::~Scoreboard( void )
	{}
}