#include "MCGameObject.h"

using namespace Demeter;

namespace MonsterChase
{
	MCGameObject::MCGameObject( 
		DVec3f iPos, DVec3f iScale, DVec3f iRot, 
		ETile type, unsigned int iPoints ) :
		DGameObject( iPos, iScale, iRot ),
		McType( type ), Points( iPoints )
	{}


	MCGameObject::~MCGameObject()
	{}
}