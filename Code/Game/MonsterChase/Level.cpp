#include "Level.h"

namespace MonsterChase
{
	Level::Level( const int iSize ) : _tiles( new ETile[ iSize ] )
	{}

	Level::~Level( void )
	{
		delete[] _tiles;
	}
}