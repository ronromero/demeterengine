return
{
	{
		builder = "ShaderBuilder.exe",
		extensions =
		{
			source = "hlsl",
			target = "shd",
		},
		assets =
		{
			{
				name = "vertexShader",
				arguments = { "vertex" },
			},
			{
				name = "fragmentShader",
				arguments = { "fragment" },
			},
			{
				name = "spriteVS",
				arguments = { "vertex" },
			},
			{
				name = "spritePS",
				arguments = { "fragment" },
			},
			{
				name = "debugVS",
				arguments = { "vertex" },
			},
			{
				name = "debugPS",
				arguments = { "fragment" },
			},
		},
	},

	{
		builder = "GenericBuilder.exe",
		extensions =
		{
			source = "mat.lua",
			target = "matl",
		},
		assets =
		{
			{
				name = "cubeMaterial",
			},
			{
				name = "planeMaterial",
			},
			{
				name = "pyramidMaterial",
			},
		},
	},
	-- Meshes
	{
		builder = "MeshBuilder.exe",
		extensions =
		{
			source = "jmsh",
			target = "mesh",
		},
		assets =
		{
			{
				name = "mcube",
			},
			{
				name = "plane",
			},
			{
				name = "pyramid",
			},
            {
				name = "pellet",
			},
		},
	},
	-- Textures
	{
		builder = "TextureBuilder.exe",
		extensions =
		{
			source = "png",
			target = "dds",
		},
		assets =
		{
			{
				name = "green_fire_1",
			},
			{
				name = "MiLxX99ia",
			},
			{
				name = "illuminati_eye",
			},
			{
				name = "CampFireFinished",
			},
			{
				name = "dragon",
			},
			{
				name = "font_number_sprite",
			},
		},
	},
}



	
