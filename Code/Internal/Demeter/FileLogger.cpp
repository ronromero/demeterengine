#include "Exports/precompiled.h"

#include "Exports/FileLogger.h"

namespace Demeter
{
	FileLogger::FileLogger( void ) {}
	FileLogger::~FileLogger( void ) {}

	void FileLogger::PublishLog( const char* iLogName )
	{
		size_t length = strlen( iLogName ) + 5;//.rlg + \0
		char *filename = reinterpret_cast<char*>(malloc( length * sizeof( char ) ));
		strcpy_s( filename, length, iLogName );
		strcat_s( filename, length, ".rlg" );

		_file.Open( filename, std::ios_base::app );
		_file.Write( iLogName );
		// TODO: write date and time of entry
		_file.Write( "\n" );
		_file.Write( _logs[ iLogName ].str().c_str() );
		_file.Write( "\r\n" );
		_file.Close();
		delete[] filename;
	}

	void FileLogger::PublishAllLogs( void )
	{
		std::map<const char*, std::stringstream>::iterator itr;
		for( itr = _logs.begin(); itr != _logs.end(); itr++ )
		{
			PublishLog( itr->first );
		}
		_logs.clear();
	}
}