// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

#include "Exports/precompiled.h"
#include "Exports/DVector.h"

namespace Demeter
{
	template<> const DVec2<float> DVec2<float>::Zero( 0.0f, 0.0f );
	template<> const DVec2<float> DVec2<float>::UnitX( 1.0f, 0.0f );
	template<> const DVec2<float> DVec2<float>::UnitY( 0.0f, 1.0f );
	template<> const DVec2<float> DVec2<float>::One( 1.0f, 1.0f );

	template<> const DVec2<double> DVec2<double>::Zero( 0.0, 0.0 );
	template<> const DVec2<double> DVec2<double>::UnitX( 1.0, 0.0 );
	template<> const DVec2<double> DVec2<double>::UnitY( 0.0, 1.0 );
	template<> const DVec2<double> DVec2<double>::One( 1.0, 1.0 );
}
