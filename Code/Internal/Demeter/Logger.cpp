#include "Exports/precompiled.h"

#include <stdarg.h> // va_start, va_end
#include <stdio.h> // vsprintf_s
#include "Exports/DLogger.h"

namespace Demeter
{
	DLogger::DLogger( void )
	{
		AddLog("Error");
	}

	DLogger::~DLogger( void ) {}

	void DLogger::AddLog( const char* iLogName )
	{
		if(!LogExists(iLogName))
		{
			_logs[iLogName];
		}
	}

	void DLogger::AppendLog( const char* iLogName, const char* iFmt, ... )
	{
		AddLog( iLogName );

		va_list args;
		va_start( args, iFmt );
		//const char *buffer = Format( iFmt, args );
		int len = _vscprintf( iFmt, args ) + 1; // 1 for terminating \0
		char *buffer = reinterpret_cast<char*>(malloc( len * sizeof( char ) ));
		vsprintf_s( buffer, len, iFmt, args );
		va_end( args );

		_logs[ iLogName ] << buffer;
		delete buffer;
	}

	const bool DLogger::LogExists( const char* iLogName ) const
	{
		return _logs.find( iLogName ) != _logs.end();
	}
}