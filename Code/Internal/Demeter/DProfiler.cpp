#include "Exports/precompiled.h"

#include "Exports/DProfiler.h"
#include "Exports/FileLogger.h"

namespace Demeter
{
	void Profiler::AddTiming( const char *ipName, double iMS )
	{
		if( sAccumulators.find( ipName ) == sAccumulators.end() )
		{
			sAccumulators[ ipName ] = struct Accumulator();
		}
		if( iMS < sAccumulators[ ipName ].mMin )
			sAccumulators[ ipName ].mMin = iMS;
		if( iMS > sAccumulators[ ipName ].mMax )
			sAccumulators[ ipName ].mMax = iMS;
		sAccumulators[ ipName ].mCount++;
		sAccumulators[ ipName ].mSum += iMS;
	}

	void Profiler::PrintTimings( DLogger *ipLog )
	{
		const char* profileLogName = "Profiler";
		std::map<std::string, struct Accumulator>::iterator iter;
		for( iter = sAccumulators.begin(); iter != sAccumulators.end(); ++iter )
		{
			DReal average = iter->second.mCount > 0 ? (iter->second.mSum) / (DReal)iter->second.mCount : 0.0;
			ipLog->AppendLog( profileLogName, "[%s] Count: %d Sum: %f Min: %f Max: %f Ave: %f ms\n",
				iter->first.c_str(), iter->second.mCount, iter->second.mSum, iter->second.mMin,
				iter->second.mMax, average );
		}
		ipLog->PublishLog( profileLogName );
	}
}