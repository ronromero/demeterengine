#include "Exports/precompiled.h"

#include "Exports/DScene.h"
#include "Exports/DContentManager.h"
#include "Exports/DGameObject.h"
#include "Exports/DRenderable.h"
#include "Exports/DGameComponent.h"
#include "Exports/Camera.h"

#include <Parrhasius/Exports/Sprite.h>
#include <Parrhasius/Exports/Rect.h>
#include <Parrhasius/Exports/PEngineSettings.h>
#include <Parrhasius/Exports/Debug.h>

#include <d3d9.h>
#include <d3dx9.h>

namespace Demeter
{
	DScene::DScene( void ) : _light( NULL ), _camera( NULL ) {}

	DScene::~DScene( void )
	{
		_contentMgr = NULL;
		SDELETE( _light );
		SDELETE( _camera );
	}

	void DScene::Load( IDirect3DDevice9 *iDevice, std::shared_ptr<DContentManager> iContentMgr )
	{
		_contentMgr = iContentMgr;
		for( _idActorMapItr = _idToActorMap.begin();
			_idActorMapItr != _idToActorMap.end();
			++_idActorMapItr )
		{
			DRenderable *renderable = _idActorMapItr->second->_renderable;
			if( renderable )
			{
				renderable->Load( iDevice );
			}
		}

		for( _contentMgr->_spriteItr = _contentMgr->_sprites.begin();
			_contentMgr->_spriteItr != _contentMgr->_sprites.end();
			_contentMgr->_spriteItr++ )
		{
			(*_contentMgr->_spriteItr)->Load( iDevice );
		}
	}

	void DScene::HandleInput( void )
	{
		std::map<unsigned int, DGameObjectPtr>::iterator it;
		for( it = _idToActorMap.begin();
			it != _idToActorMap.end();
			++it )
		{
			DController *controller = it->second->_controller;
			if( controller && controller->IsEnabled )
			{
				controller->HandleInput();
			}
		}
	}

	void DScene::HandleCollision( void )
	{
		std::vector<std::shared_ptr<DGameObject>> actors;
		for( _idActorMapItr = _idToActorMap.begin();
			_idActorMapItr != _idToActorMap.end();
			++_idActorMapItr )
		{
			actors.push_back( _idActorMapItr->second );
		}

		for( unsigned int i = 0; i < actors.size() - 1; ++i )
		{
			DGameObjectPtr actor1 = actors[ i ];
			DCollider *collider1 = actor1->_collider;
			if( !collider1 || !collider1->IsEnabled ) continue;
			for( unsigned int j = i + 1; j < actors.size(); ++j )
			{
				DGameObjectPtr actor2 = actors[ j ];
				DCollider *collider2 = actor2->_collider;
				if( !collider2 || !collider2->IsEnabled ) continue;

				unsigned int mask1 = collider1->Layer & collider2->Mask;
				if( mask1 != 0 )
				{
					HitInfo hitInfo;
					if( collider2->Intersects( collider1, &hitInfo ) )
					{
						actor2->BroadcastMessage( "OnCollision", &hitInfo );
					}
				}

				unsigned int mask2 = collider2->Layer & collider1->Mask;
				if( mask2 != 0 )
				{
					HitInfo hitInfo;
					if( collider1->Intersects( collider2, &hitInfo ) )
					{
						actor1->BroadcastMessage( "OnCollision", &hitInfo );
					}
				}
			}
		}
	}

	void DScene::Update( DReal dt )
	{
		for( _idActorMapItr = _idToActorMap.begin();
			_idActorMapItr != _idToActorMap.end();
			++_idActorMapItr )
		{
			DRigidBody *rigidBody = _idActorMapItr->second->_rigidBody;

			if( rigidBody && rigidBody->Mass > DMathf::EPSILON )
			{
				rigidBody->GameObject->_transform->Position += rigidBody->Velocity * (float)dt;
				rigidBody->Velocity += rigidBody->Force / (float)rigidBody->Mass * (float)dt;
			}
		}
	}

	void DScene::Draw( IDirect3DDevice9* iDevice )
	{
		std::map<unsigned int, DGameObjectPtr>::iterator it;
		for( it = _idToActorMap.begin();
			it != _idToActorMap.end();
			++it )
		{
			DRenderable *renderable = it->second->_renderable;
			if( renderable && renderable->IsEnabled )
			{
				renderable->Draw( iDevice, _camera, _light );
			}
		}
		PDEBUG_DRAWLINES( iDevice, &_camera->View(), &_camera->Projection() );
	}

	void DScene::DrawSprite( IDirect3DDevice9* iDevice )
	{
		std::vector<std::shared_ptr<Parrhasius::Sprite>> sprites;
		for( _contentMgr->_spriteItr = _contentMgr->_sprites.begin();
			_contentMgr->_spriteItr != _contentMgr->_sprites.end();
			_contentMgr->_spriteItr++ )
		{
			(*_contentMgr->_spriteItr)->Draw( iDevice );
		}
	}

	const Camera* DScene::GetMainCamera( void ) const
	{
		return _camera;
	}
}