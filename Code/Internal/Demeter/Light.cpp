#include "Exports/precompiled.h"

#include "Exports/Light.h"
#include "Exports/DVector.h"

namespace Demeter
{
	Light::Light( const float *iAmbientColor, const float *iDiffuseColor, const DVec3f iDiffuseDirection ) :
		DGameObject(),
		_ambientColor( iAmbientColor ),
		_diffuseColor( iDiffuseColor ),
		_diffuseDirection( iDiffuseDirection )
	{}

	Light::~Light( void )
	{}
}