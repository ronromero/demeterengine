#include "Exports/precompiled.h"

#include "Exports/Rtti.h"

namespace Demeter
{
	Rtti::Rtti( const char* name, const Rtti* iBaseType )
		: _name( name ), _baseType( iBaseType )
	{}

	Rtti::~Rtti( void ) {};

	const char* Rtti::GetName() const
	{
		return _name;
	}

	bool Rtti::IsExactly( const Rtti& type ) const
	{
		return(&type == this);
	}

	bool Rtti::IsDerived( const Rtti& type ) const
	{
		const Rtti* search = this;
		while( search )
		{
			if( search == &type )
			{
				return true;
			}
			search = search->_baseType;
		}
		return false;
	}
}
