#include "Exports/precompiled.h"

#include "Exports/DGameObject.h"
#include "Exports/DRenderable.h"
#include <d3d9.h>
#include <d3dx9.h>

namespace Demeter
{
	std::shared_ptr<DGameObject> DGameObject::Instantiate( void )
	{
		return std::shared_ptr<DGameObject>( new DGameObject() );
	}

	std::shared_ptr<DGameObject> DGameObject::Instantiate( const DVec3f iPos,
		const DVec3f iScale, const DVec3f iEulerRotInDeg )
	{
		return std::shared_ptr<DGameObject>( new DGameObject( iPos, iScale, iEulerRotInDeg ) );
	}

	DGameObject::DGameObject( void ) :
		_controller( NULL ),
		_renderable( NULL ),
		_collider( NULL ),
		_rigidBody( NULL ),
		_transform( new DTransform() )
	{
		_transform->GameObject = this;
	}

	DGameObject::DGameObject(
		const DVec3f iPos, const DVec3f iScale, const DVec3f iEulerRotInDeg ) :
		_controller( NULL ),
		_renderable( NULL ),
		_collider( NULL ),
		_rigidBody( NULL ),
		_transform( new DTransform( iPos, iScale, iEulerRotInDeg ) )
	{
		_transform->GameObject = this;
	}

	DGameObject::~DGameObject()
	{
		SDELETE( _renderable );
		SDELETE( _transform );
		SDELETE( _controller );
		SDELETE( _collider );
		SDELETE( _rigidBody );
	}

	void DGameObject::AddComponent( DGameComponent *iComponent )
	{
		iComponent->GameObject = this;
		if( iComponent->GetRttiType().IsDerived( DCollider::TYPE ) )
		{
			_collider = (DCollider*)iComponent;
		}
		else if( iComponent->GetRttiType().IsDerived( DRenderable::TYPE ) )
		{
			_renderable = (DRenderable*)iComponent;
		}
		else if( iComponent->GetRttiType().IsDerived( DRigidBody::TYPE ) )
		{
			_rigidBody = (DRigidBody*)iComponent;
		}
		else if( iComponent->GetRttiType().IsDerived( DController::TYPE ) )
		{ //TODO: make normal component
			_controller = (DController*)iComponent;
		}
		else
		{
			//TODO: add to components list
		}
	}
}