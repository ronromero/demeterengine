#ifndef _DEMETER_DENGINESETTINGS_H_
#define _DEMETER_DENGINESETTINGS_H_


namespace Parrhasius
{
	struct PEngineSettings;
}

namespace Demeter
{
	class DEngineSettings
	{
	public:
		Parrhasius::PEngineSettings* RenderSettings;
		DEngineSettings( void );
		~DEngineSettings( void );
	};
}

#endif //_DEMETER_DENGINESETTINGS_H_