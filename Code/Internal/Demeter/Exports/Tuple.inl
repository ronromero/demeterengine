template<int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>::Tuple( void )
{
	// Uninitialized for native data. 
}

template<int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>::Tuple( const Tuple& tuple )
{
	for( int i = 0; i < DIMENSION; ++i )
	{
		_tuple[ i ] = tuple._tuple[ i ];
	}
}

template<int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>::~Tuple( void ) {}

template<int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>::operator const TYPE*()const
{
	return _tuple;
}

template<int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>::operator TYPE*()
{
	return _tuple;
}

template<int DIMENSION, typename TYPE>
TYPE Tuple<DIMENSION, TYPE>::operator[]( int i ) const
{
	return _tuple[ i ];
}

template<int DIMENSION, typename TYPE>
TYPE& Tuple<DIMENSION, TYPE>::operator[]( int i )
{
	_tuple[ i ];
}

template <int DIMENSION, typename TYPE>
Tuple<DIMENSION, TYPE>& Tuple<DIMENSION, TYPE>::operator= (const Tuple& tuple)
{
	for( int i = 0; i < DIMENSION; ++i )
	{
		_tuple[ i ] = tuple._tuple[ i ];
	}
	return *this;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator==(const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) == 0;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator!=(const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) != 0;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator<(const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) < 0;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator<=(const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) <= 0;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator>(const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) > 0;
}

template<int DIMENSION, typename TYPE>
bool Tuple<DIMENSION, TYPE>::operator >= (const Tuple& tuple) const
{
	return memcmp( _tuple, tuple._tuple, DIMENSION*sizeof( TYPE ) ) >= 0;
}