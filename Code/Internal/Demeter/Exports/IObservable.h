#pragma once

namespace Demeter
{
	class IObserver;
	class HashedString;

	class IObservable
	{
		virtual void Register( const HashedString &iMessage, IObserver* iObserver ) = 0;
		virtual void UnRegister( const HashedString &iMessage, IObserver* iObserver ) = 0;
		virtual void BroadcastMessage( const HashedString &iMessage, void* iMessageData ) = 0;
	};
}