#ifndef EAE6320_TIME_H
#define EAE6320_TIME_H

#include <string>

namespace eae6320
{
	namespace Time
	{
		float GetTotalSecondsElapsed();
		float GetSecondsElapsedThisFrame();

		void OnNewFrame();
		bool Initialize( std::string* o_errorMessage = NULL );
	}
}

#endif	// EAE6320_TIME_H
