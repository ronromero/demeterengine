#ifndef _DEMETER_DSCENE_H_
#define _DEMETER_DSCENE_H_

#include <memory>
#include <vector>
#include "Camera.h"
#include "DGameObject.h"
#include "Light.h"

struct IDirect3DDevice9;

namespace Demeter
{
	class DContentManager;

	class DScene
	{
	public:
		DScene( void );
		virtual ~DScene( void );

		virtual void Load( IDirect3DDevice9 *iDevice, std::shared_ptr<DContentManager> iContentManager );
		virtual void HandleInput( void );
		virtual void Update( DReal dt );
		void HandleCollision( void );

		void Draw( IDirect3DDevice9 *iDevice );
		void DrawSprite( IDirect3DDevice9 *iDevice );

		virtual void Cleanup( void ) {}

		const Camera* GetMainCamera( void ) const;

		void AddActor( DGameObjectPtr actor )
		{
			if( _idToActorMap.find( actor->Id() ) != _idToActorMap.end() ) return;
			_idToActorMap[ actor->Id() ] = actor;
		}

		void AddLight( Light *iLight )
		{
			_light = iLight;
		}

		void AddCamera( Camera *iCamera )
		{
			_camera = iCamera;
		}

	protected:
		std::map<unsigned int, DGameObjectPtr> _idToActorMap;
		std::map<unsigned int, DGameObjectPtr>::iterator _idActorMapItr;
		Light *_light;
		Camera *_camera;

		std::shared_ptr<DContentManager> _contentMgr;

	private:
		DScene( const DScene& );
		DScene& operator=(const DScene&);
	};
}

#endif //_DEMETER_DSCENE_H_