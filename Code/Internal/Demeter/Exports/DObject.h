#ifndef _DEMETER_DOBJECT_H_
#define _DEMETER_DOBJECT_H_

#include "precompiled.h"
#include "Rtti.h"
#include "IObservable.h"

namespace Demeter
{
	class Rtti;
	class IObserver;
	class HashedString;

	class DObject : public IObservable
	{
		D1_DECLARE_RTTI

	public:
		virtual ~DObject( void );

		const unsigned int Id( void ) const;
		void Dispose( void );
		void Register( const HashedString &iMessage, IObserver* iObserver );
		void UnRegister( const HashedString &iMessage, IObserver* iObserver );
		void BroadcastMessage( const HashedString &iMessage, void* iMessageData );
		void BroadcastMessage( const char* iMessage, void* iMessageData );

	protected:
		DObject( void );

	private:
		static unsigned int s_id;
		unsigned int _id;
		std::map< HashedString, std::vector<IObserver*>> _observers;

		DObject( const DObject& );
		DObject& operator=(const DObject &);
	};
}

#endif //_DEMETER_DOBJECT_H_