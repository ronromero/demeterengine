#ifndef _DEMETER_DRENDERABLE_H_	
#define _DEMETER_DRENDERABLE_H_

#include "DGameComponent.h"
#include <Parrhasius/Exports/Mesh.h>
#include <Parrhasius/Exports/Material.h>

namespace Demeter
{
	class Camera;
	class Light;

	class DRenderable : public DGameComponent
	{
		D1_DECLARE_RTTI;
	public:
		DRenderable( const char *iMaterialPath,
			const char *iMeshPath, const char *iTexturePath );
		~DRenderable( void );

		void Load( IDirect3DDevice9 *iGraphicsDevice );
		void Draw( IDirect3DDevice9 *iGraphicsDevice, Camera *iCamera, Light *iLight );

		void SetMesh( std::shared_ptr<Parrhasius::Mesh> iMesh );
		void SetMaterial( std::shared_ptr<Parrhasius::Material> iMaterial );

	protected:
		void LoadContent( IDirect3DDevice9 *iGraphicsDevice,
			const char* iMaterialPath, const char* iMeshPath, const char* iTexturePath );

		Parrhasius::Mesh *_mesh;
		Parrhasius::Material *_material;

		D3DXHANDLE _worldToView;
		D3DXHANDLE _viewToScreen;
		D3DXHANDLE _modelToWorld;
		D3DXHANDLE _lightAmbientColor;
		D3DXHANDLE _lightDiffuseColor;
		D3DXHANDLE _lightDiffuseDir;

	private:
		const char *_materialPath, *_meshPath, *_texturePath;

	};
}

#endif //_DEMETER_DRENDERABLE_H_