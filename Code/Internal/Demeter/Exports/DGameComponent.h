#pragma once

#include "DObject.h"
#include "DVector.h"
#include "DMatrix.h"
#include "IController.h"

namespace Demeter
{
	class DCollider;
	class DGameObject;
	class DRigidBody;
	class DTransform;

	class DGameComponent : public DObject
	{
		D1_DECLARE_RTTI
	public:
		virtual ~DGameComponent( void );

		DGameObject* GameObject;
		bool IsEnabled;

	protected:
		DGameComponent( DGameObject* iObj );

	private:
		DGameComponent( const DGameComponent& );
		DGameComponent& operator=(const DGameComponent&);
	};

	class DTransform : public DGameComponent
	{
		D1_DECLARE_RTTI
	public:
		DVec3f Position, Scale, Rotation;

		DTransform( void );
		DTransform( DVec3f iPos, DVec3f iScale, DVec3f iEulerRotXYZ );
		virtual ~DTransform( void );

		Matrix4f LocalTransform( void ) const;
		Matrix4f WorldTransform( void ) const;

	private:
		Matrix3f _local;
		DTransform *_parent;

		DTransform( const DTransform& );
		DTransform& operator=(const DTransform&);
	};

	class DController : public DGameComponent, public IController
	{
		D1_DECLARE_RTTI
	public:
		DController( DGameObject* iObj );
		virtual ~DController( void );

		virtual void HandleInput( void ) {}

	private:
		DController( const DController& );
		DController& operator=(const DController&);
	};

	class DRigidBody : public DGameComponent
	{
		D1_DECLARE_RTTI
	public:
		DVec3f Velocity;
		//DVec3f Acceleration;
		DVec3f Force;
		DReal Mass;

		DRigidBody( DReal iMass = 0 );
		virtual ~DRigidBody( void );

	private:
		DRigidBody( const DRigidBody& );
		DRigidBody& operator=(const DRigidBody&);
	};

	struct HitInfo;

	class DCollider : public DGameComponent
	{
		D1_DECLARE_RTTI
	public:
		DVec3f Center;
		unsigned int Layer;
		unsigned int Mask;

		virtual ~DCollider( void );
		virtual bool Intersects( const DCollider *iCollider, HitInfo *oHitInfo ) = 0;

	protected:
		DCollider( const DVec3f iCenter, const unsigned int iLayer, const unsigned int iMask );

	private:
		DCollider( const DCollider& );
		DCollider& operator=(const DCollider&);
	};

	class DSphereCollider : public DCollider
	{
		D1_DECLARE_RTTI
	public:
		DReal Radius;

		DSphereCollider( const DVec3f iCenter, const DReal iRadius,
			const unsigned int iLayer = -1, const unsigned int iMask = -1 );
		~DSphereCollider( void );

		bool Intersects( const DCollider *iCollider, HitInfo *oHitInfo );
		bool Intersects( const DSphereCollider *iCollider, HitInfo *oHitInfo );
	};

	struct HitInfo
	{
		DVec3f Normal;
		DVec3f ContactPoint;
		DCollider* Collider;
		DCollider* OtherCollider;
		DGameObject* GameObject;
		DGameObject* OtherGameObject;
	};
}