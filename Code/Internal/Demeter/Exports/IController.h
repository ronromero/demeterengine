#ifndef _DEMETER_ICONTROLLER_H_
#define _DEMETER_ICONTROLLER_H_

namespace Demeter
{
	class IController
	{
	public:
		virtual void HandleInput( void ) = 0;
	};
}
#endif //_DEMETER_ICONTROLLER_H_