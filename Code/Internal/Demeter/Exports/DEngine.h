#ifndef _DEMETER_DENGINE_H_
#define _DEMETER_DENGINE_H_

#include "precompiled.h"

namespace Parrhasius
{
	class PEngine;
	struct PEngineSettings;
	class Material;
	class Mesh;
	class Texture;
	class Sprite;
}

namespace Demeter
{
	class DGameObject;
	class DEngineSettings;
	class Light;
	class Camera;
	class DScene;
	class DContentManager;

	void LoadDEngineSettings( HINSTANCE hInstance, PSTR lpCmdLine, int nCmdShow, DEngineSettings &oSettings );

	class DEngine
	{
	public:
		DEngine( DEngineSettings &iSettings );
		~DEngine( void );

		bool Initialize( void );
		void AddScene( std::shared_ptr<DScene> iScene );
		void Run( void );
		int ShutDown( void );

	private:
		DEngine( const DEngine& );
		DEngine* operator=(const DEngine&);

		DEngineSettings *_settings;
		std::shared_ptr<Parrhasius::PEngine> _renderEngine;
		std::shared_ptr<DScene> _mainScene;
		std::shared_ptr<DContentManager>_contentManager;
	};
}

#endif