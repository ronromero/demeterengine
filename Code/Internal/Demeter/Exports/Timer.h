#pragma once

#include "precompiled.h"

namespace Demeter
{
	class Timer
	{
	public:
		static Tick GetCurrentTick( void );
		static DReal GetDifferenceMS( Tick &iStart, Tick &iEnd );
	};

	inline Tick Timer::GetCurrentTick( void )
	{
		Tick tick;
		QueryPerformanceCounter( &tick );
		return tick;
	}

	inline DReal Timer::GetDifferenceMS( Tick &iStart, Tick &iEnd )
	{
		Tick freq;
		QueryPerformanceFrequency( &freq );
		return DReal( iEnd.QuadPart - iStart.QuadPart ) / (DReal)freq.QuadPart;
	}
}
