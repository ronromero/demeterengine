#pragma once

#include <map>
#include <string>

namespace Demeter
{
	class DLogger;

	class Profiler
	{
	public:
		void AddTiming( const char *ipName, double iDeltaMS );
		void PrintTimings( void );
		void PrintTimings( DLogger *iLogger );

	private:
		struct Accumulator
		{
			double mMax;
			double mMin;
			double mSum;
			unsigned int mCount;

			Accumulator( void ) : mMax( DBL_MIN ), mMin( DBL_MAX ), mSum( 0.0 ), mCount( 0 ) {}
		};
		std::map<std::string, struct Accumulator> sAccumulators;
	};
}