// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>::DVec3()
{
	// Uninitialized for performance in array construction.
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>::DVec3( const DVec3& vec )
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
	_tuple[ 2 ] = vec._tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>::DVec3( const Tuple<3, Real>& tuple )
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
	_tuple[ 2 ] = tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>::DVec3( Real x, Real y, Real z )
{
	_tuple[ 0 ] = x;
	_tuple[ 1 ] = y;
	_tuple[ 2 ] = z;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>& DVec3<Real>::operator= (const DVec3& vec)
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
	_tuple[ 2 ] = vec._tuple[ 2 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real>& DVec3<Real>::operator= (const Tuple<3, Real>& tuple)
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
	_tuple[ 2 ] = tuple[ 2 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::X() const
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec3<Real>::X()
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::Y() const
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec3<Real>::Y()
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::Z() const
{
	return _tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec3<Real>::Z()
{
	return _tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real> DVec3<Real>::operator+ (const DVec3& vec) const
{
	return DVec3
		(
		_tuple[ 0 ] + vec._tuple[ 0 ],
		_tuple[ 1 ] + vec._tuple[ 1 ],
		_tuple[ 2 ] + vec._tuple[ 2 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real> DVec3<Real>::operator- (const DVec3& vec) const
{
	return DVec3
		(
		_tuple[ 0 ] - vec._tuple[ 0 ],
		_tuple[ 1 ] - vec._tuple[ 1 ],
		_tuple[ 2 ] - vec._tuple[ 2 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real> DVec3<Real>::operator* (Real scalar) const
{
	return DVec3
		(
		scalar*_tuple[ 0 ],
		scalar*_tuple[ 1 ],
		scalar*_tuple[ 2 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real> DVec3<Real>::operator/ (Real scalar) const
{
	DVec3 result;

	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		result._tuple[ 0 ] = invScalar*_tuple[ 0 ];
		result._tuple[ 1 ] = invScalar*_tuple[ 1 ];
		result._tuple[ 2 ] = invScalar*_tuple[ 2 ];
	}
	else
	{
		result._tuple[ 0 ] = DMath<Real>::MAX_REAL;
		result._tuple[ 1 ] = DMath<Real>::MAX_REAL;
		result._tuple[ 2 ] = DMath<Real>::MAX_REAL;
	}

	return result;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real> DVec3<Real>::operator- () const
{
	return DVec3
		(
		-_tuple[ 0 ],
		-_tuple[ 1 ],
		-_tuple[ 2 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real>& DVec3<Real>::operator+= (const DVec3& vec)
{
	_tuple[ 0 ] += vec._tuple[ 0 ];
	_tuple[ 1 ] += vec._tuple[ 1 ];
	_tuple[ 2 ] += vec._tuple[ 2 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real>& DVec3<Real>::operator-= (const DVec3& vec)
{
	_tuple[ 0 ] -= vec._tuple[ 0 ];
	_tuple[ 1 ] -= vec._tuple[ 1 ];
	_tuple[ 2 ] -= vec._tuple[ 2 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real>& DVec3<Real>::operator*= (Real scalar)
{
	_tuple[ 0 ] *= scalar;
	_tuple[ 1 ] *= scalar;
	_tuple[ 2 ] *= scalar;
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec3<Real>& DVec3<Real>::operator/= (Real scalar)
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		_tuple[ 0 ] *= invScalar;
		_tuple[ 1 ] *= invScalar;
		_tuple[ 2 ] *= invScalar;
	}
	else
	{
		_tuple[ 0 ] *= Math<Real>::MAX_REAL;
		_tuple[ 1 ] *= Math<Real>::MAX_REAL;
		_tuple[ 2 ] *= Math<Real>::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::Length() const
{
	return DMath<Real>::Sqrt
		(
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ] +
		_tuple[ 2 ] * _tuple[ 2 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::SquaredLength() const
{
	return
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ] +
		_tuple[ 2 ] * _tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::Dot( const DVec3& vec ) const
{
	return
		_tuple[ 0 ] * vec._tuple[ 0 ] +
		_tuple[ 1 ] * vec._tuple[ 1 ] +
		_tuple[ 2 ] * vec._tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec3<Real>::Normalize( const Real epsilon )
{
	Real length = Length();

	if( length > epsilon )
	{
		Real invLength = ((Real)1) / length;
		_tuple[ 0 ] *= invLength;
		_tuple[ 1 ] *= invLength;
		_tuple[ 2 ] *= invLength;
	}
	else
	{
		length = (Real)0;
		_tuple[ 0 ] = (Real)0;
		_tuple[ 1 ] = (Real)0;
		_tuple[ 2 ] = (Real)0;
	}

	return length;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec3<Real> DVec3<Real>::Cross( const DVec3& vec ) const
{
	return DVec3
		(
		_tuple[ 1 ] * vec._tuple[ 2 ] - _tuple[ 2 ] * vec._tuple[ 1 ],
		_tuple[ 2 ] * vec._tuple[ 0 ] - _tuple[ 0 ] * vec._tuple[ 2 ],
		_tuple[ 0 ] * vec._tuple[ 1 ] - _tuple[ 1 ] * vec._tuple[ 0 ]
		);
}
