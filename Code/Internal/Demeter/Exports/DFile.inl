inline DFile::DFile( void ) : fstream( 0 ) {}
inline DFile::~DFile( void ) { if( fstream ) if( fstream->is_open() ) fstream->close(); delete fstream; }

inline void DFile::Open( const char* i_filename, const std::ios_base::openmode i_mode )
{
	fstream = new std::fstream( i_filename, i_mode );
}
inline void DFile::Close( void )
{
	fstream->close();
}
inline void DFile::Write( const char* str )
{
	*fstream << str;
}