// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)
// Modified for Demeter convenience
#pragma once

#include "DMath.h"

namespace Demeter
{
	template<typename Real>
	class DVec2 : public Tuple < 2, Real >
	{
	public:
		// Construction.
		DVec2();  // uninitialized
		DVec2( const DVec2& vec );
		DVec2( const Tuple<2, Real>& tuple );
		DVec2( Real x, Real y );

		// Assignment.
		DVec2& operator= (const DVec2& vec);
		DVec2& operator= (const Tuple<2, Real>& tuple);

		// Coordinate access.
		inline Real X() const;
		inline Real& X();
		inline Real Y() const;
		inline Real& Y();

		// Arithmetic operations.
		inline DVec2 operator+ (const DVec2& vec) const;
		inline DVec2 operator- (const DVec2& vec) const;
		inline DVec2 operator* (Real scalar) const;
		inline DVec2 operator/ (Real scalar) const;
		inline DVec2 operator- () const;

		// Arithmetic updates.
		inline DVec2& operator+= (const DVec2& vec);
		inline DVec2& operator-= (const DVec2& vec);
		inline DVec2& operator*= (Real scalar);
		inline DVec2& operator/= (Real scalar);

		// Vector operations.
		inline Real Length() const;
		inline Real SquaredLength() const;
		inline Real Dot( const DVec2& vec ) const;
		inline Real Normalize( const Real epsilon = DMath<Real>::ZERO_TOLERANCE );

		// Special vectors.
		static const DVec2 Zero;    // (0,0)
		static const DVec2 UnitX;  // (1,0)
		static const DVec2 UnitY;  // (0,1)
		static const DVec2 One;     // (1,1)

	protected:
		using Tuple<2, Real>::_tuple;
	};

#include "DVec2.inl"

	typedef DVec2<float> DVec2f;
	typedef DVec2<double> DVec2d;

	template<typename Real>
	class DVec3 : public Tuple < 3, Real >
	{
	public:
		//Construction
		DVec3( void ); // uninitialized
		DVec3( const DVec3& vec );
		DVec3( const Tuple<3, Real>& tuple );
		DVec3( const Real x, const Real y, const Real z );

		//Assignment
		DVec3& operator= (const DVec3& vec);
		DVec3& operator=(const Tuple < 3, Real>& tuple);

		//Coordinate access
		inline Real X( void ) const;
		inline Real& X( void );
		inline Real Y( void ) const;
		inline Real& Y( void );
		inline Real Z( void ) const;
		inline Real& Z( void );

		// Arithmetic operations
		inline DVec3 operator+(const DVec3& vec) const;
		inline DVec3 operator-(const DVec3& vec) const;
		inline DVec3 operator*(Real scalar) const;
		inline DVec3 operator/(Real scalar) const;
		inline DVec3 operator-(void) const;

		// Arithmetic updates
		inline DVec3& operator+= (const DVec3& vec);
		inline DVec3& operator-=(const DVec3& vec);
		inline DVec3& operator*=(Real scalar);
		inline DVec3& operator/=(Real scalar);

		//V3 operations
		inline Real Length() const;
		inline Real SquaredLength() const;
		inline Real Dot( const DVec3& vec ) const;
		inline Real Normalize( const Real epsilon = DMath<Real>::ZERO_TOLERANCE );

		//Right hand cross rule
		DVec3 Cross( const DVec3& vec ) const;

		//special vectors
		static const DVec3 Zero;
		static const DVec3 UnitX;
		static const DVec3 UnitY;
		static const DVec3 UnitZ;
		static const DVec3 One;

	protected:
		using Tuple<3, Real>::_tuple;
	};

#include "DVec3.inl"

	typedef DVec3<float> DVec3f;
	typedef DVec3<double> DVec3d;

	template<typename Real>
	class DVec4 : public Tuple < 4, Real >
	{
	public:
		// Construction.
		DVec4();  // uninitialized
		DVec4( const DVec4& vec );
		DVec4( const Tuple<4, Real>& tuple );
		DVec4( Real x, Real y, Real z, Real w );

		// Assignment.
		DVec4& operator= (const DVec4& vec);
		DVec4& operator= (const Tuple<4, Real>& tuple);

		// Coordinate access.
		inline Real X() const;
		inline Real& X();
		inline Real Y() const;
		inline Real& Y();
		inline Real Z() const;
		inline Real& Z();
		inline Real W() const;
		inline Real& W();

		// Arithmetic operations.
		inline DVec4 operator+ (const DVec4& vec) const;
		inline DVec4 operator- (const DVec4& vec) const;
		inline DVec4 operator* (Real scalar) const;
		inline DVec4 operator/ (Real scalar) const;
		inline DVec4 operator- () const;

		// Arithmetic updates.
		inline DVec4& operator+= (const DVec4& vec);
		inline DVec4& operator-= (const DVec4& vec);
		inline DVec4& operator*= (Real scalar);
		inline DVec4& operator/= (Real scalar);

		// Vector operations.
		inline Real Length() const;
		inline Real SquaredLength() const;
		inline Real Dot( const DVec4& vec ) const;
		inline Real Normalize( const Real epsilon = DMath<Real>::ZERO_TOLERANCE );

		// Compute the axis-aligned bounding box of the points.
		static void ComputeExtremes( int numVectors, const DVec4* vectors,
			DVec4& vmin, DVec4& vmax );

		// Special vectors.
		static const DVec4 Zero;
		static const DVec4 UnitX;  // (1,0,0,0)
		static const DVec4 UnitY;  // (0,1,0,0)
		static const DVec4 UnitZ;  // (0,0,1,0)
		static const DVec4 UnitW;  // (0,0,0,1)
		static const DVec4 One;     // (1,1,1,1)

	protected:
		using Tuple<4, Real>::_tuple;
	};

#include "DVec4.inl"

	typedef DVec4<float> DVec4f;
	typedef DVec4<double> DVec4d;
}