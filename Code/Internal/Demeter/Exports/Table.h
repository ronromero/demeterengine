// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

#pragma once

#include "Tuple.h"

namespace Demeter
{
	template <int NUMROWS, int NUMCOLS, typename TYPE>
	class Table
	{
	public:
		Table( void );
		Table( const Table& table );
		~Table( void );

		// Coordinate access.
		inline operator const TYPE* () const;
		inline operator TYPE* ();
		inline const TYPE* operator[] ( int row ) const;
		inline TYPE* operator[] ( int row );
		inline TYPE operator() ( int row, int col ) const;
		inline TYPE& operator() ( int row, int col );
		void SetRow( int row, const Tuple<NUMROWS, TYPE>& tuple );
		Tuple<NUMROWS, TYPE> GetRow( int row ) const;
		void SetColumn( int col, const Tuple<NUMCOLS, TYPE>& tuple );
		Tuple<NUMCOLS, TYPE> GetColumn( int col ) const;

		// Assignment.
		Table& operator= (const Table& table);

		// Comparison.  The inequalities make the comparisons using memcmp, thus
		// treating the tuple as an array of unsigned bytes.
		bool operator== (const Table& table) const;
		bool operator!= (const Table& table) const;
		bool operator<  (const Table& table) const;
		bool operator<= (const Table& table) const;
		bool operator>  (const Table& table) const;
		bool operator>= (const Table& table) const;

	protected:
		// The array is stored in row-major order.
		enum { NUMENTRIES = NUMROWS*NUMCOLS };
		TYPE _entry[ NUMENTRIES ];
	};

#include "Table.inl"
}