#ifndef _DEMETER_DIRECTIONALLIGHT_H_
#define _DEMETER_DIRECTIONALLIGHT_H_

#include "DGameObject.h"
#include <d3d9.h>
#include <d3dx9.h>
#include "DVector.h"

namespace Demeter
{
	class Light : public DGameObject
	{
	public:
		D3DXVECTOR3 _ambientColor;
		D3DXVECTOR3 _diffuseColor;
		D3DXVECTOR3 _diffuseDirection;

		Light( const float *iAmbientColor, const float *iDiffuseColor, const DVec3f iDiffuseDirection );
		~Light( void );
	};
}

#endif