// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>::DVec4()
{
	// Uninitialized for performance in array construction.
}
//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>::DVec4( const DVec4& vec )
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
	_tuple[ 2 ] = vec._tuple[ 2 ];
	_tuple[ 3 ] = vec._tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>::DVec4( const Tuple<4, Real>& tuple )
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
	_tuple[ 2 ] = tuple[ 2 ];
	_tuple[ 3 ] = tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>::DVec4( Real x, Real y, Real z, Real w )
{
	_tuple[ 0 ] = x;
	_tuple[ 1 ] = y;
	_tuple[ 2 ] = z;
	_tuple[ 3 ] = w;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>& DVec4<Real>::operator= (const DVec4& vec)
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
	_tuple[ 2 ] = vec._tuple[ 2 ];
	_tuple[ 3 ] = vec._tuple[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec4<Real>& DVec4<Real>::operator= (const Tuple<4, Real>& tuple)
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
	_tuple[ 2 ] = tuple[ 2 ];
	_tuple[ 3 ] = tuple[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::X() const
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec4<Real>::X()
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::Y() const
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec4<Real>::Y()
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::Z() const
{
	return _tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec4<Real>::Z()
{
	return _tuple[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::W() const
{
	return _tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec4<Real>::W()
{
	return _tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> DVec4<Real>::operator+ (const DVec4& vec) const
{
	return DVec4
		(
		_tuple[ 0 ] + vec._tuple[ 0 ],
		_tuple[ 1 ] + vec._tuple[ 1 ],
		_tuple[ 2 ] + vec._tuple[ 2 ],
		_tuple[ 3 ] + vec._tuple[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> DVec4<Real>::operator- (const DVec4& vec) const
{
	return DVec4
		(
		_tuple[ 0 ] - vec._tuple[ 0 ],
		_tuple[ 1 ] - vec._tuple[ 1 ],
		_tuple[ 2 ] - vec._tuple[ 2 ],
		_tuple[ 3 ] - vec._tuple[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> DVec4<Real>::operator* (Real scalar) const
{
	return DVec4
		(
		scalar*_tuple[ 0 ],
		scalar*_tuple[ 1 ],
		scalar*_tuple[ 2 ],
		scalar*_tuple[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> DVec4<Real>::operator/ (Real scalar) const
{
	DVec4 result;

	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		result._tuple[ 0 ] = invScalar*_tuple[ 0 ];
		result._tuple[ 1 ] = invScalar*_tuple[ 1 ];
		result._tuple[ 2 ] = invScalar*_tuple[ 2 ];
		result._tuple[ 3 ] = invScalar*_tuple[ 3 ];
	}
	else
	{
		result._tuple[ 0 ] = Math<Real>::MAX_REAL;
		result._tuple[ 1 ] = Math<Real>::MAX_REAL;
		result._tuple[ 2 ] = Math<Real>::MAX_REAL;
		result._tuple[ 3 ] = Math<Real>::MAX_REAL;
	}

	return result;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> DVec4<Real>::operator- () const
{
	return DVec4
		(
		-_tuple[ 0 ],
		-_tuple[ 1 ],
		-_tuple[ 2 ],
		-_tuple[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real>& DVec4<Real>::operator+= (const DVec4& vec)
{
	_tuple[ 0 ] += vec._tuple[ 0 ];
	_tuple[ 1 ] += vec._tuple[ 1 ];
	_tuple[ 2 ] += vec._tuple[ 2 ];
	_tuple[ 3 ] += vec._tuple[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real>& DVec4<Real>::operator-= (const DVec4& vec)
{
	_tuple[ 0 ] -= vec._tuple[ 0 ];
	_tuple[ 1 ] -= vec._tuple[ 1 ];
	_tuple[ 2 ] -= vec._tuple[ 2 ];
	_tuple[ 3 ] -= vec._tuple[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real>& DVec4<Real>::operator*= (Real scalar)
{
	_tuple[ 0 ] *= scalar;
	_tuple[ 1 ] *= scalar;
	_tuple[ 2 ] *= scalar;
	_tuple[ 3 ] *= scalar;
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real>& DVec4<Real>::operator/= (Real scalar)
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		_tuple[ 0 ] *= invScalar;
		_tuple[ 1 ] *= invScalar;
		_tuple[ 2 ] *= invScalar;
		_tuple[ 3 ] *= invScalar;
	}
	else
	{
		_tuple[ 0 ] *= Math<Real>::MAX_REAL;
		_tuple[ 1 ] *= Math<Real>::MAX_REAL;
		_tuple[ 2 ] *= Math<Real>::MAX_REAL;
		_tuple[ 3 ] *= Math<Real>::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::Length() const
{
	return Math<Real>::Sqrt
		(
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ] +
		_tuple[ 2 ] * _tuple[ 2 ] +
		_tuple[ 3 ] * _tuple[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::SquaredLength() const
{
	return
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ] +
		_tuple[ 2 ] * _tuple[ 2 ] +
		_tuple[ 3 ] * _tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::Dot( const DVec4& vec ) const
{
	return
		_tuple[ 0 ] * vec._tuple[ 0 ] +
		_tuple[ 1 ] * vec._tuple[ 1 ] +
		_tuple[ 2 ] * vec._tuple[ 2 ] +
		_tuple[ 3 ] * vec._tuple[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec4<Real>::Normalize( const Real epsilon )
{
	Real length = Length();

	if( length > epsilon )
	{
		Real invLength = ((Real)1) / length;
		_tuple[ 0 ] *= invLength;
		_tuple[ 1 ] *= invLength;
		_tuple[ 2 ] *= invLength;
		_tuple[ 3 ] *= invLength;
	}
	else
	{
		length = (Real)0;
		_tuple[ 0 ] = (Real)0;
		_tuple[ 1 ] = (Real)0;
		_tuple[ 2 ] = (Real)0;
		_tuple[ 3 ] = (Real)0;
	}

	return length;
}
//----------------------------------------------------------------------------
template <typename Real>
void DVec4<Real>::ComputeExtremes( int numVectors, const DVec4* vectors,
	DVec4& vmin, DVec4& vmax )
{
	assertion( numVectors > 0 && vectors,
		"Invalid inputs to ComputeExtremes\n" );

	vmin = vectors[ 0 ];
	vmax = vmin;
	for( int j = 1; j < numVectors; ++j )
	{
		const DVec4& vec = vectors[ j ];
		for( int i = 0; i < 4; ++i )
		{
			if( vec[ i ] < vmin[ i ] )
			{
				vmin[ i ] = vec[ i ];
			}
			else if( vec[ i ] > vmax[ i ] )
			{
				vmax[ i ] = vec[ i ];
			}
		}
	}
}