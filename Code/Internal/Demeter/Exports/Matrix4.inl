// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real>::Matrix4( bool makeZero )
{
	if( makeZero )
	{
		MakeZero();
	}
	else
	{
		MakeIdentity();
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real>::Matrix4( const Matrix4& mat )
{
	_entry[ 0 ] = mat._entry[ 0 ];
	_entry[ 1 ] = mat._entry[ 1 ];
	_entry[ 2 ] = mat._entry[ 2 ];
	_entry[ 3 ] = mat._entry[ 3 ];
	_entry[ 4 ] = mat._entry[ 4 ];
	_entry[ 5 ] = mat._entry[ 5 ];
	_entry[ 6 ] = mat._entry[ 6 ];
	_entry[ 7 ] = mat._entry[ 7 ];
	_entry[ 8 ] = mat._entry[ 8 ];
	_entry[ 9 ] = mat._entry[ 9 ];
	_entry[ 10 ] = mat._entry[ 10 ];
	_entry[ 11 ] = mat._entry[ 11 ];
	_entry[ 12 ] = mat._entry[ 12 ];
	_entry[ 13 ] = mat._entry[ 13 ];
	_entry[ 14 ] = mat._entry[ 14 ];
	_entry[ 15 ] = mat._entry[ 15 ];
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real>::Matrix4( Real m00, Real m01, Real m02, Real m03, Real m10,
	Real m11, Real m12, Real m13, Real m20, Real m21, Real m22, Real m23,
	Real m30, Real m31, Real m32, Real m33 )
{
	_entry[ 0 ] = m00;
	_entry[ 1 ] = m01;
	_entry[ 2 ] = m02;
	_entry[ 3 ] = m03;
	_entry[ 4 ] = m10;
	_entry[ 5 ] = m11;
	_entry[ 6 ] = m12;
	_entry[ 7 ] = m13;
	_entry[ 8 ] = m20;
	_entry[ 9 ] = m21;
	_entry[ 10 ] = m22;
	_entry[ 11 ] = m23;
	_entry[ 12 ] = m30;
	_entry[ 13 ] = m31;
	_entry[ 14 ] = m32;
	_entry[ 15 ] = m33;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real>::Matrix4( const Real entry[ 16 ], bool rowMajor )
{
	if( rowMajor )
	{
		_entry[ 0 ] = entry[ 0 ];
		_entry[ 1 ] = entry[ 1 ];
		_entry[ 2 ] = entry[ 2 ];
		_entry[ 3 ] = entry[ 3 ];
		_entry[ 4 ] = entry[ 4 ];
		_entry[ 5 ] = entry[ 5 ];
		_entry[ 6 ] = entry[ 6 ];
		_entry[ 7 ] = entry[ 7 ];
		_entry[ 8 ] = entry[ 8 ];
		_entry[ 9 ] = entry[ 9 ];
		_entry[ 10 ] = entry[ 10 ];
		_entry[ 11 ] = entry[ 11 ];
		_entry[ 12 ] = entry[ 12 ];
		_entry[ 13 ] = entry[ 13 ];
		_entry[ 14 ] = entry[ 14 ];
		_entry[ 15 ] = entry[ 15 ];
	}
	else
	{
		_entry[ 0 ] = entry[ 0 ];
		_entry[ 1 ] = entry[ 4 ];
		_entry[ 2 ] = entry[ 8 ];
		_entry[ 3 ] = entry[ 12 ];
		_entry[ 4 ] = entry[ 1 ];
		_entry[ 5 ] = entry[ 5 ];
		_entry[ 6 ] = entry[ 9 ];
		_entry[ 7 ] = entry[ 13 ];
		_entry[ 8 ] = entry[ 2 ];
		_entry[ 9 ] = entry[ 6 ];
		_entry[ 10 ] = entry[ 10 ];
		_entry[ 11 ] = entry[ 14 ];
		_entry[ 12 ] = entry[ 3 ];
		_entry[ 13 ] = entry[ 7 ];
		_entry[ 14 ] = entry[ 11 ];
		_entry[ 15 ] = entry[ 15 ];
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real>& Matrix4<Real>::operator= (const Matrix4& mat)
{
	_entry[ 0 ] = mat._entry[ 0 ];
	_entry[ 1 ] = mat._entry[ 1 ];
	_entry[ 2 ] = mat._entry[ 2 ];
	_entry[ 3 ] = mat._entry[ 3 ];
	_entry[ 4 ] = mat._entry[ 4 ];
	_entry[ 5 ] = mat._entry[ 5 ];
	_entry[ 6 ] = mat._entry[ 6 ];
	_entry[ 7 ] = mat._entry[ 7 ];
	_entry[ 8 ] = mat._entry[ 8 ];
	_entry[ 9 ] = mat._entry[ 9 ];
	_entry[ 10 ] = mat._entry[ 10 ];
	_entry[ 11 ] = mat._entry[ 11 ];
	_entry[ 12 ] = mat._entry[ 12 ];
	_entry[ 13 ] = mat._entry[ 13 ];
	_entry[ 14 ] = mat._entry[ 14 ];
	_entry[ 15 ] = mat._entry[ 15 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix4<Real>::MakeZero()
{
	_entry[ 0 ] = (Real)0;
	_entry[ 1 ] = (Real)0;
	_entry[ 2 ] = (Real)0;
	_entry[ 3 ] = (Real)0;
	_entry[ 4 ] = (Real)0;
	_entry[ 5 ] = (Real)0;
	_entry[ 6 ] = (Real)0;
	_entry[ 7 ] = (Real)0;
	_entry[ 8 ] = (Real)0;
	_entry[ 9 ] = (Real)0;
	_entry[ 10 ] = (Real)0;
	_entry[ 11 ] = (Real)0;
	_entry[ 12 ] = (Real)0;
	_entry[ 13 ] = (Real)0;
	_entry[ 14 ] = (Real)0;
	_entry[ 15 ] = (Real)0;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix4<Real>::MakeIdentity()
{
	_entry[ 0 ] = (Real)1;
	_entry[ 1 ] = (Real)0;
	_entry[ 2 ] = (Real)0;
	_entry[ 3 ] = (Real)0;
	_entry[ 4 ] = (Real)0;
	_entry[ 5 ] = (Real)1;
	_entry[ 6 ] = (Real)0;
	_entry[ 7 ] = (Real)0;
	_entry[ 8 ] = (Real)0;
	_entry[ 9 ] = (Real)0;
	_entry[ 10 ] = (Real)1;
	_entry[ 11 ] = (Real)0;
	_entry[ 12 ] = (Real)0;
	_entry[ 13 ] = (Real)0;
	_entry[ 14 ] = (Real)0;
	_entry[ 15 ] = (Real)1;
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator+ (const Matrix4& mat) const
{
	return Matrix4<Real>
		(
		_entry[ 0 ] + mat._entry[ 0 ],
		_entry[ 1 ] + mat._entry[ 1 ],
		_entry[ 2 ] + mat._entry[ 2 ],
		_entry[ 3 ] + mat._entry[ 3 ],
		_entry[ 4 ] + mat._entry[ 4 ],
		_entry[ 5 ] + mat._entry[ 5 ],
		_entry[ 6 ] + mat._entry[ 6 ],
		_entry[ 7 ] + mat._entry[ 7 ],
		_entry[ 8 ] + mat._entry[ 8 ],
		_entry[ 9 ] + mat._entry[ 9 ],
		_entry[ 10 ] + mat._entry[ 10 ],
		_entry[ 11 ] + mat._entry[ 11 ],
		_entry[ 12 ] + mat._entry[ 12 ],
		_entry[ 13 ] + mat._entry[ 13 ],
		_entry[ 14 ] + mat._entry[ 14 ],
		_entry[ 15 ] + mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator- (const Matrix4& mat) const
{
	return Matrix4<Real>
		(
		_entry[ 0 ] - mat._entry[ 0 ],
		_entry[ 1 ] - mat._entry[ 1 ],
		_entry[ 2 ] - mat._entry[ 2 ],
		_entry[ 3 ] - mat._entry[ 3 ],
		_entry[ 4 ] - mat._entry[ 4 ],
		_entry[ 5 ] - mat._entry[ 5 ],
		_entry[ 6 ] - mat._entry[ 6 ],
		_entry[ 7 ] - mat._entry[ 7 ],
		_entry[ 8 ] - mat._entry[ 8 ],
		_entry[ 9 ] - mat._entry[ 9 ],
		_entry[ 10 ] - mat._entry[ 10 ],
		_entry[ 11 ] - mat._entry[ 11 ],
		_entry[ 12 ] - mat._entry[ 12 ],
		_entry[ 13 ] - mat._entry[ 13 ],
		_entry[ 14 ] - mat._entry[ 14 ],
		_entry[ 15 ] - mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator* (Real scalar) const
{
	return Matrix4<Real>
		(
		scalar*_entry[ 0 ],
		scalar*_entry[ 1 ],
		scalar*_entry[ 2 ],
		scalar*_entry[ 3 ],
		scalar*_entry[ 4 ],
		scalar*_entry[ 5 ],
		scalar*_entry[ 6 ],
		scalar*_entry[ 7 ],
		scalar*_entry[ 8 ],
		scalar*_entry[ 9 ],
		scalar*_entry[ 10 ],
		scalar*_entry[ 11 ],
		scalar*_entry[ 12 ],
		scalar*_entry[ 13 ],
		scalar*_entry[ 14 ],
		scalar*_entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator/ (Real scalar) const
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		return Matrix4<Real>
			(
			invScalar*_entry[ 0 ],
			invScalar*_entry[ 1 ],
			invScalar*_entry[ 2 ],
			invScalar*_entry[ 3 ],
			invScalar*_entry[ 4 ],
			invScalar*_entry[ 5 ],
			invScalar*_entry[ 6 ],
			invScalar*_entry[ 7 ],
			invScalar*_entry[ 8 ],
			invScalar*_entry[ 9 ],
			invScalar*_entry[ 10 ],
			invScalar*_entry[ 11 ],
			invScalar*_entry[ 12 ],
			invScalar*_entry[ 13 ],
			invScalar*_entry[ 14 ],
			invScalar*_entry[ 15 ]
			);
	}
	else
	{
		return Matrix4<Real>
			(
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL,
			Math<Real>::MAX_REAL
			);
	}
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator- () const
{
	return Matrix4<Real>
		(
		-_entry[ 0 ],
		-_entry[ 1 ],
		-_entry[ 2 ],
		-_entry[ 3 ],
		-_entry[ 4 ],
		-_entry[ 5 ],
		-_entry[ 6 ],
		-_entry[ 7 ],
		-_entry[ 8 ],
		-_entry[ 9 ],
		-_entry[ 10 ],
		-_entry[ 11 ],
		-_entry[ 12 ],
		-_entry[ 13 ],
		-_entry[ 14 ],
		-_entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real>& Matrix4<Real>::operator+= (const Matrix4& mat)
{
	_entry[ 0 ] += mat._entry[ 0 ];
	_entry[ 1 ] += mat._entry[ 1 ];
	_entry[ 2 ] += mat._entry[ 2 ];
	_entry[ 3 ] += mat._entry[ 3 ];
	_entry[ 4 ] += mat._entry[ 4 ];
	_entry[ 5 ] += mat._entry[ 5 ];
	_entry[ 6 ] += mat._entry[ 6 ];
	_entry[ 7 ] += mat._entry[ 7 ];
	_entry[ 8 ] += mat._entry[ 8 ];
	_entry[ 9 ] += mat._entry[ 9 ];
	_entry[ 10 ] += mat._entry[ 10 ];
	_entry[ 11 ] += mat._entry[ 11 ];
	_entry[ 12 ] += mat._entry[ 12 ];
	_entry[ 13 ] += mat._entry[ 13 ];
	_entry[ 14 ] += mat._entry[ 14 ];
	_entry[ 15 ] += mat._entry[ 15 ];
	return *this;
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real>& Matrix4<Real>::operator-= (const Matrix4& mat)
{
	_entry[ 0 ] -= mat._entry[ 0 ];
	_entry[ 1 ] -= mat._entry[ 1 ];
	_entry[ 2 ] -= mat._entry[ 2 ];
	_entry[ 3 ] -= mat._entry[ 3 ];
	_entry[ 4 ] -= mat._entry[ 4 ];
	_entry[ 5 ] -= mat._entry[ 5 ];
	_entry[ 6 ] -= mat._entry[ 6 ];
	_entry[ 7 ] -= mat._entry[ 7 ];
	_entry[ 8 ] -= mat._entry[ 8 ];
	_entry[ 9 ] -= mat._entry[ 9 ];
	_entry[ 10 ] -= mat._entry[ 10 ];
	_entry[ 11 ] -= mat._entry[ 11 ];
	_entry[ 12 ] -= mat._entry[ 12 ];
	_entry[ 13 ] -= mat._entry[ 13 ];
	_entry[ 14 ] -= mat._entry[ 14 ];
	_entry[ 15 ] -= mat._entry[ 15 ];
	return *this;
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real>& Matrix4<Real>::operator*= (Real scalar)
{
	_entry[ 0 ] *= scalar;
	_entry[ 1 ] *= scalar;
	_entry[ 2 ] *= scalar;
	_entry[ 3 ] *= scalar;
	_entry[ 4 ] *= scalar;
	_entry[ 5 ] *= scalar;
	_entry[ 6 ] *= scalar;
	_entry[ 7 ] *= scalar;
	_entry[ 8 ] *= scalar;
	_entry[ 9 ] *= scalar;
	_entry[ 10 ] *= scalar;
	_entry[ 11 ] *= scalar;
	_entry[ 12 ] *= scalar;
	_entry[ 13 ] *= scalar;
	_entry[ 14 ] *= scalar;
	_entry[ 15 ] *= scalar;
	return *this;
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real>& Matrix4<Real>::operator/= (Real scalar)
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		_entry[ 0 ] *= invScalar;
		_entry[ 1 ] *= invScalar;
		_entry[ 2 ] *= invScalar;
		_entry[ 3 ] *= invScalar;
		_entry[ 4 ] *= invScalar;
		_entry[ 5 ] *= invScalar;
		_entry[ 6 ] *= invScalar;
		_entry[ 7 ] *= invScalar;
		_entry[ 8 ] *= invScalar;
		_entry[ 9 ] *= invScalar;
		_entry[ 10 ] *= invScalar;
		_entry[ 11 ] *= invScalar;
		_entry[ 12 ] *= invScalar;
		_entry[ 13 ] *= invScalar;
		_entry[ 14 ] *= invScalar;
		_entry[ 15 ] *= invScalar;
	}
	else
	{
		_entry[ 0 ] = Math<Real>::MAX_REAL;
		_entry[ 1 ] = Math<Real>::MAX_REAL;
		_entry[ 2 ] = Math<Real>::MAX_REAL;
		_entry[ 3 ] = Math<Real>::MAX_REAL;
		_entry[ 4 ] = Math<Real>::MAX_REAL;
		_entry[ 5 ] = Math<Real>::MAX_REAL;
		_entry[ 6 ] = Math<Real>::MAX_REAL;
		_entry[ 7 ] = Math<Real>::MAX_REAL;
		_entry[ 8 ] = Math<Real>::MAX_REAL;
		_entry[ 9 ] = Math<Real>::MAX_REAL;
		_entry[ 10 ] = Math<Real>::MAX_REAL;
		_entry[ 11 ] = Math<Real>::MAX_REAL;
		_entry[ 12 ] = Math<Real>::MAX_REAL;
		_entry[ 13 ] = Math<Real>::MAX_REAL;
		_entry[ 14 ] = Math<Real>::MAX_REAL;
		_entry[ 15 ] = Math<Real>::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
template <class Real>
inline DVec4<Real> Matrix4<Real>::operator* (const DVec4<Real>& vec) const
{
	return DVec4<Real>
		(
		_entry[ 0 ] * vec[ 0 ] +
		_entry[ 1 ] * vec[ 1 ] +
		_entry[ 2 ] * vec[ 2 ] +
		_entry[ 3 ] * vec[ 3 ],

		_entry[ 4 ] * vec[ 0 ] +
		_entry[ 5 ] * vec[ 1 ] +
		_entry[ 6 ] * vec[ 2 ] +
		_entry[ 7 ] * vec[ 3 ],

		_entry[ 8 ] * vec[ 0 ] +
		_entry[ 9 ] * vec[ 1 ] +
		_entry[ 10 ] * vec[ 2 ] +
		_entry[ 11 ] * vec[ 3 ],

		_entry[ 12 ] * vec[ 0 ] +
		_entry[ 13 ] * vec[ 1 ] +
		_entry[ 14 ] * vec[ 2 ] +
		_entry[ 15 ] * vec[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
Matrix4<Real> Matrix4<Real>::Transpose() const
{
	return Matrix4<Real>
		(
		_entry[ 0 ],
		_entry[ 4 ],
		_entry[ 8 ],
		_entry[ 12 ],
		_entry[ 1 ],
		_entry[ 5 ],
		_entry[ 9 ],
		_entry[ 13 ],
		_entry[ 2 ],
		_entry[ 6 ],
		_entry[ 10 ],
		_entry[ 14 ],
		_entry[ 3 ],
		_entry[ 7 ],
		_entry[ 11 ],
		_entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
inline Matrix4<Real> Matrix4<Real>::operator* (const Matrix4& mat) const
{
	// A*B
	return Matrix4<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] +
		_entry[ 1 ] * mat._entry[ 4 ] +
		_entry[ 2 ] * mat._entry[ 8 ] +
		_entry[ 3 ] * mat._entry[ 12 ],

		_entry[ 0 ] * mat._entry[ 1 ] +
		_entry[ 1 ] * mat._entry[ 5 ] +
		_entry[ 2 ] * mat._entry[ 9 ] +
		_entry[ 3 ] * mat._entry[ 13 ],

		_entry[ 0 ] * mat._entry[ 2 ] +
		_entry[ 1 ] * mat._entry[ 6 ] +
		_entry[ 2 ] * mat._entry[ 10 ] +
		_entry[ 3 ] * mat._entry[ 14 ],

		_entry[ 0 ] * mat._entry[ 3 ] +
		_entry[ 1 ] * mat._entry[ 7 ] +
		_entry[ 2 ] * mat._entry[ 11 ] +
		_entry[ 3 ] * mat._entry[ 15 ],

		_entry[ 4 ] * mat._entry[ 0 ] +
		_entry[ 5 ] * mat._entry[ 4 ] +
		_entry[ 6 ] * mat._entry[ 8 ] +
		_entry[ 7 ] * mat._entry[ 12 ],

		_entry[ 4 ] * mat._entry[ 1 ] +
		_entry[ 5 ] * mat._entry[ 5 ] +
		_entry[ 6 ] * mat._entry[ 9 ] +
		_entry[ 7 ] * mat._entry[ 13 ],

		_entry[ 4 ] * mat._entry[ 2 ] +
		_entry[ 5 ] * mat._entry[ 6 ] +
		_entry[ 6 ] * mat._entry[ 10 ] +
		_entry[ 7 ] * mat._entry[ 14 ],

		_entry[ 4 ] * mat._entry[ 3 ] +
		_entry[ 5 ] * mat._entry[ 7 ] +
		_entry[ 6 ] * mat._entry[ 11 ] +
		_entry[ 7 ] * mat._entry[ 15 ],

		_entry[ 8 ] * mat._entry[ 0 ] +
		_entry[ 9 ] * mat._entry[ 4 ] +
		_entry[ 10 ] * mat._entry[ 8 ] +
		_entry[ 11 ] * mat._entry[ 12 ],

		_entry[ 8 ] * mat._entry[ 1 ] +
		_entry[ 9 ] * mat._entry[ 5 ] +
		_entry[ 10 ] * mat._entry[ 9 ] +
		_entry[ 11 ] * mat._entry[ 13 ],

		_entry[ 8 ] * mat._entry[ 2 ] +
		_entry[ 9 ] * mat._entry[ 6 ] +
		_entry[ 10 ] * mat._entry[ 10 ] +
		_entry[ 11 ] * mat._entry[ 14 ],

		_entry[ 8 ] * mat._entry[ 3 ] +
		_entry[ 9 ] * mat._entry[ 7 ] +
		_entry[ 10 ] * mat._entry[ 11 ] +
		_entry[ 11 ] * mat._entry[ 15 ],

		_entry[ 12 ] * mat._entry[ 0 ] +
		_entry[ 13 ] * mat._entry[ 4 ] +
		_entry[ 14 ] * mat._entry[ 8 ] +
		_entry[ 15 ] * mat._entry[ 12 ],

		_entry[ 12 ] * mat._entry[ 1 ] +
		_entry[ 13 ] * mat._entry[ 5 ] +
		_entry[ 14 ] * mat._entry[ 9 ] +
		_entry[ 15 ] * mat._entry[ 13 ],

		_entry[ 12 ] * mat._entry[ 2 ] +
		_entry[ 13 ] * mat._entry[ 6 ] +
		_entry[ 14 ] * mat._entry[ 10 ] +
		_entry[ 15 ] * mat._entry[ 14 ],

		_entry[ 12 ] * mat._entry[ 3 ] +
		_entry[ 13 ] * mat._entry[ 7 ] +
		_entry[ 14 ] * mat._entry[ 11 ] +
		_entry[ 15 ] * mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
Matrix4<Real> Matrix4<Real>::TransposeTimes( const Matrix4& mat ) const
{
	// A^T*B
	return Matrix4<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] +
		_entry[ 4 ] * mat._entry[ 4 ] +
		_entry[ 8 ] * mat._entry[ 8 ] +
		_entry[ 12 ] * mat._entry[ 12 ],

		_entry[ 0 ] * mat._entry[ 1 ] +
		_entry[ 4 ] * mat._entry[ 5 ] +
		_entry[ 8 ] * mat._entry[ 9 ] +
		_entry[ 12 ] * mat._entry[ 13 ],

		_entry[ 0 ] * mat._entry[ 2 ] +
		_entry[ 4 ] * mat._entry[ 6 ] +
		_entry[ 8 ] * mat._entry[ 10 ] +
		_entry[ 12 ] * mat._entry[ 14 ],

		_entry[ 0 ] * mat._entry[ 3 ] +
		_entry[ 4 ] * mat._entry[ 7 ] +
		_entry[ 8 ] * mat._entry[ 11 ] +
		_entry[ 12 ] * mat._entry[ 15 ],

		_entry[ 1 ] * mat._entry[ 0 ] +
		_entry[ 5 ] * mat._entry[ 4 ] +
		_entry[ 9 ] * mat._entry[ 8 ] +
		_entry[ 13 ] * mat._entry[ 12 ],

		_entry[ 1 ] * mat._entry[ 1 ] +
		_entry[ 5 ] * mat._entry[ 5 ] +
		_entry[ 9 ] * mat._entry[ 9 ] +
		_entry[ 13 ] * mat._entry[ 13 ],

		_entry[ 1 ] * mat._entry[ 2 ] +
		_entry[ 5 ] * mat._entry[ 6 ] +
		_entry[ 9 ] * mat._entry[ 10 ] +
		_entry[ 13 ] * mat._entry[ 14 ],

		_entry[ 1 ] * mat._entry[ 3 ] +
		_entry[ 5 ] * mat._entry[ 7 ] +
		_entry[ 9 ] * mat._entry[ 11 ] +
		_entry[ 13 ] * mat._entry[ 15 ],

		_entry[ 2 ] * mat._entry[ 0 ] +
		_entry[ 6 ] * mat._entry[ 4 ] +
		_entry[ 10 ] * mat._entry[ 8 ] +
		_entry[ 14 ] * mat._entry[ 12 ],

		_entry[ 2 ] * mat._entry[ 1 ] +
		_entry[ 6 ] * mat._entry[ 5 ] +
		_entry[ 10 ] * mat._entry[ 9 ] +
		_entry[ 14 ] * mat._entry[ 13 ],

		_entry[ 2 ] * mat._entry[ 2 ] +
		_entry[ 6 ] * mat._entry[ 6 ] +
		_entry[ 10 ] * mat._entry[ 10 ] +
		_entry[ 14 ] * mat._entry[ 14 ],

		_entry[ 2 ] * mat._entry[ 3 ] +
		_entry[ 6 ] * mat._entry[ 7 ] +
		_entry[ 10 ] * mat._entry[ 11 ] +
		_entry[ 14 ] * mat._entry[ 15 ],

		_entry[ 3 ] * mat._entry[ 0 ] +
		_entry[ 7 ] * mat._entry[ 4 ] +
		_entry[ 11 ] * mat._entry[ 8 ] +
		_entry[ 15 ] * mat._entry[ 12 ],

		_entry[ 3 ] * mat._entry[ 1 ] +
		_entry[ 7 ] * mat._entry[ 5 ] +
		_entry[ 11 ] * mat._entry[ 9 ] +
		_entry[ 15 ] * mat._entry[ 13 ],

		_entry[ 3 ] * mat._entry[ 2 ] +
		_entry[ 7 ] * mat._entry[ 6 ] +
		_entry[ 11 ] * mat._entry[ 10 ] +
		_entry[ 15 ] * mat._entry[ 14 ],

		_entry[ 3 ] * mat._entry[ 3 ] +
		_entry[ 7 ] * mat._entry[ 7 ] +
		_entry[ 11 ] * mat._entry[ 11 ] +
		_entry[ 15 ] * mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
Matrix4<Real> Matrix4<Real>::TimesTranspose( const Matrix4& mat ) const
{
	// A*B^T
	return Matrix4<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] +
		_entry[ 1 ] * mat._entry[ 1 ] +
		_entry[ 2 ] * mat._entry[ 2 ] +
		_entry[ 3 ] * mat._entry[ 3 ],

		_entry[ 0 ] * mat._entry[ 4 ] +
		_entry[ 1 ] * mat._entry[ 5 ] +
		_entry[ 2 ] * mat._entry[ 6 ] +
		_entry[ 3 ] * mat._entry[ 7 ],

		_entry[ 0 ] * mat._entry[ 8 ] +
		_entry[ 1 ] * mat._entry[ 9 ] +
		_entry[ 2 ] * mat._entry[ 10 ] +
		_entry[ 3 ] * mat._entry[ 11 ],

		_entry[ 0 ] * mat._entry[ 12 ] +
		_entry[ 1 ] * mat._entry[ 13 ] +
		_entry[ 2 ] * mat._entry[ 14 ] +
		_entry[ 3 ] * mat._entry[ 15 ],

		_entry[ 4 ] * mat._entry[ 0 ] +
		_entry[ 5 ] * mat._entry[ 1 ] +
		_entry[ 6 ] * mat._entry[ 2 ] +
		_entry[ 7 ] * mat._entry[ 3 ],

		_entry[ 4 ] * mat._entry[ 4 ] +
		_entry[ 5 ] * mat._entry[ 5 ] +
		_entry[ 6 ] * mat._entry[ 6 ] +
		_entry[ 7 ] * mat._entry[ 7 ],

		_entry[ 4 ] * mat._entry[ 8 ] +
		_entry[ 5 ] * mat._entry[ 9 ] +
		_entry[ 6 ] * mat._entry[ 10 ] +
		_entry[ 7 ] * mat._entry[ 11 ],

		_entry[ 4 ] * mat._entry[ 12 ] +
		_entry[ 5 ] * mat._entry[ 13 ] +
		_entry[ 6 ] * mat._entry[ 14 ] +
		_entry[ 7 ] * mat._entry[ 15 ],

		_entry[ 8 ] * mat._entry[ 0 ] +
		_entry[ 9 ] * mat._entry[ 1 ] +
		_entry[ 10 ] * mat._entry[ 2 ] +
		_entry[ 11 ] * mat._entry[ 3 ],

		_entry[ 8 ] * mat._entry[ 4 ] +
		_entry[ 9 ] * mat._entry[ 5 ] +
		_entry[ 10 ] * mat._entry[ 6 ] +
		_entry[ 11 ] * mat._entry[ 7 ],

		_entry[ 8 ] * mat._entry[ 8 ] +
		_entry[ 9 ] * mat._entry[ 9 ] +
		_entry[ 10 ] * mat._entry[ 10 ] +
		_entry[ 11 ] * mat._entry[ 11 ],

		_entry[ 8 ] * mat._entry[ 12 ] +
		_entry[ 9 ] * mat._entry[ 13 ] +
		_entry[ 10 ] * mat._entry[ 14 ] +
		_entry[ 11 ] * mat._entry[ 15 ],

		_entry[ 12 ] * mat._entry[ 0 ] +
		_entry[ 13 ] * mat._entry[ 1 ] +
		_entry[ 14 ] * mat._entry[ 2 ] +
		_entry[ 15 ] * mat._entry[ 3 ],

		_entry[ 12 ] * mat._entry[ 4 ] +
		_entry[ 13 ] * mat._entry[ 5 ] +
		_entry[ 14 ] * mat._entry[ 6 ] +
		_entry[ 15 ] * mat._entry[ 7 ],

		_entry[ 12 ] * mat._entry[ 8 ] +
		_entry[ 13 ] * mat._entry[ 9 ] +
		_entry[ 14 ] * mat._entry[ 10 ] +
		_entry[ 15 ] * mat._entry[ 11 ],

		_entry[ 12 ] * mat._entry[ 12 ] +
		_entry[ 13 ] * mat._entry[ 13 ] +
		_entry[ 14 ] * mat._entry[ 14 ] +
		_entry[ 15 ] * mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <class Real>
Matrix4<Real> Matrix4<Real>::TransposeTimesTranspose( const Matrix4& mat )
const
{
	// A^T*B^T
	return Matrix4<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] +
		_entry[ 4 ] * mat._entry[ 1 ] +
		_entry[ 8 ] * mat._entry[ 2 ] +
		_entry[ 12 ] * mat._entry[ 3 ],

		_entry[ 0 ] * mat._entry[ 4 ] +
		_entry[ 4 ] * mat._entry[ 5 ] +
		_entry[ 8 ] * mat._entry[ 6 ] +
		_entry[ 12 ] * mat._entry[ 7 ],

		_entry[ 0 ] * mat._entry[ 8 ] +
		_entry[ 4 ] * mat._entry[ 9 ] +
		_entry[ 8 ] * mat._entry[ 10 ] +
		_entry[ 12 ] * mat._entry[ 11 ],

		_entry[ 0 ] * mat._entry[ 12 ] +
		_entry[ 4 ] * mat._entry[ 13 ] +
		_entry[ 8 ] * mat._entry[ 14 ] +
		_entry[ 12 ] * mat._entry[ 15 ],

		_entry[ 1 ] * mat._entry[ 0 ] +
		_entry[ 5 ] * mat._entry[ 1 ] +
		_entry[ 9 ] * mat._entry[ 2 ] +
		_entry[ 13 ] * mat._entry[ 3 ],

		_entry[ 1 ] * mat._entry[ 4 ] +
		_entry[ 5 ] * mat._entry[ 5 ] +
		_entry[ 9 ] * mat._entry[ 6 ] +
		_entry[ 13 ] * mat._entry[ 7 ],

		_entry[ 1 ] * mat._entry[ 8 ] +
		_entry[ 5 ] * mat._entry[ 9 ] +
		_entry[ 9 ] * mat._entry[ 10 ] +
		_entry[ 13 ] * mat._entry[ 11 ],

		_entry[ 1 ] * mat._entry[ 12 ] +
		_entry[ 5 ] * mat._entry[ 13 ] +
		_entry[ 9 ] * mat._entry[ 14 ] +
		_entry[ 13 ] * mat._entry[ 15 ],

		_entry[ 2 ] * mat._entry[ 0 ] +
		_entry[ 6 ] * mat._entry[ 1 ] +
		_entry[ 10 ] * mat._entry[ 2 ] +
		_entry[ 14 ] * mat._entry[ 3 ],

		_entry[ 2 ] * mat._entry[ 4 ] +
		_entry[ 6 ] * mat._entry[ 5 ] +
		_entry[ 10 ] * mat._entry[ 6 ] +
		_entry[ 14 ] * mat._entry[ 7 ],

		_entry[ 2 ] * mat._entry[ 8 ] +
		_entry[ 6 ] * mat._entry[ 9 ] +
		_entry[ 10 ] * mat._entry[ 10 ] +
		_entry[ 14 ] * mat._entry[ 11 ],

		_entry[ 2 ] * mat._entry[ 12 ] +
		_entry[ 6 ] * mat._entry[ 13 ] +
		_entry[ 10 ] * mat._entry[ 14 ] +
		_entry[ 14 ] * mat._entry[ 15 ],

		_entry[ 3 ] * mat._entry[ 0 ] +
		_entry[ 7 ] * mat._entry[ 1 ] +
		_entry[ 11 ] * mat._entry[ 2 ] +
		_entry[ 15 ] * mat._entry[ 3 ],

		_entry[ 3 ] * mat._entry[ 4 ] +
		_entry[ 7 ] * mat._entry[ 5 ] +
		_entry[ 11 ] * mat._entry[ 6 ] +
		_entry[ 15 ] * mat._entry[ 7 ],

		_entry[ 3 ] * mat._entry[ 8 ] +
		_entry[ 7 ] * mat._entry[ 9 ] +
		_entry[ 11 ] * mat._entry[ 10 ] +
		_entry[ 15 ] * mat._entry[ 11 ],

		_entry[ 3 ] * mat._entry[ 12 ] +
		_entry[ 7 ] * mat._entry[ 13 ] +
		_entry[ 11 ] * mat._entry[ 14 ] +
		_entry[ 15 ] * mat._entry[ 15 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real> Matrix4<Real>::Inverse( const Real epsilon ) const
{
	Real a0 = _entry[ 0 ] * _entry[ 5 ] - _entry[ 1 ] * _entry[ 4 ];
	Real a1 = _entry[ 0 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 4 ];
	Real a2 = _entry[ 0 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 4 ];
	Real a3 = _entry[ 1 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 5 ];
	Real a4 = _entry[ 1 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 5 ];
	Real a5 = _entry[ 2 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 6 ];
	Real b0 = _entry[ 8 ] * _entry[ 13 ] - _entry[ 9 ] * _entry[ 12 ];
	Real b1 = _entry[ 8 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 12 ];
	Real b2 = _entry[ 8 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 12 ];
	Real b3 = _entry[ 9 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 13 ];
	Real b4 = _entry[ 9 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 13 ];
	Real b5 = _entry[ 10 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 14 ];

	Real det = a0*b5 - a1*b4 + a2*b3 + a3*b2 - a4*b1 + a5*b0;
	if( Math<Real>::FAbs( det ) > epsilon )
	{
		Matrix4 inverse;
		inverse._entry[ 0 ] = +_entry[ 5 ] * b5 - _entry[ 6 ] * b4 + _entry[ 7 ] * b3;
		inverse._entry[ 4 ] = -_entry[ 4 ] * b5 + _entry[ 6 ] * b2 - _entry[ 7 ] * b1;
		inverse._entry[ 8 ] = +_entry[ 4 ] * b4 - _entry[ 5 ] * b2 + _entry[ 7 ] * b0;
		inverse._entry[ 12 ] = -_entry[ 4 ] * b3 + _entry[ 5 ] * b1 - _entry[ 6 ] * b0;
		inverse._entry[ 1 ] = -_entry[ 1 ] * b5 + _entry[ 2 ] * b4 - _entry[ 3 ] * b3;
		inverse._entry[ 5 ] = +_entry[ 0 ] * b5 - _entry[ 2 ] * b2 + _entry[ 3 ] * b1;
		inverse._entry[ 9 ] = -_entry[ 0 ] * b4 + _entry[ 1 ] * b2 - _entry[ 3 ] * b0;
		inverse._entry[ 13 ] = +_entry[ 0 ] * b3 - _entry[ 1 ] * b1 + _entry[ 2 ] * b0;
		inverse._entry[ 2 ] = +_entry[ 13 ] * a5 - _entry[ 14 ] * a4 + _entry[ 15 ] * a3;
		inverse._entry[ 6 ] = -_entry[ 12 ] * a5 + _entry[ 14 ] * a2 - _entry[ 15 ] * a1;
		inverse._entry[ 10 ] = +_entry[ 12 ] * a4 - _entry[ 13 ] * a2 + _entry[ 15 ] * a0;
		inverse._entry[ 14 ] = -_entry[ 12 ] * a3 + _entry[ 13 ] * a1 - _entry[ 14 ] * a0;
		inverse._entry[ 3 ] = -_entry[ 9 ] * a5 + _entry[ 10 ] * a4 - _entry[ 11 ] * a3;
		inverse._entry[ 7 ] = +_entry[ 8 ] * a5 - _entry[ 10 ] * a2 + _entry[ 11 ] * a1;
		inverse._entry[ 11 ] = -_entry[ 8 ] * a4 + _entry[ 9 ] * a2 - _entry[ 11 ] * a0;
		inverse._entry[ 15 ] = +_entry[ 8 ] * a3 - _entry[ 9 ] * a1 + _entry[ 10 ] * a0;

		Real invDet = ((Real)1) / det;
		inverse._entry[ 0 ] *= invDet;
		inverse._entry[ 1 ] *= invDet;
		inverse._entry[ 2 ] *= invDet;
		inverse._entry[ 3 ] *= invDet;
		inverse._entry[ 4 ] *= invDet;
		inverse._entry[ 5 ] *= invDet;
		inverse._entry[ 6 ] *= invDet;
		inverse._entry[ 7 ] *= invDet;
		inverse._entry[ 8 ] *= invDet;
		inverse._entry[ 9 ] *= invDet;
		inverse._entry[ 10 ] *= invDet;
		inverse._entry[ 11 ] *= invDet;
		inverse._entry[ 12 ] *= invDet;
		inverse._entry[ 13 ] *= invDet;
		inverse._entry[ 14 ] *= invDet;
		inverse._entry[ 15 ] *= invDet;

		return inverse;
	}

	return ZERO;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix4<Real> Matrix4<Real>::Adjoint() const
{
	Real a0 = _entry[ 0 ] * _entry[ 5 ] - _entry[ 1 ] * _entry[ 4 ];
	Real a1 = _entry[ 0 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 4 ];
	Real a2 = _entry[ 0 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 4 ];
	Real a3 = _entry[ 1 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 5 ];
	Real a4 = _entry[ 1 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 5 ];
	Real a5 = _entry[ 2 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 6 ];
	Real b0 = _entry[ 8 ] * _entry[ 13 ] - _entry[ 9 ] * _entry[ 12 ];
	Real b1 = _entry[ 8 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 12 ];
	Real b2 = _entry[ 8 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 12 ];
	Real b3 = _entry[ 9 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 13 ];
	Real b4 = _entry[ 9 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 13 ];
	Real b5 = _entry[ 10 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 14 ];

	return Matrix4<Real>
		(
		+_entry[ 5 ] * b5 - _entry[ 6 ] * b4 + _entry[ 7 ] * b3,
		-_entry[ 1 ] * b5 + _entry[ 2 ] * b4 - _entry[ 3 ] * b3,
		+_entry[ 13 ] * a5 - _entry[ 14 ] * a4 + _entry[ 15 ] * a3,
		-_entry[ 9 ] * a5 + _entry[ 10 ] * a4 - _entry[ 11 ] * a3,
		-_entry[ 4 ] * b5 + _entry[ 6 ] * b2 - _entry[ 7 ] * b1,
		+_entry[ 0 ] * b5 - _entry[ 2 ] * b2 + _entry[ 3 ] * b1,
		-_entry[ 12 ] * a5 + _entry[ 14 ] * a2 - _entry[ 15 ] * a1,
		+_entry[ 8 ] * a5 - _entry[ 10 ] * a2 + _entry[ 11 ] * a1,
		+_entry[ 4 ] * b4 - _entry[ 5 ] * b2 + _entry[ 7 ] * b0,
		-_entry[ 0 ] * b4 + _entry[ 1 ] * b2 - _entry[ 3 ] * b0,
		+_entry[ 12 ] * a4 - _entry[ 13 ] * a2 + _entry[ 15 ] * a0,
		-_entry[ 8 ] * a4 + _entry[ 9 ] * a2 - _entry[ 11 ] * a0,
		-_entry[ 4 ] * b3 + _entry[ 5 ] * b1 - _entry[ 6 ] * b0,
		+_entry[ 0 ] * b3 - _entry[ 1 ] * b1 + _entry[ 2 ] * b0,
		-_entry[ 12 ] * a3 + _entry[ 13 ] * a1 - _entry[ 14 ] * a0,
		+_entry[ 8 ] * a3 - _entry[ 9 ] * a1 + _entry[ 10 ] * a0
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Real Matrix4<Real>::Determinant() const
{
	Real a0 = _entry[ 0 ] * _entry[ 5 ] - _entry[ 1 ] * _entry[ 4 ];
	Real a1 = _entry[ 0 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 4 ];
	Real a2 = _entry[ 0 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 4 ];
	Real a3 = _entry[ 1 ] * _entry[ 6 ] - _entry[ 2 ] * _entry[ 5 ];
	Real a4 = _entry[ 1 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 5 ];
	Real a5 = _entry[ 2 ] * _entry[ 7 ] - _entry[ 3 ] * _entry[ 6 ];
	Real b0 = _entry[ 8 ] * _entry[ 13 ] - _entry[ 9 ] * _entry[ 12 ];
	Real b1 = _entry[ 8 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 12 ];
	Real b2 = _entry[ 8 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 12 ];
	Real b3 = _entry[ 9 ] * _entry[ 14 ] - _entry[ 10 ] * _entry[ 13 ];
	Real b4 = _entry[ 9 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 13 ];
	Real b5 = _entry[ 10 ] * _entry[ 15 ] - _entry[ 11 ] * _entry[ 14 ];
	Real det = a0*b5 - a1*b4 + a2*b3 + a3*b2 - a4*b1 + a5*b0;
	return det;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix4<Real>::MakeObliqueProjection( const DVec3<Real>& normal,
	const DVec3<Real>& origin, const DVec3<Real>& direction )
{
	// The projection plane is Dot(N,X-P) = 0 where N is a 3-by-1 unit-length
	// normal vector and P is a 3-by-1 point on the plane.  The projection
	// is oblique to the plane, in the direction of the 3-by-1 vector D.
	// Necessarily Dot(N,D) is not zero for this projection to make sense.
	// Given a 3-by-1 point U, compute the intersection of the line U+t*D
	// with the plane to obtain t = -Dot(N,U-P)/Dot(N,D).  Then
	//
	//   projection(U) = P + [I - D*N^T/Dot(N,D)]*(U-P)
	//
	// A 4-by-4 homogeneous transformation representing the projection is
	//
	//       +-                               -+
	//   M = | D*N^T - Dot(N,D)*I   -Dot(N,P)D |
	//       |          0^T          -Dot(N,D) |
	//       +-                               -+
	//
	// where M applies to [U^T 1]^T by M*[U^T 1]^T.  The matrix is chosen so
	// that M[3][3] > 0 whenever Dot(N,D) < 0 (projection is onto the
	// "positive side" of the plane).

	Real dotND = normal.Dot( direction );
	Real dotNO = normal.Dot( origin );
	_entry[ 0 ] = direction[ 0 ] * normal[ 0 ] - dotND;
	_entry[ 1 ] = direction[ 0 ] * normal[ 1 ];
	_entry[ 2 ] = direction[ 0 ] * normal[ 2 ];
	_entry[ 3 ] = -dotNO*direction[ 0 ];
	_entry[ 4 ] = direction[ 1 ] * normal[ 0 ];
	_entry[ 5 ] = direction[ 1 ] * normal[ 1 ] - dotND;
	_entry[ 6 ] = direction[ 1 ] * normal[ 2 ];
	_entry[ 7 ] = -dotNO*direction[ 1 ];
	_entry[ 8 ] = direction[ 2 ] * normal[ 0 ];
	_entry[ 9 ] = direction[ 2 ] * normal[ 1 ];
	_entry[ 10 ] = direction[ 2 ] * normal[ 2 ] - dotND;
	_entry[ 11 ] = -dotNO*direction[ 2 ];
	_entry[ 12 ] = 0.0f;
	_entry[ 13 ] = 0.0f;
	_entry[ 14 ] = 0.0f;
	_entry[ 15 ] = -dotND;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix4<Real>::MakePerspectiveProjection(
	const DVec3<Real>& normal, const DVec3<Real>& origin,
	const DVec3<Real>& eye )
{
	//     +-                                                 -+
	// M = | Dot(N,E-P)*I - E*N^T    -(Dot(N,E-P)*I - E*N^T)*E |
	//     |        -N^t                      Dot(N,E)         |
	//     +-                                                 -+
	//
	// where E is the eye point, P is a point on the plane, and N is a
	// unit-length plane normal.

	Real dotND = normal.Dot( eye - origin );
	_entry[ 0 ] = dotND - eye[ 0 ] * normal[ 0 ];
	_entry[ 1 ] = -eye[ 0 ] * normal[ 1 ];
	_entry[ 2 ] = -eye[ 0 ] * normal[ 2 ];
	_entry[ 3 ] = -(_entry[ 0 ] * eye[ 0 ] + _entry[ 1 ] * eye[ 1 ] + _entry[ 2 ] * eye[ 2 ]);
	_entry[ 4 ] = -eye[ 1 ] * normal[ 0 ];
	_entry[ 5 ] = dotND - eye[ 1 ] * normal[ 1 ];
	_entry[ 6 ] = -eye[ 1 ] * normal[ 2 ];
	_entry[ 7 ] = -(_entry[ 4 ] * eye[ 0 ] + _entry[ 5 ] * eye[ 1 ] + _entry[ 6 ] * eye[ 2 ]);
	_entry[ 8 ] = -eye[ 2 ] * normal[ 0 ];
	_entry[ 9 ] = -eye[ 2 ] * normal[ 1 ];
	_entry[ 10 ] = dotND - eye[ 2 ] * normal[ 2 ];
	_entry[ 11 ] = -(_entry[ 8 ] * eye[ 0 ] + _entry[ 9 ] * eye[ 1 ] + _entry[ 10 ] * eye[ 2 ]);
	_entry[ 12 ] = -normal[ 0 ];
	_entry[ 13 ] = -normal[ 1 ];
	_entry[ 14 ] = -normal[ 2 ];
	_entry[ 15 ] = normal.Dot( eye );
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix4<Real>::MakeReflection( const DVec3<Real>& normal,
	const DVec3<Real>& origin )
{
	//     +-                         -+
	// M = | I-2*N*N^T    2*Dot(N,P)*N |
	//     |     0^T            1      |
	//     +-                         -+
	//
	// where P is a point on the plane and N is a unit-length plane normal.

	Real twoDotNO = ((Real)2)*(normal.Dot( origin ));
	_entry[ 0 ] = (Real)1 - ((Real)2)*normal[ 0 ] * normal[ 0 ];
	_entry[ 1 ] = -((Real)2)*normal[ 0 ] * normal[ 1 ];
	_entry[ 2 ] = -((Real)2)*normal[ 0 ] * normal[ 2 ];
	_entry[ 3 ] = twoDotNO*normal[ 0 ];
	_entry[ 4 ] = -((Real)2)*normal[ 1 ] * normal[ 0 ];
	_entry[ 5 ] = (Real)1 - ((Real)2)*normal[ 1 ] * normal[ 1 ];
	_entry[ 6 ] = -((Real)2)*normal[ 1 ] * normal[ 2 ];
	_entry[ 7 ] = twoDotNO*normal[ 1 ];
	_entry[ 8 ] = -((Real)2)*normal[ 2 ] * normal[ 0 ];
	_entry[ 9 ] = -((Real)2)*normal[ 2 ] * normal[ 1 ];
	_entry[ 10 ] = (Real)1 - ((Real)2)*normal[ 2 ] * normal[ 2 ];
	_entry[ 11 ] = twoDotNO*normal[ 2 ];
	_entry[ 12 ] = (Real)0;
	_entry[ 13 ] = (Real)0;
	_entry[ 14 ] = (Real)0;
	_entry[ 15 ] = (Real)1;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Matrix4<Real> operator* (Real scalar, const Matrix4<Real>& mat)
{
	return mat*scalar;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec4<Real> operator* (const DVec4<Real>& vec,
	const Matrix4<Real>& mat)
{
	return DVec4<Real>(
		vec[ 0 ] * mat[ 0 ][ 0 ] + vec[ 1 ] * mat[ 1 ][ 0 ] + vec[ 2 ] * mat[ 2 ][ 0 ] + vec[ 3 ] * mat[ 3 ][ 0 ],
		vec[ 0 ] * mat[ 0 ][ 1 ] + vec[ 1 ] * mat[ 1 ][ 1 ] + vec[ 2 ] * mat[ 2 ][ 1 ] + vec[ 3 ] * mat[ 3 ][ 1 ],
		vec[ 0 ] * mat[ 0 ][ 2 ] + vec[ 1 ] * mat[ 1 ][ 2 ] + vec[ 2 ] * mat[ 2 ][ 2 ] + vec[ 3 ] * mat[ 3 ][ 2 ],
		vec[ 0 ] * mat[ 0 ][ 3 ] + vec[ 1 ] * mat[ 1 ][ 3 ] + vec[ 2 ] * mat[ 2 ][ 3 ] + vec[ 3 ] * mat[ 3 ][ 3 ] );
}
//----------------------------------------------------------------------------
