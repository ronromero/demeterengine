// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)
// Modified for Demeter convenience
#pragma once

#include "precompiled.h"
#include "Tuple.h"
#include <assert.h>

namespace Demeter
{
	template <typename Real>
	class DMath
	{
	public:
		// Wrappers to hide implementations of functions.  The ACos and ASin
		// functions clamp the input argument to [-1,1] to avoid NaN issues
		// when the input is slightly larger than 1 or slightly smaller than -1.
		// Other functions have the potential for using a fast and approximate
		// algorithm rather than calling the standard math library functions.
		static Real ACos( Real value );
		static Real ASin( Real value );
		static Real ATan( Real value );
		static Real ATan2( Real y, Real x );
		static Real Ceil( Real value );
		static Real Cos( Real value );
		static Real Exp( Real value );
		static Real FAbs( Real value );
		static Real Floor( Real value );
		static Real FMod( Real x, Real y );
		static Real InvSqrt( Real value );
		static Real Log( Real value );
		static Real Log2( Real value );
		static Real Log10( Real value );
		static Real Pow( Real base, Real exponent );
		static Real Sin( Real value );
		static Real Sqr( Real value );
		static Real Sqrt( Real value );
		static Real Tan( Real value );

		// Common constants.
		static const Real EPSILON;
		static const Real ZERO_TOLERANCE;
		static const Real MAX_REAL;
		static const Real PI;
		static const Real TWO_PI;
		static const Real HALF_PI;
		static const Real INV_PI;
		static const Real INV_TWO_PI;
		static const Real DEG_TO_RAD;
		static const Real RAD_TO_DEG;
		static const Real LN_2;
		static const Real LN_10;
		static const Real INV_LN_2;
		static const Real INV_LN_10;
		static const Real SQRT_2;
		static const Real INV_SQRT_2;
		static const Real SQRT_3;
		static const Real INV_SQRT_3;
	};

#include "DMath.inl"

	typedef DMath<float> DMathf;
	typedef DMath<double> DMathd;
	typedef DMath<DReal> Math;
}