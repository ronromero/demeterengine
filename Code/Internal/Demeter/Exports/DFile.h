#pragma once

#include <fstream>
#include <stdarg.h> // va_list, va_start
#include <stdio.h> // vsprintf

namespace Demeter
{
	class DFile
	{
	public:
		DFile( void );
		~DFile( void );
		void Open( const char* i_filename, const std::ios_base::openmode i_mode );
		void Close( void );
		void Write( const char* str );

	private:
		std::fstream *fstream;
	};
#include "DFile.inl"
}