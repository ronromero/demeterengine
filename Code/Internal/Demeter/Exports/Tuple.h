#pragma once

#include <cstring>

namespace Demeter
{
	template<int DIMENSION, typename TYPE>
	class Tuple
	{
	public:
		Tuple( void );
		Tuple( const Tuple& tuple );
		~Tuple( void );

		inline operator const TYPE*()const;
		inline operator TYPE*();
		inline TYPE operator[]( int i ) const;
		inline TYPE& operator[]( int i );

		Tuple& operator= (const Tuple& tuple);

		bool operator== (const Tuple& tuple) const;
		bool operator!= (const Tuple& tuple) const;
		bool operator< (const Tuple& tuple) const;
		bool operator<= (const Tuple& tuple) const;
		bool operator> (const Tuple& tuple) const;
		bool operator>= (const Tuple& tuple) const;

	protected:
		TYPE _tuple[ DIMENSION ];
	};
#include "Tuple.inl"
}