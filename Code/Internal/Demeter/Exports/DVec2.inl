// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>::DVec2()
{
	// Uninitialized for performance in array construction.
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>::DVec2( const DVec2& vec )
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>::DVec2( const Tuple<2, Real>& tuple )
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>::DVec2( Real x, Real y )
{
	_tuple[ 0 ] = x;
	_tuple[ 1 ] = y;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>& DVec2<Real>::operator= (const DVec2& vec)
{
	_tuple[ 0 ] = vec._tuple[ 0 ];
	_tuple[ 1 ] = vec._tuple[ 1 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real>& DVec2<Real>::operator= (const Tuple<2, Real>& tuple)
{
	_tuple[ 0 ] = tuple[ 0 ];
	_tuple[ 1 ] = tuple[ 1 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::X() const
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec2<Real>::X()
{
	return _tuple[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::Y() const
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real& DVec2<Real>::Y()
{
	return _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> DVec2<Real>::operator+ (const DVec2& vec) const
{
	return DVec2
		(
		_tuple[ 0 ] + vec._tuple[ 0 ],
		_tuple[ 1 ] + vec._tuple[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> DVec2<Real>::operator- (const DVec2& vec) const
{
	return DVec2
		(
		_tuple[ 0 ] - vec._tuple[ 0 ],
		_tuple[ 1 ] - vec._tuple[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> DVec2<Real>::operator* (Real scalar) const
{
	return DVec2
		(
		scalar*_tuple[ 0 ],
		scalar*_tuple[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> DVec2<Real>::operator/ (Real scalar) const
{
	DVec2 result;

	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		result._tuple[ 0 ] = invScalar*_tuple[ 0 ];
		result._tuple[ 1 ] = invScalar*_tuple[ 1 ];
	}
	else
	{
		result._tuple[ 0 ] = Math<Real>::MAX_REAL;
		result._tuple[ 1 ] = Math<Real>::MAX_REAL;
	}

	return result;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> DVec2<Real>::operator- () const
{
	return DVec2
		(
		-_tuple[ 0 ],
		-_tuple[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real>& DVec2<Real>::operator+= (const DVec2& vec)
{
	_tuple[ 0 ] += vec._tuple[ 0 ];
	_tuple[ 1 ] += vec._tuple[ 1 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real>& DVec2<Real>::operator-= (const DVec2& vec)
{
	_tuple[ 0 ] -= vec._tuple[ 0 ];
	_tuple[ 1 ] -= vec._tuple[ 1 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real>& DVec2<Real>::operator*= (Real scalar)
{
	_tuple[ 0 ] *= scalar;
	_tuple[ 1 ] *= scalar;
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real>& DVec2<Real>::operator/= (Real scalar)
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		_tuple[ 0 ] *= invScalar;
		_tuple[ 1 ] *= invScalar;
	}
	else
	{
		_tuple[ 0 ] *= Math<Real>::MAX_REAL;
		_tuple[ 1 ] *= Math<Real>::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::Length() const
{
	return Math<Real>::Sqrt
		(
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::SquaredLength() const
{
	return
		_tuple[ 0 ] * _tuple[ 0 ] +
		_tuple[ 1 ] * _tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::Dot( const DVec2& vec ) const
{
	return
		_tuple[ 0 ] * vec._tuple[ 0 ] +
		_tuple[ 1 ] * vec._tuple[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
inline Real DVec2<Real>::Normalize( const Real epsilon )
{
	Real length = Length();

	if( length > epsilon )
	{
		Real invLength = ((Real)1) / length;
		_tuple[ 0 ] *= invLength;
		_tuple[ 1 ] *= invLength;
	}
	else
	{
		length = (Real)0;
		_tuple[ 0 ] = (Real)0;
		_tuple[ 1 ] = (Real)0;
	}

	return length;
}