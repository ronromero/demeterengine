inline HashedString::HashedString( void ) : _hash( Hash( "" ) ) {}

inline HashedString::HashedString( const char * iString ) :
_hash( Hash( iString ) )
#ifdef DEBUG_KEEP_STRING
, _string( iString )
#endif
{
}

inline HashedString::HashedString( const HashedString &iOther ) :
_hash( iOther._hash )
#ifdef DEBUG_KEEP_STRING
, _string( iOther._string )
#endif
{
}

inline HashedString& HashedString::operator=(const HashedString & iOther)
{
	_hash = iOther._hash;
#ifdef DEBUG_KEEP_STRING
	_string = iOther._string;
#endif
	return *this;
}

inline const bool HashedString::operator <(const HashedString & iOther) const
{
	return _hash < iOther._hash;
}

inline const bool HashedString::operator >( const HashedString & iOther ) const
{
	return _hash > iOther._hash;
}

inline const unsigned int HashedString::Get( void ) const { return _hash; }
inline bool HashedString::operator==(const HashedString & iOther) const
{
	return _hash == iOther._hash;
}