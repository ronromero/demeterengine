#ifndef _DEMETER_DGAMEOBJECT_H_
#define _DEMETER_DGAMEOBJECT_H_

#include "DObject.h"
#include <memory>
#include <d3d9.h>
#include <d3dx9.h>
#include "DGameComponent.h"
#include "DVector.h"
#include "DMatrix.h"

namespace Demeter
{
	class DController;
	class DRenderable;
	class DGameComponent;
	class DCollider;

	class DGameObject : public DObject
	{
	public:
		DRenderable *_renderable;
		DController *_controller;
		DTransform *_transform;
		DCollider *_collider;
		DRigidBody *_rigidBody;

		static std::shared_ptr<DGameObject> Instantiate( void );
		static std::shared_ptr<DGameObject> Instantiate( const DVec3f iPos,
			const DVec3f iScale, const DVec3f iEulerRotInDeg );

		~DGameObject( void );
		void AddComponent( DGameComponent *iComponent );

		//protected:
		DGameObject( void );
		DGameObject( const DVec3f iPos, const DVec3f iScale, const DVec3f iEulerRotInDeg );

	private:
		DGameObject( const DGameObject& );
		DGameObject& operator=(const DGameObject&);
	};

	typedef std::shared_ptr<DGameObject> DGameObjectPtr;
}
#endif //_DEMETER_DGAMEOBJECT_H_