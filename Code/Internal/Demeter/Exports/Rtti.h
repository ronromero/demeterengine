#ifndef _DEMETER_RTTI_H_
#define _DEMETER_RTTI_H_

namespace Demeter
{
	class Rtti
	{
	public:
		Rtti( const char* name, const Rtti* baseType );
		~Rtti( void );

		const char* GetName( void ) const;
		bool IsExactly( const Rtti& iType ) const;
		bool IsDerived( const Rtti& iType ) const;

	private:
		const char* _name;
		const Rtti* _baseType;
	};
}

#ifndef D1_DECLARE_RTTI
#define D1_DECLARE_RTTI  \
	public: \
	static const Rtti TYPE; \
	virtual const Rtti& GetRttiType() const \
	{\
	return TYPE; \
}
#endif

#ifndef D1_IMPLEMENT_RTTI
#define D1_IMPLEMENT_RTTI(nsname, baseclassname, classname) \
	const Rtti classname::TYPE(#nsname"."#classname, &baseclassname::TYPE)
#endif

#endif // _DEMETER_RTTI_H_