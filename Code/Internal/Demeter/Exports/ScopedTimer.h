#pragma once

#include "precompiled.h"
#include "Timer.h"
#include "DProfiler.h"
#include "FileLogger.h"

namespace Demeter
{
	extern Profiler gProfiler;
	extern FileLogger gProfilerLog;

	class ScopedTimer
	{
	public:
		ScopedTimer( const char *ipName );
		ScopedTimer( const char *ipName, Profiler *ipProfiler );
		~ScopedTimer( void );

	private:
		const char *mpScopeName;
		Tick mStart;
		Profiler *mpProfiler;
	};

	inline ScopedTimer::ScopedTimer( const char *ipName )
		: mpScopeName( ipName )
		, mpProfiler( NULL )
		, mStart( Timer::GetCurrentTick() )
	{
		//assertion( ipName != NULL, "Timer name cannot be empty/null" );
	}

	inline ScopedTimer::ScopedTimer( const char *ipName, Profiler *ipProfiler )
		: mpScopeName( ipName )
		, mpProfiler( ipProfiler )
		, mStart( Timer::GetCurrentTick() )
	{
		//assertion( ipName != NULL, "Timer name cannot be empty/null" );
	}

	inline ScopedTimer::~ScopedTimer()
	{
		Tick end = Timer::GetCurrentTick();
		DReal diff = Timer::GetDifferenceMS( mStart, end );
		if( mpProfiler )
			mpProfiler->AddTiming( mpScopeName, diff );
		else
			gProfiler.AddTiming( mpScopeName, diff );
	}

#if defined( _PROFILE )
#define PROFILE_CONCAT_HELPER(left, right) left##right
#define PROFILE_CONCAT(left, right) PROFILE_CONCAT_HELPER(left, right)

#define PROFILE_SCOPE_BEGIN(str)	{ ScopedTimer PROFILE_CONCAT(__Timer, __LINE__)( str );
#define PROFILE_SCOPE_END()			}

#define PROFILE_UNSCOPED(str)		ScopedTimer PROFILE_CONCAT(__Timer, __LINE__)( str );
#define PROFILE_PRINT_RESULTS()		Demeter::gProfiler.PrintTimings( &Demeter::gProfilerLog );
#else
#define PROFILE_SCOPE_BEGIN(str)	__noop
#define PROFILE_SCOPE_END			__noop
#define PROFILE_UNSCOPED(str)		__noop
#define PROFILE_PRINT_RESULTS()		__noop
#endif // ENABLE_PROFILING
}