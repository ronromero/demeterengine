#pragma once

namespace Demeter
{
	class HashedString;
	class DObject;

	class IObserver
	{
	public:
		virtual void ProcessMessage( const HashedString &iMessage, DObject &iObject,
			void* iObjectData ) = 0;
	};
}