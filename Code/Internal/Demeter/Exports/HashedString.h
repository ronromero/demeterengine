#ifndef _DEMETER_HASHEDSTRING_H_
#define _DEMETER_HASHEDSTRING_H_

#include "precompiled.h"

#if defined( _DEBUG )
#define DEBUG_KEEP_STRING
#endif

namespace Demeter
{
	class HashedString
	{
	public:
		HashedString( void );
		HashedString( const char * iString );
		HashedString( const HashedString & iOther );
		HashedString& operator=(const HashedString & iOther);

		const unsigned int Get( void ) const;
		bool operator ==(const HashedString & iOther) const;
		const bool operator <(const HashedString & iOther) const;
		const bool operator >(const HashedString & iOther) const;

		static unsigned int Hash( const char * iString )
		{
			//assertion( iString, "Cannot hash a null/empty string" );
			return Hash( reinterpret_cast<void *>(const_cast<char *>(iString)), strlen( iString ) );
		}

		static unsigned int Hash( const void * iBytes, unsigned int iByteCount )
		{
			// FNV hash, http://isthe.com/chongo/tech/comp/fnv/

			register const unsigned char * p = static_cast<const unsigned char *>(iBytes);
			unsigned int hash = 2166136261;

			for( unsigned int i = 0; i < iByteCount; ++i )
				hash = 16777619 * (hash ^ p[ i ]);

			return hash ^ (hash >> 16);
		}

	private:
		unsigned int _hash;
#ifdef DEBUG_KEEP_STRING
		std::string  _string;
#endif
	};
#include "HashedString.inl"
}
#endif //_DEMETER_HASHEDSTRING_H_