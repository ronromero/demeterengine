#ifndef _DEMETER_DCONTENTMANAGER_H_
#define _DEMETER_DCONTENTMANAGER_H_

#include <map>
#include <vector>

struct IDirect3DDevice9;

namespace Parrhasius
{
	class Sprite;
	class PEngine;
	class Mesh;
	class Texture;
	class Material;
}

namespace Demeter
{
	class DGameObject;
	class Light;
	class Camera;
	class DEngineSettings;

	class DContentManager
	{
	public:
		DContentManager( DEngineSettings *iSettings );
		~DContentManager( void );

		std::shared_ptr<Parrhasius::Mesh> LoadMesh( IDirect3DDevice9 *iGraphicsDevice, const char* iPath );
		std::shared_ptr<Parrhasius::Texture> LoadTexture( IDirect3DDevice9 *iGraphicsDevice, const char* iPath );
		std::shared_ptr<Parrhasius::Material> LoadMaterial( IDirect3DDevice9 *iGraphicsDevice, const char* iPath );


	public: //TODO: make private
		std::vector<std::shared_ptr<Parrhasius::Sprite>> _sprites;
		std::vector<std::shared_ptr<Parrhasius::Sprite>>::iterator _spriteItr;
		DEngineSettings *_settings;

	private:
		std::map<const char*, std::shared_ptr<Parrhasius::Mesh>> _meshes;
		std::map<const char*, std::shared_ptr<Parrhasius::Texture>> _textures;
		std::map<const char*, std::shared_ptr<Parrhasius::Material>> _materials;

		DContentManager( const DContentManager& );
		DContentManager& operator=(const DContentManager);
	};
}
#endif //_DEMETER_DCONTENTMANAGER_H_