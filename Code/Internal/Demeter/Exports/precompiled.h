#ifndef _PARRHASIUS_PRECOMPILED_H_
#define _PARRHASIUS_PRECOMPILED_H_

// OS_DEFINES (D1_WIN32, D1_LINUX)
// GRAPHICS_DEFINES (D1_DX9, D1_DX11, D1_OPENGL)
// FILE_DEFINES (D1_LITTLE_ENDIAN, D1_BIG_ENDIAN)

//#define D1_USE_FLOAT
#define DUSE_DEBUGLINES


#if defined(_WIN32) || defined(WIN32)
#define D1_WIN32
#define D1_LITTLE_ENDIAN
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#undef NOMINMAX
#undef WIN32_LEAN_AND_MEAN

#include <cstdint>
#include <climits> // small portion of stdint.h for Unix systems
//typedef __int8              int8_t;
//typedef __int16             int16_t;
//typedef __int32             int32_t;
//typedef __int64             int64_t;
//typedef unsigned __int8     uint8_t;
//typedef unsigned __int16    uint16_t;
//typedef unsigned __int32    uint32_t;
//typedef unsigned __int64    uint64_t;

//#define INT8_MIN            _I8_MIN
//#define INT8_MAX            _I8_MAX
//#define INT16_MIN           _I16_MIN
//#define INT16_MAX           _I16_MAX
//#define INT32_MIN           _I32_MIN
//#define INT32_MAX           _I32_MAX
//#define INT64_MIN           _I64_MIN
//#define INT64_MAX           _I64_MAX
//#define UINT8_MAX           _UI8_MAX
//#define UINT16_MAX          _UI16_MAX
//#define UINT32_MAX          _UI32_MAX
//#define UINT64_MAX          _UI64_MAX

//char -	A char is usually 8 bits and is generally large enough to hold an 
//			ASCII or UTF-8 character (see Section 5.4.4.1). Some compilers define 
//			char to be signed, while others use unsigned chars by default
typedef uint8_t U8;
typedef int8_t I8;
//short -	A short is intended to be smaller than an int as is 16 bits on many machines.
typedef uint16_t U16;
typedef int16_t I16;
//int -		An int is supposed to hold a signed integer value that is the most 
//			efficient size for the target platform; it is generally defined to be 
//			32 bits wide on Pentium class PCs.
typedef uint32_t U32;
typedef int32_t I32;
typedef uint_fast32_t U32F;
typedef int_fast32_t I32F;
//long -	A long is as large or larger than an int and may be 32 or 64 bits, depending 
//			on the hardware.
typedef uint64_t U64;
typedef int64_t I64;
//float -	On most modern compilers, a float is 32-bit IEEE-754 floating point value
//double -	A double is a double-precision (i.e., 64-bit) IEEE-754 floating point value
typedef float F32;
typedef double F64;
//bool -	A bool is a true/false value. the size of a bool varies widely across different 
//			compilers and hardware architectures. It is never implemented as a single bit, 
//			but some compilers define it to be 8 bits, while others use a full 32 bits.

typedef U32* PTR32;
typedef U64* PTR64;

typedef LARGE_INTEGER Tick;
#endif // _WIN32 || WIN32


/// Linux
#if defined(__LINUX__)
#include <inttypes.h>
#define D1_LITTLE_ENDIAN
#endif // __LINUX

// Common headers
#include <cassert>
#include <cctype>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>
#include <memory>

// Common STL headers.
#include <algorithm>
#include <deque>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>


// User-defined keywords for syntax highlighting of special class sections.
#define public_internal public
#define protected_internal protected
#define private_internal private

#ifndef SDELETE
#define SDELETE(p)	{ if(p){ delete (p); (p)=NULL; } }
#endif

#ifndef SDELETE_ARRAY
#define SDELETE_ARRAY(p) { if(p) { delete[] (p); (p)=NULL; } }
#endif

#ifndef SRELEASE
#define SRELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }
#endif

#ifdef D1_USE_FLOAT
typedef float DReal;
#define DRealMax FLT_MAX;
#define DRealMin FLT_MIN;
#else
typedef double DReal;
#define DRealMax DBL_MAX;
#define DRealMin DBL_MIN;
#endif

#if defined(_DEBUG) && defined(DUSE_DEBUGLINES)
#include <Parrhasius/Exports/Debug.h>
#define PDEBUG_INITIALIZE(x) Parrhasius::Debug::Initialize((x))
#define PDEBUG_RELEASE()  Parrhasius::Debug::Release()
#define PDEBUG_ADDLINE( start, end, color )  Parrhasius::Debug::AddLine( (start), (end), (color))
#define PDEBUG_DRAWLINES( device, view, proj )  Parrhasius::Debug::DrawLines( (device), (view), (proj))
#else
#define PDEBUG_INITIALIZE(x) void(0)
#define PDEBUG_RELEASE() void(0)
#define PDEBUG_ADDLINE( start, end, color ) void(0)
#define PDEBUG_DRAWLINES( device, view, proj ) void(0)

#endif

#endif	// _PARRHASIUS_PRECOMPILED_H_
