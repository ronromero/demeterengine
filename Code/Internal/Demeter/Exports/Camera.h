#ifndef _DEMETER_CAMERA_H_
#define _DEMETER_CAMERA_H_

#include "DGameObject.h"
#include <d3d9.h>
#include <d3dx9.h>

namespace Demeter
{
	class Camera : public DGameObject
	{
	public:
		Camera( const DVec3f iPos,
			const DVec3f iRot,
			const float iFovInRad,
			const float iAspectRatio,
			const float zNear,
			const float zFar );

		const D3DXMATRIX View( void ) const;

		const D3DXMATRIX Projection( void ) const;
		void Projection( const float iFovInRad,
			const float iAspectRatio,
			const float zNear,
			const float zFar );

	private:
		D3DXMATRIX _view;
		D3DXMATRIX _proj;
		float _fovy;
		float _aspect;
		float _near;
		float _far;
	};
}

#endif //_DEMETER_CAMERA_H_