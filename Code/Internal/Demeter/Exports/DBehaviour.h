#pragma once

#include "DGameComponent.h"
#include "IObserver.h"

namespace Demeter
{
	class DBehaviour : public DGameComponent, IObserver
	{
	public:
		DBehaviour( void );
		~DBehaviour( void );
		virtual void ProcessMessage( const HashedString &iMessage, DObject &iObject,
			void* iObjectData ) = 0;

	private:
	};

	DBehaviour::DBehaviour( void ) : DGameComponent()
	{
	}

	class MonoBehaviour : DBehaviour
	{

	};
}