// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::ACos( Real value )
{
	if( -(Real)1 < value )
	{
		if( value < (Real)1 )
		{
			return acos( value );
		}
		else
		{
			return (Real)0;
		}
	}
	else
	{
		return PI;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::ASin( Real value )
{
	if( -(Real)1 < value )
	{
		if( value < (Real)1 )
		{
			return asin( value );
		}
		else
		{
			return HALF_PI;
		}
	}
	else
	{
		return -HALF_PI;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::ATan( Real value )
{
	return atan( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::ATan2( Real y, Real x )
{
	if( x != (Real)0 || y != (Real)0 )
	{
		return atan2( y, x );
	}
	else
	{
		// Mathematically, ATan2(0,0) is undefined, but ANSI standards
		// require the function to return 0.
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Ceil( Real value )
{
	return ceil( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Cos( Real value )
{
	return cos( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Exp( Real value )
{
	return exp( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::FAbs( Real value )
{
	return fabs( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Floor( Real value )
{
	return floor( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::FMod( Real x, Real y )
{
	if( y != (Real)0 )
	{
		return fmod( x, y );
	}
	else
	{
		//assertion( false, "Zero input to FMod\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::InvSqrt( Real value )
{
	if( value != (Real)0 )
	{
		return ((Real)1) / sqrt( value );
	}
	else
	{
		//assertion( false, "Division by zero in InvSqr\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Log( Real value )
{
	if( value > (Real)0 )
	{
		return log( value );
	}
	else
	{
		//assertion( false, "Nonpositive input to Log\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Log2( Real value )
{
	if( value > (Real)0 )
	{
		return DMath<Real>::INV_LN_2 * log( value );
	}
	else
	{
		//assertion( false, "Nonpositive input to Log2\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Log10( Real value )
{
	if( value > (Real)0 )
	{
		return DMath<Real>::INV_LN_10 * log( value );
	}
	else
	{
		//assertion( false, "Nonpositive input to Log10\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Pow( Real base, Real exponent )
{
	if( base >= (Real)0 )
	{
		return pow( base, exponent );
	}
	else
	{
		//assertion( false, "Negative base not allowed in Pow\n" );
		return DMath<Real>::MAX_REAL;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Sin( Real value )
{
	return sin( value );
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Sqr( Real value )
{
	return value*value;
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Sqrt( Real value )
{
	if( value >= (Real)0 )
	{
		return sqrt( value );
	}
	else
	{
		//assertion( false, "Negative input to Sqrt\n" );
		return (Real)0;
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Real DMath<Real>::Tan( Real value )
{
	return tan( value );
}