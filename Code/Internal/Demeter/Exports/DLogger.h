#pragma once

#include "precompiled.h"
#include <sstream>

namespace Demeter
{
	class DLogger
	{
	public:
		DLogger( void );
		virtual ~DLogger( void );

		void AddLog( const char* iLogName );
		void AppendLog( const char* iLogName, const char* fmt, ... );
		const bool LogExists( const char* iLogName ) const;
		virtual void PublishLog( const char* iLogName ) = 0;
		virtual void PublishAllLogs( void ) = 0;

	protected:
		std::map<const char*, std::stringstream> _logs;

	private:
		DLogger( const DLogger& );
		DLogger& operartor( const DLogger& );
	};
}