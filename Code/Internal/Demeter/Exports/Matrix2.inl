// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( bool makeZero )
{
	if( makeZero )
	{
		MakeZero();
	}
	else
	{
		MakeIdentity();
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( const Matrix2& mat )
{
	_entry[ 0 ] = mat._entry[ 0 ];
	_entry[ 1 ] = mat._entry[ 1 ];
	_entry[ 2 ] = mat._entry[ 2 ];
	_entry[ 3 ] = mat._entry[ 3 ];
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( Real m00, Real m01, Real m10, Real m11 )
{
	_entry[ 0 ] = m00;
	_entry[ 1 ] = m01;
	_entry[ 2 ] = m10;
	_entry[ 3 ] = m11;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( const Real entry[ 4 ], bool rowMajor )
{
	if( rowMajor )
	{
		_entry[ 0 ] = entry[ 0 ];
		_entry[ 1 ] = entry[ 1 ];
		_entry[ 2 ] = entry[ 2 ];
		_entry[ 3 ] = entry[ 3 ];
	}
	else
	{
		_entry[ 0 ] = entry[ 0 ];
		_entry[ 1 ] = entry[ 2 ];
		_entry[ 2 ] = entry[ 1 ];
		_entry[ 3 ] = entry[ 3 ];
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( const DVec2<Real>& u, const DVec2<Real>& v,
	bool columns )
{
	if( columns )
	{
		_entry[ 0 ] = u[ 0 ];
		_entry[ 1 ] = v[ 0 ];
		_entry[ 2 ] = u[ 1 ];
		_entry[ 3 ] = v[ 1 ];
	}
	else
	{
		_entry[ 0 ] = u[ 0 ];
		_entry[ 1 ] = u[ 1 ];
		_entry[ 2 ] = v[ 0 ];
		_entry[ 3 ] = v[ 1 ];
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( const DVec2<Real>* v, bool columns )
{
	if( columns )
	{
		_entry[ 0 ] = v[ 0 ][ 0 ];
		_entry[ 1 ] = v[ 1 ][ 0 ];
		_entry[ 2 ] = v[ 0 ][ 1 ];
		_entry[ 3 ] = v[ 1 ][ 1 ];
	}
	else
	{
		_entry[ 0 ] = v[ 0 ][ 0 ];
		_entry[ 1 ] = v[ 0 ][ 1 ];
		_entry[ 2 ] = v[ 1 ][ 0 ];
		_entry[ 3 ] = v[ 1 ][ 1 ];
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( Real m00, Real m11 )
{
	MakeDiagonal( m00, m11 );
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( Real angle )
{
	MakeRotation( angle );
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>::Matrix2( const DVec2<Real>& u, const DVec2<Real>& v )
{
	MakeTensorProduct( u, v );
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>& Matrix2<Real>::operator= (const Matrix2& mat)
{
	_entry[ 0 ] = mat._entry[ 0 ];
	_entry[ 1 ] = mat._entry[ 1 ];
	_entry[ 2 ] = mat._entry[ 2 ];
	_entry[ 3 ] = mat._entry[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::MakeZero()
{
	_entry[ 0 ] = (Real)0;
	_entry[ 1 ] = (Real)0;
	_entry[ 2 ] = (Real)0;
	_entry[ 3 ] = (Real)0;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::MakeIdentity()
{
	_entry[ 0 ] = (Real)1;
	_entry[ 1 ] = (Real)0;
	_entry[ 2 ] = (Real)0;
	_entry[ 3 ] = (Real)1;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::MakeDiagonal( Real m00, Real m11 )
{
	_entry[ 0 ] = m00;
	_entry[ 1 ] = (Real)0;
	_entry[ 2 ] = (Real)0;
	_entry[ 3 ] = m11;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::MakeRotation( Real angle )
{
	_entry[ 0 ] = DMath<Real>::Cos( angle );
	_entry[ 2 ] = DMath<Real>::Sin( angle );
	_entry[ 1 ] = -_entry[ 2 ];
	_entry[ 3 ] = _entry[ 0 ];
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::MakeTensorProduct( const DVec2<Real>& u,
	const DVec2<Real>& v )
{
	_entry[ 0 ] = u[ 0 ] * v[ 0 ];
	_entry[ 1 ] = u[ 0 ] * v[ 1 ];
	_entry[ 2 ] = u[ 1 ] * v[ 0 ];
	_entry[ 3 ] = u[ 1 ] * v[ 1 ];
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator+ (const Matrix2& mat) const
{
	return Matrix2<Real>
		(
		_entry[ 0 ] + mat._entry[ 0 ],
		_entry[ 1 ] + mat._entry[ 1 ],
		_entry[ 2 ] + mat._entry[ 2 ],
		_entry[ 3 ] + mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator- (const Matrix2& mat)
const
{
	return Matrix2<Real>
		(
		_entry[ 0 ] - mat._entry[ 0 ],
		_entry[ 1 ] - mat._entry[ 1 ],
		_entry[ 2 ] - mat._entry[ 2 ],
		_entry[ 3 ] - mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator* (Real scalar) const
{
	return Matrix2<Real>
		(
		scalar*_entry[ 0 ],
		scalar*_entry[ 1 ],
		scalar*_entry[ 2 ],
		scalar*_entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator/ (Real scalar) const
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		return Matrix2<Real>
			(
			invScalar*_entry[ 0 ],
			invScalar*_entry[ 1 ],
			invScalar*_entry[ 2 ],
			invScalar*_entry[ 3 ]
			);
	}
	else
	{
		return Matrix2<Real>
			(
			DMath<Real>::MAX_REAL,
			DMath<Real>::MAX_REAL,
			DMath<Real>::MAX_REAL,
			DMath<Real>::MAX_REAL
			);
	}
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator- () const
{
	return Matrix2<Real>
		(
		-_entry[ 0 ],
		-_entry[ 1 ],
		-_entry[ 2 ],
		-_entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>& Matrix2<Real>::operator+= (const Matrix2& mat)
{
	_entry[ 0 ] += mat._entry[ 0 ];
	_entry[ 1 ] += mat._entry[ 1 ];
	_entry[ 2 ] += mat._entry[ 2 ];
	_entry[ 3 ] += mat._entry[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>& Matrix2<Real>::operator-= (const Matrix2& mat)
{
	_entry[ 0 ] -= mat._entry[ 0 ];
	_entry[ 1 ] -= mat._entry[ 1 ];
	_entry[ 2 ] -= mat._entry[ 2 ];
	_entry[ 3 ] -= mat._entry[ 3 ];
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>& Matrix2<Real>::operator*= (Real scalar)
{
	_entry[ 0 ] *= scalar;
	_entry[ 1 ] *= scalar;
	_entry[ 2 ] *= scalar;
	_entry[ 3 ] *= scalar;
	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real>& Matrix2<Real>::operator/= (Real scalar)
{
	if( scalar != (Real)0 )
	{
		Real invScalar = ((Real)1) / scalar;
		_entry[ 0 ] *= invScalar;
		_entry[ 1 ] *= invScalar;
		_entry[ 2 ] *= invScalar;
		_entry[ 3 ] *= invScalar;
	}
	else
	{
		_entry[ 0 ] = DMath<Real>::MAX_REAL;
		_entry[ 1 ] = DMath<Real>::MAX_REAL;
		_entry[ 2 ] = DMath<Real>::MAX_REAL;
		_entry[ 3 ] = DMath<Real>::MAX_REAL;
	}

	return *this;
}
//----------------------------------------------------------------------------
template <typename Real>
DVec2<Real> Matrix2<Real>::operator* (const DVec2<Real>& vec) const
{
	return DVec2<Real>
		(
		_entry[ 0 ] * vec[ 0 ] + _entry[ 1 ] * vec[ 1 ],
		_entry[ 2 ] * vec[ 0 ] + _entry[ 3 ] * vec[ 1 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Real Matrix2<Real>::QForm( const DVec2<Real>& u, const DVec2<Real>& v )
const
{
	return u.Dot( (*this)*v );
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::Transpose() const
{
	return Matrix2<Real>
		(
		_entry[ 0 ],
		_entry[ 2 ],
		_entry[ 1 ],
		_entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::operator* (const Matrix2& mat) const
{
	// A*B
	return Matrix2<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] + _entry[ 1 ] * mat._entry[ 2 ],
		_entry[ 0 ] * mat._entry[ 1 ] + _entry[ 1 ] * mat._entry[ 3 ],
		_entry[ 2 ] * mat._entry[ 0 ] + _entry[ 3 ] * mat._entry[ 2 ],
		_entry[ 2 ] * mat._entry[ 1 ] + _entry[ 3 ] * mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::TransposeTimes( const Matrix2& mat ) const
{
	// A^T*B
	return Matrix2<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] + _entry[ 2 ] * mat._entry[ 2 ],
		_entry[ 0 ] * mat._entry[ 1 ] + _entry[ 2 ] * mat._entry[ 3 ],
		_entry[ 1 ] * mat._entry[ 0 ] + _entry[ 3 ] * mat._entry[ 2 ],
		_entry[ 1 ] * mat._entry[ 1 ] + _entry[ 3 ] * mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::TimesTranspose( const Matrix2& mat ) const
{
	// A*B^T
	return Matrix2<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] + _entry[ 1 ] * mat._entry[ 1 ],
		_entry[ 0 ] * mat._entry[ 2 ] + _entry[ 1 ] * mat._entry[ 3 ],
		_entry[ 2 ] * mat._entry[ 0 ] + _entry[ 3 ] * mat._entry[ 1 ],
		_entry[ 2 ] * mat._entry[ 2 ] + _entry[ 3 ] * mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::TransposeTimesTranspose( const Matrix2& mat )
const
{
	// A^T*B^T
	return Matrix2<Real>
		(
		_entry[ 0 ] * mat._entry[ 0 ] + _entry[ 2 ] * mat._entry[ 1 ],
		_entry[ 0 ] * mat._entry[ 2 ] + _entry[ 2 ] * mat._entry[ 3 ],
		_entry[ 1 ] * mat._entry[ 0 ] + _entry[ 3 ] * mat._entry[ 1 ],
		_entry[ 1 ] * mat._entry[ 2 ] + _entry[ 3 ] * mat._entry[ 3 ]
		);
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::Inverse( const Real epsilon ) const
{
	Matrix2<Real> inverse;

	Real det = _entry[ 0 ] * _entry[ 3 ] - _entry[ 1 ] * _entry[ 2 ];
	if( DMath<Real>::FAbs( det ) > epsilon )
	{
		Real invDet = ((Real)1) / det;
		inverse._entry[ 0 ] = _entry[ 3 ] * invDet;
		inverse._entry[ 1 ] = -_entry[ 1 ] * invDet;
		inverse._entry[ 2 ] = -_entry[ 2 ] * invDet;
		inverse._entry[ 3 ] = _entry[ 0 ] * invDet;
	}
	else
	{
		inverse._entry[ 0 ] = (Real)0;
		inverse._entry[ 1 ] = (Real)0;
		inverse._entry[ 2 ] = (Real)0;
		inverse._entry[ 3 ] = (Real)0;
	}

	return inverse;
}
//----------------------------------------------------------------------------
template <typename Real>
Matrix2<Real> Matrix2<Real>::Adjoint() const
{
	return Matrix2<Real>( _entry[ 3 ], -_entry[ 1 ], -_entry[ 2 ], _entry[ 0 ] );
}
//----------------------------------------------------------------------------
template <typename Real>
Real Matrix2<Real>::Determinant() const
{
	return _entry[ 0 ] * _entry[ 3 ] - _entry[ 1 ] * _entry[ 2 ];
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::ExtractAngle( Real& angle ) const
{
	// assert:  'this' matrix represents a rotation
	angle = DMath<Real>::ATan2( _entry[ 2 ], _entry[ 0 ] );
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::Orthonormalize()
{
	// Algorithm uses Gram-Schmidt orthogonalization.  If 'this' matrix is
	// M = [m0|m1], then orthonormal output matrix is Q = [q0|q1],
	//
	//   q0 = m0/|m0|
	//   q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
	//
	// where |V| indicates length of vector V and A*B indicates dot
	// product of vectors A and B.

	// Compute q0.
	Real invLength = DMath<Real>::InvSqrt( _entry[ 0 ] * _entry[ 0 ] +
		_entry[ 2 ] * _entry[ 2 ] );

	_entry[ 0 ] *= invLength;
	_entry[ 2 ] *= invLength;

	// Compute q1.
	Real dot0 = _entry[ 0 ] * _entry[ 1 ] + _entry[ 2 ] * _entry[ 3 ];
	_entry[ 1 ] -= dot0*_entry[ 0 ];
	_entry[ 3 ] -= dot0*_entry[ 2 ];

	invLength = DMath<Real>::InvSqrt( _entry[ 1 ] * _entry[ 1 ] +
		_entry[ 3 ] * _entry[ 3 ] );

	_entry[ 1 ] *= invLength;
	_entry[ 3 ] *= invLength;
}
//----------------------------------------------------------------------------
template <typename Real>
void Matrix2<Real>::EigenDecomposition( Matrix2& rot, Matrix2& diag ) const
{
	Real trace = _entry[ 0 ] + _entry[ 3 ];
	Real diff = _entry[ 0 ] - _entry[ 3 ];
	Real discr = DMath<Real>::Sqrt( diff*diff + ((Real)4)*_entry[ 1 ] * _entry[ 1 ] );
	Real eigVal0 = ((Real)0.5)*(trace - discr);
	Real eigVal1 = ((Real)0.5)*(trace + discr);
	diag.MakeDiagonal( eigVal0, eigVal1 );

	Real cs, sn;
	if( diff >= (Real)0 )
	{
		cs = _entry[ 1 ];
		sn = eigVal0 - _entry[ 0 ];
	}
	else
	{
		cs = eigVal0 - _entry[ 3 ];
		sn = _entry[ 1 ];
	}
	Real invLength = DMath<Real>::InvSqrt( cs*cs + sn*sn );
	cs *= invLength;
	sn *= invLength;

	rot._entry[ 0 ] = cs;
	rot._entry[ 1 ] = -sn;
	rot._entry[ 2 ] = sn;
	rot._entry[ 3 ] = cs;
}
//----------------------------------------------------------------------------
template <typename Real>
inline Matrix2<Real> operator* (Real scalar, const Matrix2<Real>& mat)
{
	return mat*scalar;
}
//----------------------------------------------------------------------------
template <typename Real>
inline DVec2<Real> operator* (const DVec2<Real>& vec,
	const Matrix2<Real>& mat)
{
	return DVec2<Real>
		(
		vec[ 0 ] * mat[ 0 ][ 0 ] + vec[ 1 ] * mat[ 1 ][ 0 ],
		vec[ 0 ] * mat[ 0 ][ 1 ] + vec[ 1 ] * mat[ 1 ][ 1 ]
		);
}
//----------------------------------------------------------------------------
