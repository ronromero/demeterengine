#pragma once

#include "DLogger.h"
#include "DFile.h"

namespace Demeter
{
	class FileLogger : public DLogger
	{
	public:
		FileLogger( void );
		~FileLogger( void );

		virtual void PublishLog( const char* iLogName );
		virtual void PublishAllLogs( void );

	private:
		FileLogger( const FileLogger& iFileLogger );
		DFile _file;
	};
}