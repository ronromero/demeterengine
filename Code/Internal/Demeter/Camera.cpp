#include "Exports/precompiled.h"

#include "Exports/Camera.h"

namespace Demeter
{
	Camera::Camera( const DVec3f iPos,
		const DVec3f iRot,
		const float iFovInRad,
		const float iAspectRatio,
		const float zNear,
		const float zFar ) : DGameObject( iPos, DVec3f::One, iRot )
	{
		Projection( iFovInRad, iAspectRatio, zNear, zFar );
	}

	const D3DXMATRIX Camera::View( void ) const
	{
		D3DXVECTOR3 pos( _transform->Position.X(),
			_transform->Position.Y(), _transform->Position.Z() );
		D3DXMATRIX view;
		{
			D3DXQUATERNION rot;
			D3DXQuaternionRotationYawPitchRoll( &rot, _transform->Rotation.X(),
				_transform->Rotation.Y(), _transform->Rotation.Z() );
			D3DXMatrixTransformation( &view, NULL, NULL, NULL, NULL, &rot, &pos );
			D3DXMatrixInverse( &view, NULL, &view );
		}
		return view;
	}

	void Camera::Projection( const float iFovInRad,
		const float iAspectRatio,
		const float zNear,
		const float zFar )
	{
		_fovy = iFovInRad;
		_aspect = iAspectRatio;
		_near = zNear;
		_far = zFar;
		D3DXMatrixPerspectiveFovLH( &_proj, _fovy, _aspect, _near, _far );
	}

	const D3DXMATRIX Camera::Projection( void ) const
	{
		return _proj;
	}
}