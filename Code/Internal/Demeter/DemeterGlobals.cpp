#include "Exports/precompiled.h"
#include "Exports/DProfiler.h"
#include "Exports/FileLogger.h"

namespace Demeter
{
	Profiler gProfiler;
	FileLogger gProfilerLog;
}