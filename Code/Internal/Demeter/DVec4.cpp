// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

#include "Exports/precompiled.h"
#include "Exports/DVector.h"

namespace Demeter
{
	template<> const DVec4<float> DVec4<float>::Zero( 0.0f, 0.0f, 0.0f, 0.0f );
	template<> const DVec4<float> DVec4<float>::UnitX( 1.0f, 0.0f, 0.0f, 0.0f );
	template<> const DVec4<float> DVec4<float>::UnitY( 0.0f, 1.0f, 0.0f, 0.0f );
	template<> const DVec4<float> DVec4<float>::UnitZ( 0.0f, 0.0f, 1.0f, 0.0f );
	template<> const DVec4<float> DVec4<float>::UnitW( 0.0f, 0.0f, 0.0f, 1.0f );
	template<> const DVec4<float> DVec4<float>::One( 1.0f, 1.0f, 1.0f, 1.0f );

	template<> const DVec4<double> DVec4<double>::Zero( 0.0, 0.0, 0.0, 0.0 );
	template<> const DVec4<double> DVec4<double>::UnitX( 1.0, 0.0, 0.0, 0.0 );
	template<> const DVec4<double> DVec4<double>::UnitY( 0.0, 1.0, 0.0, 0.0 );
	template<> const DVec4<double> DVec4<double>::UnitZ( 0.0, 0.0, 1.0, 0.0 );
	template<> const DVec4<double> DVec4<double>::UnitW( 0.0, 0.0, 0.0, 1.0 );
	template<> const DVec4<double> DVec4<double>::One( 1.0, 1.0, 1.0, 1.0 );
}