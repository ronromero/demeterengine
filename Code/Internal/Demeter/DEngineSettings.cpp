#include "Exports/precompiled.h"

#include "Exports/DEngineSettings.h"
#include <Parrhasius/Exports/PEngineSettings.h>

namespace Demeter
{

	DEngineSettings::DEngineSettings( void ) : RenderSettings( new Parrhasius::PEngineSettings() ) {}
	DEngineSettings::~DEngineSettings( void )
	{
		SDELETE( RenderSettings );
	}
}