#include "Exports/precompiled.h"

#include "Exports/DRenderable.h"
#include "Exports/Camera.h"
#include "Exports/Light.h"
#include <Parrhasius/Exports/Mesh.h>
#include <Parrhasius/Exports/Material.h>
#include <Parrhasius/Exports/ShaderAssembly.h>
#include <assert.h>

#ifndef PIX_EVENT_ENABLED
#define PIX_EVENT_ENABLED
#endif

namespace Demeter
{
	D1_IMPLEMENT_RTTI( Demeter, DGameComponent, DRenderable );

	DRenderable::DRenderable( const char *iMaterialPath,
		const char *iMeshPath, const char *iTexturePath )
		: DGameComponent( NULL ),
		_mesh( NULL )
		, _material( new Parrhasius::Material() )
		, _materialPath( NULL )
		, _meshPath( NULL )
		, _texturePath( NULL )
	{
		{
			size_t len = strlen( iMaterialPath ) + 1;
			_materialPath = new char[ len ];
			memcpy( const_cast<char*>(_materialPath), iMaterialPath, len );
		}
		{
			size_t len = strlen( iMeshPath ) + 1;
			_meshPath = new char[ len ];
			memcpy( const_cast<char*>(_meshPath), iMeshPath, len );
		}
		{
			size_t len = strlen( iTexturePath ) + 1;
			_texturePath = new char[ len ];
			memcpy( const_cast<char*>(_texturePath), iTexturePath, len );
		}
	}

	DRenderable::~DRenderable( void )
	{
		SDELETE( _mesh );
		SDELETE( _material );
		SDELETE( _materialPath );
		SDELETE( _meshPath );
		SDELETE( _texturePath );
	}

	void DRenderable::Load( IDirect3DDevice9 *iGraphicsDevice )
	{
		if( Parrhasius::LoadMaterial( _materialPath, _material ) )
		{
			_material->Load( iGraphicsDevice, _texturePath );
			_modelToWorld = _material->Shaders()->SetVertexHandle( "g_transform_modelToWorld" );
			assert( _modelToWorld );
			_worldToView = _material->Shaders()->SetVertexHandle( "g_transform_worldToView" );
			assert( _worldToView );
			_viewToScreen = _material->Shaders()->SetVertexHandle( "g_transform_viewToScreen" );
			assert( _viewToScreen );

			_lightAmbientColor = _material->Shaders()->SetPixelHandle( "g_ambient_color" );
			assert( _lightAmbientColor );
			_lightDiffuseColor = _material->Shaders()->SetPixelHandle( "g_diffuse_color" );
			assert( _lightDiffuseColor );
			_lightDiffuseDir = _material->Shaders()->SetPixelHandle( "g_diffuse_dir" );
			assert( _lightDiffuseDir );

			D3DXHANDLE samplerHandle = _material->Shaders()->PixelConstantTable()->GetConstantByName( NULL, "g_color_sampler" );
			_material->Shaders()->SetSamplerRegister( samplerHandle );
		}

		if( !_mesh )
		{
			_mesh = new Parrhasius::Mesh();
		}
		_mesh->Load( iGraphicsDevice, _meshPath );
	}

	void DRenderable::Draw( IDirect3DDevice9 *iGraphicsDevice, Camera *iCamera, Light *iLight )
	{
		if( _material )
		{
			{
#ifdef PIX_EVENT_ENABLED
				D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"Set Material(per-view and per-Material)" );
#endif
				_material->Shaders()->Activate( iGraphicsDevice );

				assert( _worldToView );
				_material->Shaders()->VertexConstantTable()->SetMatrix( iGraphicsDevice, _worldToView, &iCamera->View() );
				assert( _viewToScreen );
				_material->Shaders()->VertexConstantTable()->SetMatrix( iGraphicsDevice, _viewToScreen, &iCamera->Projection() );
				_material->SetTexture( iGraphicsDevice );
#ifdef PIX_EVENT_ENABLED
				D3DPERF_EndEvent();
#endif

#ifdef PIX_EVENT_ENABLED
				D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"Set Material(per-instance)" );
#endif
				assert( _modelToWorld );
				D3DXMATRIX transform( GameObject->_transform->WorldTransform() );
				_material->Shaders()->VertexConstantTable()->SetMatrix( iGraphicsDevice, _modelToWorld, &transform );

#ifdef PIX_EVENT_ENABLED
				D3DPERF_EndEvent();
#endif

#ifdef PIX_EVENT_ENABLED
				D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"Set Light(per-instance)" );
#endif
				_material->Shaders()->PixelConstantTable()->SetFloatArray( iGraphicsDevice, _lightAmbientColor, &iLight->_ambientColor[ 0 ], 3 );
				_material->Shaders()->PixelConstantTable()->SetFloatArray( iGraphicsDevice, _lightDiffuseColor, &iLight->_diffuseColor[ 0 ], 3 );
				_material->Shaders()->PixelConstantTable()->SetFloatArray( iGraphicsDevice, _lightDiffuseDir, &iLight->_diffuseDirection[ 0 ], 3 );
#ifdef PIX_EVENT_ENABLED
				D3DPERF_EndEvent();
#endif
			}
		}

		if( _mesh )
		{
#ifdef PIX_EVENT_ENABLED
			D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 255 ), L"Draw Mesh" );
#endif
			_mesh->Draw( iGraphicsDevice );
#ifdef PIX_EVENT_ENABLED
			D3DPERF_EndEvent();
#endif
		}
	}
}