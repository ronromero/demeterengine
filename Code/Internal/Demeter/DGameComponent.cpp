#include "Exports/precompiled.h"

#include "Exports/DGameObject.h"
#include "Exports/DGameComponent.h"
#include "Exports/DVector.h"

namespace Demeter
{
	D1_IMPLEMENT_RTTI( Demeter, DObject, DGameComponent );
	DGameComponent::DGameComponent( DGameObject *iObj ) :
		DObject(), GameObject( iObj ), IsEnabled( true )
	{}
	DGameComponent::~DGameComponent( void )
	{}



	D1_IMPLEMENT_RTTI( Demeter, DGameComponent, DTransform );
	DTransform::DTransform( void ) : DGameComponent( NULL ), _parent( NULL ),
		Position( DVec3f::Zero ), Scale( DVec3f::One ), Rotation( DVec3f::Zero )
	{}
	DTransform::DTransform( DVec3f iPos, DVec3f iScale, DVec3f iEulerRotXYZ ) :
		DGameComponent( NULL ), _parent( NULL ), Position( iPos ),
		Scale( iScale ), Rotation( iEulerRotXYZ )
	{}
	DTransform::~DTransform( void )
	{
		SDELETE( _parent );
	}
	Matrix4f DTransform::LocalTransform( void ) const
	{
		D3DXMATRIX pos;
		D3DXMATRIX rot;
		D3DXMATRIX scale;
		D3DXMATRIX ident;
		D3DXMATRIX trans;

		D3DXMatrixIdentity( &ident );
		D3DXMatrixScaling( &scale, Scale.X(), Scale.Y(), Scale.Z() );
		D3DXMatrixTranslation( &pos, Position.X(), Position.Y(), Position.Z() );
		D3DXMatrixRotationYawPitchRoll( &rot, Rotation.X(), Rotation.Y(), Rotation.Z() );

		D3DXMatrixMultiply( &trans, &ident, &scale );
		D3DXMatrixMultiply( &trans, &trans, &rot );
		D3DXMatrixMultiply( &trans, &trans, &pos );

		return Matrix4f( &trans._11, true );
	}
	Matrix4f DTransform::WorldTransform( void ) const
	{
		if( _parent != NULL )
		{
			return _parent->WorldTransform() * LocalTransform();
		}
		return LocalTransform();
	}



	D1_IMPLEMENT_RTTI( Demeter, DGameComponent, DController );
	DController::DController( DGameObject* iObj )
		: DGameComponent( iObj )
	{}
	DController::~DController( void ) {}



	D1_IMPLEMENT_RTTI( Demeter, DGameComponent, DRigidBody );
	DRigidBody::DRigidBody( DReal iMass ) : DGameComponent( NULL ),
		Mass( iMass ), Force( DVec3f::Zero ), Velocity( DVec3f::Zero )
	{}
	DRigidBody::~DRigidBody( void ) {}



	D1_IMPLEMENT_RTTI( Demeter, DGameComponent, DCollider );
	DCollider::DCollider( const DVec3f iCenter, const unsigned int iLayer, const unsigned int iMask )
		: DGameComponent( NULL ), Center( iCenter ), Layer( iLayer ), Mask( iMask )
	{}
	DCollider::~DCollider( void ) {}



	D1_IMPLEMENT_RTTI( Demeter, DCollider, DSphereCollider );
	DSphereCollider::DSphereCollider( const DVec3f iCenter, const DReal iRadius,
		const unsigned int iLayer, const unsigned int iMask )
		: DCollider( iCenter, iLayer, iMask ), Radius( iRadius )
	{}
	DSphereCollider::~DSphereCollider( void ) {}

	bool DSphereCollider::Intersects( const DCollider *iCollider, HitInfo *oHitInfo )
	{
		if( iCollider->GetRttiType().IsExactly( DSphereCollider::TYPE ) )
		{
			return Intersects( (DSphereCollider*)iCollider, oHitInfo );
		}
		return false;
	}

	bool DSphereCollider::Intersects( const DSphereCollider *iCollider, HitInfo *oHitInfo )
	{
		if( !oHitInfo )
			return false;
		oHitInfo->GameObject = GameObject;
		oHitInfo->OtherGameObject = iCollider->GameObject;
		oHitInfo->Collider = this;
		oHitInfo->OtherCollider = const_cast<DSphereCollider*>(iCollider);
		DVec3f aCenter = oHitInfo->GameObject->_transform->Position + Center;
		DVec3f bCenter = oHitInfo->OtherGameObject->_transform->Position + iCollider->Center;
		oHitInfo->Normal = (aCenter - bCenter);

		DReal dist = oHitInfo->Normal.Normalize();
		DReal radii = Radius + iCollider->Radius;

		return dist < radii;
	}
}