#include "Exports/precompiled.h"

#include <Parrhasius/Exports/Parrhasius.h>

#include "Exports/DEngineSettings.h"
#include "Exports/DEngine.h"
#include "Exports/DRenderable.h"
#include "Exports/Camera.h"
#include "Exports/Light.h"
#include "Exports/DScene.h"
#include "Exports/DContentManager.h"

#include <d3d9.h>

#include "Exports/Time.h"
#include "Exports/UserInput.h"

namespace Demeter
{
	namespace
	{
		bool s_showRenderlines = false;
		bool s_prevKey = false;
		bool s_currKey = false;
	}

	void LoadDEngineSettings( HINSTANCE hInstance, PSTR lpCmdLine, int nCmdShow, DEngineSettings &oSettings )
	{
		const char *filename = "./demeter.ini";
		oSettings.RenderSettings->Width = GetPrivateProfileInt( "graphics", "width", 800, filename );
		oSettings.RenderSettings->Height = GetPrivateProfileInt( "graphics", "height", 600, filename );
		char buffer[ 7 ];
		ZeroMemory( buffer, 7 );
		GetPrivateProfileString( "graphics", "fullscreen", "False", buffer, 7, filename );
		oSettings.RenderSettings->FullScreen = (strcmp( "False", buffer ) == 0 || strcmp( "false", buffer ) == 0) ? false : true;
	}

	DEngine::DEngine( DEngineSettings &iSettings )
		:
		_renderEngine( NULL ),
		_mainScene( NULL )
	{
		_settings = &iSettings;
	}

	DEngine::~DEngine( void ) {}

	void DEngine::AddScene( std::shared_ptr<DScene> iScene )
	{
		_mainScene = iScene;
	}

	bool DEngine::Initialize( void )
	{
		Parrhasius::PrintToConsole( "DEngine::Initialize()\n" );
		eae6320::Time::Initialize();

		_renderEngine = std::shared_ptr<Parrhasius::PEngine>( new Parrhasius::PEngine( *_settings->RenderSettings ) );

		_contentManager = std::shared_ptr<DContentManager>( new DContentManager( _settings ) );

		bool initialized = _renderEngine->Initialize();
		assert( initialized );
		PDEBUG_INITIALIZE( _renderEngine->Device() );

		return initialized;
	}

	void DEngine::Run( void )
	{
		Parrhasius::PrintToConsole( "DEngine::Run()\n" );

		if( _mainScene && _contentManager )
		{
			_mainScene->Load( _renderEngine->Device(), _contentManager );
		}

		bool quit = false;
		do
		{
			_renderEngine->Service( &quit );
			eae6320::Time::OnNewFrame();

			{ // toggle s_showRenderlines
				s_prevKey = s_currKey;
				s_currKey = eae6320::UserInput::IsKeyPressed( VK_BACK );
				if( !s_prevKey && s_currKey )
				{
					s_showRenderlines = !s_showRenderlines;
				}
			}

			_mainScene->HandleInput();
			_mainScene->Update( eae6320::Time::GetSecondsElapsedThisFrame() );
			_mainScene->HandleCollision();

			//TODO: move to _renderEngine.Draw(_mainScene);
			if( _renderEngine->Device()->TestCooperativeLevel() == S_OK ) // Hack?
			{
				_renderEngine->Clear();
				_renderEngine->BeginScene();
				{
					_mainScene->Draw( _renderEngine->Device() );

					if( s_showRenderlines )
					{
						PDEBUG_ADDLINE( D3DXVECTOR3( -2, 0, 0 ), D3DXVECTOR3( 2, 0, 0 ), D3DCOLOR_XRGB( 255, 0, 0 ) );
						PDEBUG_ADDLINE( D3DXVECTOR3( 0, -2, 0 ), D3DXVECTOR3( 0, 2, 0 ), D3DCOLOR_XRGB( 0, 255, 0 ) );
						PDEBUG_ADDLINE( D3DXVECTOR3( 0, 0, -2 ), D3DXVECTOR3( 0, 0, 2 ), D3DCOLOR_XRGB( 0, 0, 255 ) );
					}

					if( _contentManager &&  _contentManager->_sprites.size() > 0 )
					{
						_renderEngine->BeginSprite();
						{
							_mainScene->DrawSprite( _renderEngine->Device() );
						}
						_renderEngine->EndSprite();
					}
				}
				_renderEngine->EndScene();
				_renderEngine->Present();
			}
		} while( quit == false );
	}

	int DEngine::ShutDown( void )
	{
		if( _mainScene )
		{
			_mainScene->Cleanup();
		}

		Parrhasius::PrintToConsole( "DEngine::ShutDown()\n" );

		PDEBUG_RELEASE();
		_renderEngine->ShutDown();
		return 0;
	}
}