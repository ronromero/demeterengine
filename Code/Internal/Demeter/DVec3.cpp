#include "Exports/precompiled.h"

#include "Exports/DVector.h"

namespace Demeter
{
	template<> const DVec3<float> DVec3<float>::Zero( 0.0f, 0.0f, 0.0f );
	template<> const DVec3<float> DVec3<float>::UnitX( 1.0f, 0.0f, 0.0f );
	template<> const DVec3<float> DVec3<float>::UnitY( 0.0f, 1.0f, 0.0f );
	template<> const DVec3<float> DVec3<float>::UnitZ( 0.0f, 0.0f, 1.0f );
	template<> const DVec3<float> DVec3<float>::One( 1.0f, 1.0f, 1.0f );

	template<> const DVec3<double> DVec3<double>::Zero( 0.0, 0.0, 0.0 );
	template<> const DVec3<double> DVec3<double>::UnitX( 1.0, 0.0, 0.0 );
	template<> const DVec3<double> DVec3<double>::UnitY( 0.0, 1.0, 0.0 );
	template<> const DVec3<double> DVec3<double>::UnitZ( 0.0, 0.0, 1.0 );
	template<> const DVec3<double> DVec3<double>::One( 1.0, 1.0, 1.0 );
}