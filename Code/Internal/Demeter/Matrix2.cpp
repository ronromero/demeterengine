// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

#include "Exports/precompiled.h"

#include "Exports/DMatrix.h"

namespace Demeter
{
	template<> const Matrix2<float> Matrix2<float>::Zero( true );
	template<> const Matrix2<float> Matrix2<float>::Identity( false );

	template<> const Matrix2<double> Matrix2<double>::Zero( true );
	template<> const Matrix2<double> Matrix2<double>::Identity( false );
}
