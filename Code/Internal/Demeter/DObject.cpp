#include "Exports/precompiled.h"

#include "Exports/DObject.h"
#include "Exports/IObservable.h"
#include "Exports/IObserver.h"
#include "Exports/HashedString.h"
#include "Exports/ScopedTimer.h"

namespace Demeter
{
	unsigned int DObject::s_id = 0;

	const Rtti DObject::TYPE( "Demeter.DObject", 0 );

	DObject::DObject( void ) : _id( s_id++ )
	{
		PROFILE_UNSCOPED( "DObject(void)" );
	}

	DObject::~DObject()
	{
		PROFILE_UNSCOPED( "~DObject(void)" );
	}

	void DObject::Dispose( void )
	{
		PROFILE_UNSCOPED( "DObject::Dispose(void)" );
		BroadcastMessage( "Dispose", NULL );
		_observers.clear();
	}

	const unsigned int DObject::Id( void ) const
	{
		PROFILE_UNSCOPED( "DObject.Id()" );
		return _id;
	}

	void DObject::Register( const HashedString &iMessage, IObserver* iObserver )
	{
		PROFILE_UNSCOPED( "Object.Register(HashedString&, IObserver*)" );
		_observers[ iMessage ].push_back( iObserver );
	}

	void DObject::UnRegister( const HashedString &iMessage, IObserver *iObserver )
	{
		PROFILE_UNSCOPED( "Object.UnRegister(HashedString&, IObserver*)" );
		if( _observers.find( iMessage ) == _observers.end() ) return;
		std::vector< IObserver* > observers = _observers[ iMessage ];
		std::vector< IObserver* >::iterator obsItr = observers.begin();
		while( obsItr != observers.end() )
		{
			if( *obsItr == iObserver )
			{
				observers.erase( obsItr );
				return;
			}
			obsItr++;
		}
	}

	void DObject::BroadcastMessage( const HashedString &iMessage, void* iMessageData )
	{
		PROFILE_UNSCOPED( "Object.BroadcastMessage(HashedString&, void*)" );
		if( _observers.find( iMessage ) == _observers.end() ) return;
		std::vector< IObserver* > observers = _observers[ iMessage ];
		std::vector< IObserver* >::iterator obsItr = observers.begin();
		while( obsItr != observers.end() )
		{
			(*obsItr)->ProcessMessage( iMessage, *this, iMessageData );
			obsItr++;
		}
	}

	void DObject::BroadcastMessage( const char* iMessage, void* iMessageData )
	{
		PROFILE_UNSCOPED( "Object.BroadcastMessage(const char*, void*)" );
		BroadcastMessage( HashedString( iMessage ), iMessageData );
	}

}