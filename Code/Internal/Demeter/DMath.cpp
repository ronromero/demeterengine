// Geometric Tools, LLC
// Copyright (c) 1998-2010
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
//
// File Version: 5.0.0 (2010/01/01)

#include "Exports/precompiled.h"
#include "Exports/DMath.h"

namespace Demeter
{
	template<> const float DMath<float>::EPSILON = FLT_EPSILON;
	template<> const float DMath<float>::ZERO_TOLERANCE = 1e-06f;
	template<> const float DMath<float>::MAX_REAL = FLT_MAX;
	template<> const float DMath<float>::PI = (float)(4.0*atan( 1.0 ));
	template<> const float DMath<float>::TWO_PI = 2.0f*DMath<float>::PI;
	template<> const float DMath<float>::HALF_PI = 0.5f*DMath<float>::PI;
	template<> const float DMath<float>::INV_PI = 1.0f / DMath<float>::PI;
	template<> const float DMath<float>::INV_TWO_PI = 1.0f / DMath<float>::TWO_PI;
	template<> const float DMath<float>::DEG_TO_RAD = DMath<float>::PI / 180.0f;
	template<> const float DMath<float>::RAD_TO_DEG = 180.0f / DMath<float>::PI;
	template<> const float DMath<float>::LN_2 = DMath<float>::Log( 2.0f );
	template<> const float DMath<float>::LN_10 = DMath<float>::Log( 10.0f );
	template<> const float DMath<float>::INV_LN_2 = 1.0f / DMath<float>::LN_2;
	template<> const float DMath<float>::INV_LN_10 = 1.0f / DMath<float>::LN_10;
	template<> const float DMath<float>::SQRT_2 = (float)(sqrt( 2.0 ));
	template<> const float DMath<float>::INV_SQRT_2 = 1.0f / DMath<float>::SQRT_2;
	template<> const float DMath<float>::SQRT_3 = (float)(sqrt( 3.0 ));
	template<> const float DMath<float>::INV_SQRT_3 = 1.0f / DMath<float>::SQRT_3;

	template<> const double DMath<double>::EPSILON = DBL_EPSILON;
	template<> const double DMath<double>::ZERO_TOLERANCE = 1e-08;
	template<> const double DMath<double>::MAX_REAL = DBL_MAX;
	template<> const double DMath<double>::PI = 4.0*atan( 1.0 );
	template<> const double DMath<double>::TWO_PI = 2.0*DMath<double>::PI;
	template<> const double DMath<double>::HALF_PI = 0.5*DMath<double>::PI;
	template<> const double DMath<double>::INV_PI = 1.0 / DMath<double>::PI;
	template<> const double DMath<double>::INV_TWO_PI = 1.0 / DMath<double>::TWO_PI;
	template<> const double DMath<double>::DEG_TO_RAD = DMath<double>::PI / 180.0;
	template<> const double DMath<double>::RAD_TO_DEG = 180.0 / DMath<double>::PI;
	template<> const double DMath<double>::LN_2 = DMath<double>::Log( 2.0 );
	template<> const double DMath<double>::LN_10 = DMath<double>::Log( 10.0 );
	template<> const double DMath<double>::INV_LN_2 = 1.0 / DMath<double>::LN_2;
	template<> const double DMath<double>::INV_LN_10 = 1.0 / DMath<double>::LN_10;
	template<> const double DMath<double>::SQRT_2 = sqrt( 2.0 );
	template<> const double DMath<double>::INV_SQRT_2 = 1.0f / DMath<float>::SQRT_2;
	template<> const double DMath<double>::SQRT_3 = sqrt( 3.0 );
	template<> const double DMath<double>::INV_SQRT_3 = 1.0f / DMath<float>::SQRT_3;
}