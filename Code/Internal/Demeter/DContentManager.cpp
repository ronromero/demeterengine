#include "Exports/precompiled.h"

#include "Exports/DContentManager.h"
#include "Exports/DGameObject.h"
#include "Exports/DEngineSettings.h"
#include <Parrhasius/Exports/Mesh.h>
#include <Parrhasius/Exports/Texture.h>
#include <Parrhasius/Exports/Material.h>

namespace Demeter
{
	DContentManager::DContentManager( DEngineSettings *iSettings ) :
		_settings( iSettings )
	{}

	DContentManager::~DContentManager( void )
	{}

	std::shared_ptr<Parrhasius::Mesh> DContentManager::LoadMesh( IDirect3DDevice9 *iDevice, const char* iPath )
	{
		if( _meshes.find( iPath ) != _meshes.end() )
		{
			return _meshes[ iPath ];
		}
		//if( FileSystem::PathExists( iPath ) )
		//{
		Parrhasius::Mesh *resource = new Parrhasius::Mesh();
		resource->Load( iDevice, iPath );
		_meshes[ iPath ] = std::shared_ptr<Parrhasius::Mesh>( resource );
		return _meshes[ iPath ];
		//}
		return NULL;
	}

	std::shared_ptr<Parrhasius::Texture> DContentManager::LoadTexture( IDirect3DDevice9 *iDevice, const char* iPath )
	{
		if( _textures.find( iPath ) != _textures.end() )
		{
			return _textures[ iPath ];
		}
		//if( FileSystem::PathExists( iPath ) )
		//{
		Parrhasius::Texture *resource = new Parrhasius::Texture();
		resource->Load( iDevice, iPath );
		_textures[ iPath ] = std::shared_ptr<Parrhasius::Texture>( resource );
		return _textures[ iPath ];
		//}
		return NULL;
	}

	std::shared_ptr<Parrhasius::Material> DContentManager::LoadMaterial( IDirect3DDevice9 *iDevice, const char* iPath )
	{
		if( _materials.find( iPath ) != _materials.end() )
		{
			return _materials[ iPath ];
		}
		//if( FileSystem::PathExists( iPath ) )
		//{
		Parrhasius::Material *resource = new Parrhasius::Material();
		Parrhasius::LoadMaterial( iPath, &resource[ 0 ] );
		//resource->Load( iDevice, "" ); //TODO: fix
		_materials[ iPath ] = std::shared_ptr<Parrhasius::Material>( resource );
		return _materials[ iPath ];
		//}
		return NULL;
	}
}