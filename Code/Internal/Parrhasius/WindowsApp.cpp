#include "Exports/precompiled.h"
#include "Exports/WindowsApp.h"
#include "Exports/PEngineSettings.h"
#include <string>


namespace Parrhasius
{
	namespace
	{
		HWND s_mainWindow = NULL;
		HINSTANCE s_instance = NULL;
		ATOM s_mwClass = NULL;
		LPCSTR s_mainWindowClassName = "Parrhasius Window Class";
		PEngineSettings *s_settings;
	}

	LRESULT CALLBACK WndProc( HWND ihWindow, UINT iMsg, WPARAM wParam, LPARAM lParam );
	bool InitWindows( HINSTANCE ihMainWnd, int iCmdShow );
	ATOM RegisterWindow( HINSTANCE iInstanceHandle );
	bool UnregisterWindow( const HINSTANCE iInstanceHandle );
	std::string GetLastWindowsError();

	WindowsApp::WindowsApp( HINSTANCE ihWnd, int iCmdShow, PEngineSettings &iSettings ) :
		_ihMainWnd( ihWnd ),
		_cmdShow( iCmdShow )
	{
		s_settings = &iSettings;
	}

	WindowsApp::~WindowsApp()
	{}

	HWND WindowsApp::GetHandle( void )
	{
		return s_mainWindow;
	}

	bool WindowsApp::Initialize( void )
	{
		if( InitWindows( _ihMainWnd, _cmdShow ) )
		{
			return true;
		}
		return false;
	}

	bool WindowsApp::Service( bool *oQuit )
	{
		*oQuit = false;

		MSG message = { 0 };
		BOOL gotMsg;
		do
		{
			gotMsg = PeekMessage( &message, NULL, 0U, 0U, PM_REMOVE );
			if( gotMsg )
			{
				if( TranslateAccelerator( s_mainWindow, NULL, &message ) == 0 )
				{
					TranslateMessage( &message );
					DispatchMessage( &message );
				}
				if( message.message == WM_QUIT )
				{
					*oQuit = true;
				}
			}
		} while( (gotMsg == TRUE) && (*oQuit == false) );

		return true;
	}

	ATOM RegisterWindow( HINSTANCE iInstanceHandle )
	{
		WNDCLASSEX wc = { 0 };
		wc.cbSize = sizeof( WNDCLASSEX );
		wc.hInstance = iInstanceHandle;
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = WndProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hIcon = LoadIcon( iInstanceHandle, IDI_WINLOGO );
		wc.hIconSm = LoadIcon( iInstanceHandle, IDI_WINLOGO );
		wc.hCursor = LoadCursor( 0, IDC_ARROW );
		wc.hbrBackground = reinterpret_cast<HBRUSH>(IntToPtr( COLOR_BACKGROUND + 1 ));
		wc.lpszMenuName = NULL;
		wc.lpszClassName = s_mainWindowClassName;

		const ATOM mwc = RegisterClassEx( &wc );
		if( mwc == NULL )
		{
			MessageBox( 0, "RegisterClass FAILED", 0, MB_OK | MB_ICONERROR );
		}
		return mwc;
	}

	std::string GetLastWindowsError()
	{
		// Windows stores the error as a code
		const DWORD errorCode = GetLastError();
		// Format the message
		std::string errorMessage;
		{
			const DWORD formattingOptions = FORMAT_MESSAGE_FROM_SYSTEM
				| FORMAT_MESSAGE_ALLOCATE_BUFFER
				| FORMAT_MESSAGE_IGNORE_INSERTS;
			const void* messageIsFromWindows = NULL;
			const DWORD useTheDefaultLanguage = 0;
			char* messageBuffer = NULL;
			const DWORD minimumCharacterCountToAllocate = 1;
			va_list* insertsAreIgnored = NULL;
			const DWORD storedCharacterCount = FormatMessage( formattingOptions, messageIsFromWindows, errorCode,
				useTheDefaultLanguage, reinterpret_cast<LPSTR>(&messageBuffer), minimumCharacterCountToAllocate, insertsAreIgnored );
			if( storedCharacterCount != 0 )
			{
				errorMessage = messageBuffer;
			}
			else
			{
				errorMessage = "Unknown Windows Error";
			}
			LocalFree( messageBuffer );
		}
		return errorMessage;
	}

	bool InitWindows( HINSTANCE ihMainWnd, int iCmdShow )
	{
		s_instance = ihMainWnd;
		s_mwClass = RegisterWindow( s_instance );
		if( s_mwClass == NULL ) return false;
		const DWORD wStyle =
			s_settings->FullScreen ? WS_OVERLAPPED | WS_POPUP :
			(WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU);
		const DWORD wStyleEx = WS_EX_OVERLAPPEDWINDOW;
		LPCSTR wWindowName = "Game Name Here";
		const int pos_x = CW_USEDEFAULT;
		const int pos_y = CW_USEDEFAULT;
		const int width = CW_USEDEFAULT;
		const int height = CW_USEDEFAULT;
		const HWND hParent = NULL;
		const HMENU hMenu = NULL;
		const HINSTANCE hProgram = s_instance;
		LPVOID userData = NULL;
		s_mainWindow = CreateWindowEx( wStyleEx, s_mainWindowClassName, wWindowName, wStyle,
			pos_x, pos_y, width, height,
			hParent, hMenu, hProgram, userData );

		if( s_mainWindow == NULL )
		{
			MessageBox( NULL, "Create Window FAILED", 0, MB_OK | MB_ICONERROR );
			return false;
		}

		// Change the window's size based on the desired client area resolution
		{
			const int desiredWidth = s_settings->FullScreen ? (int)GetSystemMetrics( SM_CXSCREEN ) : s_settings->Width;
			const int desiredHeight = s_settings->FullScreen ? (int)GetSystemMetrics( SM_CYSCREEN ) : s_settings->Height;
			RECT windowCoordinates;
			struct
			{
				long width;
				long height;
			} nonClientAreaSize;
			{
				// Get the coordinates of the entire window
				if( GetWindowRect( s_mainWindow, &windowCoordinates ) == FALSE )
				{
					std::string errorMessage( "Windows failed to get the coordinates of the main window: " );
					errorMessage += GetLastWindowsError();
					MessageBox( NULL, errorMessage.c_str(), NULL, MB_OK | MB_ICONERROR );
					return false;
				}
				// Get the dimensions of the client area
				RECT clientDimensions;
				if( GetClientRect( s_mainWindow, &clientDimensions ) == FALSE )
				{
					std::string errorMessage( "Windows failed to get the dimensions of the main window's client area: " );
					errorMessage += GetLastWindowsError();
					MessageBox( NULL, errorMessage.c_str(), NULL, MB_OK | MB_ICONERROR );
					return false;
				}
				// Get the difference between them
				nonClientAreaSize.width = (windowCoordinates.right - windowCoordinates.left) - clientDimensions.right;
				nonClientAreaSize.height = (windowCoordinates.bottom - windowCoordinates.top) - clientDimensions.bottom;
			}
			// Resize the window
			{
				BOOL shouldTheResizedWindowBeRedrawn = TRUE;
				if( MoveWindow( s_mainWindow, windowCoordinates.left, windowCoordinates.top,
					desiredWidth + nonClientAreaSize.width, desiredHeight + nonClientAreaSize.height,
					shouldTheResizedWindowBeRedrawn ) == FALSE )
				{
					std::string errorMessage( "Windows failed to resize the main window: " );
					errorMessage += GetLastWindowsError();
					MessageBox( NULL, errorMessage.c_str(), NULL, MB_OK | MB_ICONERROR );
					return false;
				}
			}
		}
		ShowWindow( s_mainWindow, iCmdShow );
		UpdateWindow( s_mainWindow );

		return true;
	}

	LRESULT CALLBACK WndProc( HWND ihWindow, UINT iMsg, WPARAM wParam, LPARAM lParam )
	{
		switch( iMsg )
		{
			case WM_NCDESTROY:
				s_mainWindow = NULL;
				PostQuitMessage( 0 );
				return 0;

		}
		return DefWindowProc( ihWindow, iMsg, wParam, lParam );
	}
}
