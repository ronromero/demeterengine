#include "Exports/precompiled.h"

#include "Exports/Rect.h"

namespace Parrhasius
{
	Rect::Rect() {}
	Rect::Rect( float iX, float iY, float iWidth, float iHeight ) :
		x( iX ), y( iY ), width( iWidth ), height( iHeight )
	{}
}