
namespace Parrhasius
{
	struct PEngineSettings
	{
		int Width;
		int Height;
		bool FullScreen;
	};
}