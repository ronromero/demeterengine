#ifndef _PARRHASIUS_MATL_H_
#define _PARRHASIUS_MATL_H_

#include "precompiled.h"
#include "Material.h"
#include <../External/Lua/Exports/Includes.h>
#include <iostream>
#include <assert.h>
#include "WindowsHelper.h"
#include "ShaderAssembly.h"

namespace Parrhasius
{
	namespace MatL
	{
		bool LoadTableValues_Shaders_paths( lua_State *iState, Material *oMaterial );
		bool LoadTableValues_Shaders( lua_State *iState, Material *oMaterial );
		bool LoadTableValues( lua_State *iState, Material *oMaterial );
		bool LoadAsset( const char* iPath, Material *oMaterial );
		bool LoadShaderConstants( lua_State *iState, Material *oMaterial );
		const char* GetFieldAsString( lua_State *iState, const char *key );
		float GetFloat( lua_State *iState, int idx );

		bool LoadTableValues_Shaders_paths( lua_State *iState, Material *oMaterial )
		{
			std::cout << "Iterating through every shader path:\n";
			std::string errorMsg;
			const char* vertexKey = "vertexShader";
			const char* vertexPath = GetFieldAsString( iState, vertexKey );
			const char* pixelKey = "fragmentShader";
			const char* pixelPath = GetFieldAsString( iState, pixelKey );
			oMaterial->LoadShaderAssembly( vertexPath, pixelPath );

			return true;
		}

		float GetFloat( lua_State *iState, int idx )
		{
			lua_pushinteger( iState, idx );
			lua_gettable( iState, -2 );
			if( !lua_isnumber( iState, -1 ) )
			{
				//TODO: throw error?
				return 0.0f;
			}
			float result = (float)lua_tonumber( iState, -1 );
			lua_pop( iState, 1 );
			return result;
		}

		const char* GetFieldAsString( lua_State *iState, const char *key )
		{
			lua_pushstring( iState, key );
			lua_gettable( iState, -2 );;
			if( !lua_isstring( iState, -1 ) )
			{
				//TODO: throw error?
				return "";
			}
			const char* result = lua_tostring( iState, -1 );
			lua_pop( iState, 1 );
			return result;
		}

		bool LoadTableValues_Shaders( lua_State *iState, Material *oMaterial )
		{
			bool wereThereErrors = false;

			{
				const char* key = "constants";
				lua_pushstring( iState, key );
				lua_gettable( iState, -2 );
				if( lua_istable( iState, -1 ) )
				{
					if( !LoadShaderConstants( iState, oMaterial ) )
					{
						std::cerr << "There was a problem loading the shader constants\n";
					}
				}
				lua_pop( iState, 1 );
			}

			{
				const char* key = "shaders";
				lua_pushstring( iState, key );
				lua_gettable( iState, -2 );

				if( lua_istable( iState, -1 ) )
				{
					if( !LoadTableValues_Shaders_paths( iState, oMaterial ) )
					{
						wereThereErrors = true;
						goto OnExit;
					}
				}
				else
				{
					wereThereErrors = true;
					std::cerr << "The value at \"" << key << "\" must be a table "
						<< "(instead of a " << luaL_typename( iState, -1 ) << ")\n";
					goto OnExit;
				}
			}

OnExit:
			lua_pop( iState, 1 );
			return !wereThereErrors;
		}

		bool LoadShaderConstants( lua_State *iState, Material *oMaterial )
		{
			std::cout << "Iterating through every shader constants:\n";

			{
				const char* key = "g_colorModifier";
				lua_pushstring( iState, key );
				lua_gettable( iState, -2 );
				if( lua_istable( iState, -1 ) )
				{
					float x = GetFloat( iState, 1 );
					float y = GetFloat( iState, 2 );
					float z = GetFloat( iState, 3 );
					float *color = new float[ 3 ];
					color[ 0 ] = x;
					color[ 1 ] = y;
					color[ 2 ] = z;
					oMaterial->SetFloatConstant( key, color );
				}
				lua_pop( iState, 1 );
			}

			return true;
		}

		bool LoadTableValues( lua_State *iState, Material *oMaterial )
		{
			if( !LoadTableValues_Shaders( iState, oMaterial ) )
			{
				return false;
			}

			return true;
		}

		bool LoadAsset( const char* iPath, Material *oMaterial )
		{
			bool wereThereErrors = false;

			lua_State* luaState = luaL_newstate();
			if( !luaState )
			{
				wereThereErrors = true;
				std::cerr << "Failed to create a lua state\n";
				goto OnExit;
			}

			{
				const int result = luaL_loadfile( luaState, iPath );
				if( result != LUA_OK )
				{
					wereThereErrors = true;
					std::cerr << lua_tostring( luaState, -1 );
					lua_pop( luaState, 1 );
					goto OnExit;
				}
			}

			{
				const int argumentCount = 0;
				const int returnValueCount = LUA_MULTRET;
				const int noMessageHandler = 0;
				const int result = lua_pcall( luaState, argumentCount, returnValueCount, noMessageHandler );
				if( result == LUA_OK )
				{
					const int returnedValueCount = lua_gettop( luaState );
					if( returnedValueCount == 1 )
					{ // Well Behaved
						if( !lua_istable( luaState, returnedValueCount ) )
						{ //Not a correct asset file
							wereThereErrors = true;
							std::cerr << "Asset files must return a table (instead of a "
								<< luaL_typename( luaState, -1 ) << " )\n";
							lua_pop( luaState, 1 );
							goto OnExit;
						}
					}
					else
					{ // Not well behaved
						wereThereErrors = true;
						std::cerr << "Asset files must return a single table (instead of "
							<< returnedValueCount << " values)\n";
						lua_pop( luaState, returnedValueCount );
						goto OnExit;
					}
				}
				else
				{
					wereThereErrors = true;
					std::cerr << lua_tostring( luaState, -1 );
					lua_pop( luaState, 1 );
					goto OnExit;
				}
			} // end scope

			if( !LoadTableValues( luaState, oMaterial ) )
			{
				wereThereErrors = true;
			}
			lua_pop( luaState, 1 );

OnExit:
			if( luaState )
			{
				assert( lua_gettop( luaState ) == 0 );
				lua_close( luaState );
				luaState = NULL;
			}

			return !wereThereErrors;
		}
	}
}

#endif //_PARRHASIUS_MATL_H_