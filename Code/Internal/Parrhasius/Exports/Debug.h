#ifndef _PARRHASIUS_DEBUG_H_
#define _PARRHASIUS_DEBUG_H_

#include <d3d9.h>
#include <d3dx9.h>
#include "Mesh.h"

namespace Parrhasius
{
	namespace
	{
		struct sDebugVertex
		{
			float x, y, z;
			D3DCOLOR color;
		};

		D3DVERTEXELEMENT9 s_debugVertexElements[] =
		{
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
			{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
			D3DDECL_END()
		};
	}

	class Debug
	{
	public:
		static void Initialize( IDirect3DDevice9 *iGraphicsDevice );
		static void Release( void );
		static void AddLine( const D3DXVECTOR3 &start, const D3DXVECTOR3 &end, const D3DXCOLOR color = D3DCOLOR_XRGB( 255, 255, 255 ) );
		static void DrawLines( IDirect3DDevice9 *iGraphicsDevice, const D3DXMATRIX *iView, const D3DXMATRIX *iProjection );

	private:
		static void LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage );
	};
}

#endif // _PARRHASIUS_DEBUG_H_