#ifndef _PARRHASIUS_WINDOWSHELPER_H_
#define _PARRHASIUS_WINDOWSHELPER_H_

#include "precompiled.h"
#include <string>

namespace Parrhasius
{
	std::string GetFormattedWindowsError( const DWORD i_errorCode );
	std::string GetLastWindowsError( DWORD* o_optionalErrorCode = NULL );

	void PrintToConsole( const char *iMessage );
	void ErrorMessageBox( const char* iMessage );
};

#endif _PARRHASIUS_WINDOWSHELPER_H_