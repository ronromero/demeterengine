#ifndef _PARRHASIUS_TEXTURE_H_
#define _PARRHASIUS_TEXTURE_H_

#include <d3d9.h>

namespace Parrhasius
{
	class Texture
	{
	public:
		IDirect3DTexture9* _texture;

		Texture( void );
		~Texture( void );

		bool Load( IDirect3DDevice9 *iDevice, const char* iTexturePath );
		bool SetTexture( DWORD iRegister, IDirect3DDevice9 *iDevice );
	};
}

#endif //_PARRHASIUS_TEXTURE_H_