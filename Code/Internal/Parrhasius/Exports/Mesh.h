#ifndef _PARRHASIUS_MESH_H_
#define _PARRHASIUS_MESH_H_

#include <stdint.h>
#include <memory>
#include <d3d9.h>

namespace Parrhasius
{
	namespace
	{
		struct sVertex
		{
			float x, y, z;
			D3DCOLOR color;
			float u, v;
			float nx, ny, nz;
		};

		D3DVERTEXELEMENT9 s_vertexElements[] =
		{
			{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
			{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0 },
			{ 0, 16, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
			{ 0, 24, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0 },
			D3DDECL_END()
		};
	}

	class _MeshData
	{
	public:
		uint32_t numVertices;
		uint32_t numIndices;
		sVertex *vertexBuffer;
		uint32_t *indexBuffer;

		_MeshData( void );
		~_MeshData( void );

		bool Write( const char* iPath );
	};

	bool LoadMeshData( const char* meshPath, _MeshData *iData );

	class Mesh
	{
	public:
		Mesh( void );
		~Mesh( void );

		bool Write( const char* meshPath );
		void Draw( IDirect3DDevice9* iGraphicsDevice );
		void Load( IDirect3DDevice9 *iGraphicsDevice, const char* iPath );

	private:
		uint32_t numVertices;
		uint32_t numIndices;
		IDirect3DIndexBuffer9 *_indexBuffer;
		IDirect3DVertexBuffer9 *_vertexBuffer;
		IDirect3DVertexDeclaration9 *_vertexDeclaration;

		void LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage, _MeshData *iData );
		void LoadIndexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage, _MeshData *iData );
	};
}

#endif //_PARRHASIUS_MESH_H_