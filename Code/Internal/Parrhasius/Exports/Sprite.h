#ifndef _PARRHASIUS_SPRITE_H_
#define _PARRHASIUS_SPRITE_H_

#include <d3d9.h>
#include "Mesh.h"
#include "Rect.h"

namespace Parrhasius
{
	class ShaderAssembly;
	class Texture;
	class Rect;

	class Sprite
	{
	public:
		Sprite( const char* iTexPath, Rect iPosition );
		~Sprite( void );
		void Load( IDirect3DDevice9 *iGraphicsDevice );
		void Draw( IDirect3DDevice9 *iGraphicsDevice );

		void SetTexCoords( const Rect *iTexCoords );
		void SetPosition( const Rect *iPosition );
		void SetColor( D3DCOLOR iColor );

	private:
		IDirect3DVertexBuffer9 *_vertexBuffer;
		IDirect3DVertexDeclaration9 *_vertexDeclaration;
		ShaderAssembly *_shaders;
		Texture *_texture;
		sVertex *_data;

		const char *_texPath;
		D3DCOLOR _color;
		Rect _position;
		Rect _texCoords;

		void LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage );

		Sprite( const Sprite& );
		Sprite& operator=(const Sprite&);
	};
}

#endif // _PARRHASIUS_SPRITE_H_