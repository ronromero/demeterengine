#ifndef _PARRHASIUS_SHADERASSEMBLY_H_
#define _PARRHASIUS_SHADERASSEMBLY_H_

#include <d3d9.h>
#include <d3dx9.h>
#include <map>

namespace Parrhasius
{
	struct _ShaderAssembly
	{
		void *CompiledVertexShader;
		void *CompiledPixelShader;
		IDirect3DVertexShader9 *VertexShader;
		IDirect3DPixelShader9 *PixelShader;
		ID3DXConstantTable *VertexConstTable;
		ID3DXConstantTable *PixelConstTable;
		DWORD SamplerRegister;

		_ShaderAssembly( void )
			: CompiledPixelShader( NULL )
			, CompiledVertexShader( NULL )
			, PixelConstTable( NULL )
			, PixelShader( NULL )
			, VertexConstTable( NULL )
			, VertexShader( NULL )
			, SamplerRegister( NULL )
		{}

		~_ShaderAssembly( void )
		{
			if( PixelConstTable )
			{
				PixelConstTable->Release();
				PixelConstTable = NULL;
			}
			if( VertexConstTable )
			{
				VertexConstTable->Release();
				VertexConstTable = NULL;
			}
			if( VertexShader )
			{
				VertexShader->Release();
				VertexShader = NULL;
			}
			if( PixelShader )
			{
				PixelShader->Release();
				PixelShader = NULL;
			}
		}
	};

	class ShaderAssembly
	{
	public:
		ShaderAssembly( void *iCompiledVertexShader, void *iCompiledPixelShader );

		~ShaderAssembly( void );
		void Load( IDirect3DDevice9 *iGraphicsDevice );
		void Activate( IDirect3DDevice9 *iGraphicsDevice ) const;

		ID3DXConstantTable* VertexConstantTable( void );
		ID3DXConstantTable* PixelConstantTable( void );

		D3DXHANDLE SetVertexHandle( const char* iConstName );
		D3DXHANDLE GetVertexHandle( const char* iConstName );
		D3DXHANDLE SetPixelHandle( const char* iConstName );
		D3DXHANDLE GetPixelHandle( const char* iConstName );
		DWORD GetSamplerRegister( void );
		void SetSamplerRegister( D3DXHANDLE );

	private:
		_ShaderAssembly _assembly;
		std::map<const char*, D3DXHANDLE> _vertexHandles;
		std::map<const char*, D3DXHANDLE> _pixelHandles;
	};
}

#endif //_PARRHASIUS_SHADERASSEMBLY_H_