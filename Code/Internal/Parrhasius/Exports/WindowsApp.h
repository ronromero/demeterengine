#ifndef _PARRHASIUS_WINDOWSAPP_H_
#define _PARRHASIUS_WINDOWSAPP_H_

#include "precompiled.h"
#include <string>

namespace Parrhasius
{
	struct PEngineSettings;

	class WindowsApp
	{
	public:

		HINSTANCE _ihMainWnd;

		WindowsApp( HINSTANCE hWnd, int iCmd, PEngineSettings &iSettings );
		~WindowsApp( void );

		HWND GetHandle( void );

		bool Initialize( void );
		bool Service( bool *oQuit );

	private:
		int _cmdShow;
	};
}

#endif // _PARRHASIUS_WINDOWSAPP_H_