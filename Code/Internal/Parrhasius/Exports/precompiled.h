#ifndef _PARRHASIUS_PRECOMPILED_H_
#define _PARRHASIUS_PRECOMPILED_H_

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

#undef NOMINMAX
#undef WIN32_LEAN_AND_MEAN

#define SAFE_RELEASE(x) if((x)) { (x)->Release(); (x) = NULL; }
#define SDELETE(x) if((x)) { delete (x); (x) = NULL; }

#define DUSE_DEBUGLINES
#if defined(_DEBUG) && defined(DUSE_DEBUGLINES)
#include "Debug.h"
#define DEBUG_INITIALIZE(x) Debug::Initialize((x))
#define DEBUG_RELEASE() Debug::Release()
#define DEBUG_ADDLINE( start, end, color ) Debug::AddLine( (start), (end), (color))
#define DEBUG_DRAWLINES( device, view, proj ) Debug::DrawLines( (device), (view), (proj))
#else
#define DEBUG_INITIALIZE(x) void(0)
#define DEBUG_RELEASE() void(0)
#define DEBUG_ADDLINE( start, end, color ) void(0)
#define DEBUG_DRAWLINES( device, view, proj ) void(0)

#endif

#endif	// _PARRHASIUS_PRECOMPILED_H_
