#ifndef _PARRHASIUS_MATERIAL_H_
#define _PARRHASIUS_MATERIAL_H_

#include <map>
#include <memory>
#include <d3dx9.h>

namespace Parrhasius
{
	class ShaderAssembly;
	class Texture;

	class Material
	{
	public:
		Material( void );
		~Material( void );

		void Load( IDirect3DDevice9 *iGraphicsDevice, const char* iTexturePath );
		bool LoadShaderAssembly( const char* iVsPath, const char* iPsPath );

		ShaderAssembly* Shaders( void );
		bool SetFloatConstant( const char* key, float* iValue );
		float* GetFloatConstant( const char* key );
		bool SetTexture( IDirect3DDevice9 *iGraphicsDevice );

	private:
		ShaderAssembly *_shaderAssembly;
		Texture* _texture;
		std::map<const char*, float*> _constants;

		Material( const Material& );
		Material& operator=(const Material&);
	};

	typedef std::shared_ptr<Material*> MaterialPtr;

	bool LoadMaterial( const char* iFilename, Material *oMaterial );
	bool LoadAndAllocateShaderProgram( const char* i_path, void*& o_compiledShader, std::string* o_errorMessage );
}

#endif //_PARRHASIUS_MATERIAL_H_