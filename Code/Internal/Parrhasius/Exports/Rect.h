#ifndef _PARRHASIUS_RECT_H_
#define _PARRHASIUS_RECT_H_

namespace Parrhasius
{
	class Rect
	{
	public:
		float x, y, width, height;
		Rect(void);
		Rect( float iX, float iY, float iWidth, float iHeight );
	};
}
#endif