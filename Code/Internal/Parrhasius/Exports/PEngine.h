#ifndef _PARRHASIUS_ENGINE_H_
#define _PARRHASIUS_ENGINE_H_

struct IDirect3D9;
struct IDirect3DDevice9;

namespace Parrhasius
{
	struct PEngineSettings;
	class WindowsApp;

	class PEngine
	{
	public:
		PEngine( PEngineSettings &iSettings );
		~PEngine( void );

		bool Initialize( void );
		bool Service( bool *oQuit );
		void Clear( void );
		void BeginScene( void );
		void EndScene( void );
		void BeginSprite( void );
		void EndSprite( void );
		void Present( void );

		void ShutDown( void );
		IDirect3DDevice9* Device( void );

	private:
		IDirect3D9* _direct3dInterface;
		IDirect3DDevice9* _direct3dDevice;
		WindowsApp *_app;
		PEngineSettings *_settings;

		bool CreateDevice( const HWND i_mainWindow );
		bool CreateInterface( const HWND i_mainWindow );

		PEngine( const PEngine& );
		PEngine* operator=(const PEngine&);
	};
}

#endif //_PARRHASIUS_ENGINE_H_