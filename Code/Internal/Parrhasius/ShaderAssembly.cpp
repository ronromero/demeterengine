#include "Exports/precompiled.h"
#include "Exports/ShaderAssembly.h"
#include <assert.h>

namespace Parrhasius
{
	ShaderAssembly::ShaderAssembly( void *iCompiledVertexShader,
		void *iCompiledPixelShader )
	{
		_assembly.CompiledVertexShader = iCompiledVertexShader;
		_assembly.CompiledPixelShader = iCompiledPixelShader;
	}

	ShaderAssembly::~ShaderAssembly( void )
	{}

	void ShaderAssembly::Activate( IDirect3DDevice9 *iGraphicsDevice ) const
	{
		HRESULT result;
		result = iGraphicsDevice->SetVertexShader( _assembly.VertexShader );
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->SetPixelShader( _assembly.PixelShader );
		assert( SUCCEEDED( result ) );
	}

	void ShaderAssembly::Load( IDirect3DDevice9 *iGraphicsDevice )
	{
		{
			const DWORD* compiledVertexShaderPtr =
				reinterpret_cast<DWORD*>(_assembly.CompiledVertexShader);
			iGraphicsDevice->CreateVertexShader( compiledVertexShaderPtr, &_assembly.VertexShader );
			D3DXGetShaderConstantTable( compiledVertexShaderPtr, &_assembly.VertexConstTable );
			delete _assembly.CompiledVertexShader;
			_assembly.VertexConstTable->SetDefaults( iGraphicsDevice );
		}

		{
			const DWORD* compiledPixelShaderPtr =
				reinterpret_cast<DWORD*>(_assembly.CompiledPixelShader);
			iGraphicsDevice->CreatePixelShader( compiledPixelShaderPtr, &_assembly.PixelShader );
			D3DXGetShaderConstantTable( compiledPixelShaderPtr, &_assembly.PixelConstTable );
			delete _assembly.CompiledPixelShader;
			_assembly.PixelConstTable->SetDefaults( iGraphicsDevice );
		}
	}

	ID3DXConstantTable* ShaderAssembly::VertexConstantTable( void )
	{
		return _assembly.VertexConstTable;
	}

	ID3DXConstantTable* ShaderAssembly::PixelConstantTable( void )
	{
		return _assembly.PixelConstTable;
	}

	D3DXHANDLE ShaderAssembly::SetVertexHandle( const char* iConstName )
	{
		if( _assembly.VertexConstTable == NULL ||
			_vertexHandles.find( iConstName ) != _vertexHandles.end() ) return NULL;
		_vertexHandles[ iConstName ] = _assembly.VertexConstTable->GetConstantByName( NULL, iConstName );
		return _vertexHandles[ iConstName ];
	}

	D3DXHANDLE ShaderAssembly::GetVertexHandle( const char* iConstName )
	{
		if( _vertexHandles.find( iConstName ) == _vertexHandles.end() ) return NULL;
		return _vertexHandles[ iConstName ];
	}

	D3DXHANDLE ShaderAssembly::SetPixelHandle( const char* iConstName )
	{
		if( _assembly.PixelConstTable == NULL ||
			_pixelHandles.find( iConstName ) != _pixelHandles.end() ) return NULL;
		_pixelHandles[ iConstName ] = _assembly.PixelConstTable->GetConstantByName( NULL, iConstName );
		return _pixelHandles[ iConstName ];
	}

	D3DXHANDLE ShaderAssembly::GetPixelHandle( const char* iConstName )
	{
		if( _pixelHandles.find( iConstName ) == _pixelHandles.end() ) return NULL;
		return _pixelHandles[ iConstName ];
	}

	DWORD ShaderAssembly::GetSamplerRegister( void )
	{
		return _assembly.SamplerRegister;
	}

	void ShaderAssembly::SetSamplerRegister( D3DXHANDLE samplerHandle )
	{
		if( samplerHandle != NULL )
		{
			_assembly.SamplerRegister = static_cast<DWORD>(_assembly.PixelConstTable->GetSamplerIndex( samplerHandle ));
		}
	}
}