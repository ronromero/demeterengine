#include "Exports/precompiled.h"

#include "Exports/PEngineSettings.h"
#include "Exports/PEngine.h"
#include "Exports/WindowsApp.h"
#include <d3d9.h>
#include <assert.h>

#ifndef PIX_EVENT_ENABLED
#define PIX_EVENT_ENABLED
#endif

namespace Parrhasius
{
	PEngine::PEngine( PEngineSettings &iSettings )
	{
		_settings = &iSettings;
		HINSTANCE hInstance = GetModuleHandle( NULL );
		_app = new WindowsApp( hInstance, 1, iSettings );
	}

	PEngine::~PEngine( void )
	{
		SDELETE( _app );
	}

	bool PEngine::Initialize( void )
	{
		if( _app->Initialize() )
		{
			if( !CreateInterface( _app->GetHandle() ) )
			{
				return false;
			}
			if( !CreateDevice( _app->GetHandle() ) )
			{
				ShutDown();
				return false;
			}
			return true;
		}
		return false;
	}

	bool PEngine::Service( bool *oQuit )
	{
		return _app->Service( oQuit );
		return false;
	}

	void PEngine::Clear( void )
	{
		const D3DRECT* subRectanglesToClear = NULL;
		const DWORD subRectangleCount = 0;
		const DWORD clearTheRenderTarget = D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER;
		D3DCOLOR clearColor = D3DCOLOR_XRGB( 33, 33, 33 );
		const float depthToClear = 1.0f;
		const DWORD noStencilBuffer = 0;
		HRESULT result = _direct3dDevice->Clear( subRectangleCount, subRectanglesToClear,
			clearTheRenderTarget, clearColor, depthToClear, noStencilBuffer );

		assert( SUCCEEDED( result ) );
	}

	void PEngine::BeginScene( void )
	{
		HRESULT result = _direct3dDevice->BeginScene();
		assert( SUCCEEDED( result ) );
	}

	void PEngine::EndScene( void )
	{
		HRESULT result = _direct3dDevice->EndScene();
		assert( SUCCEEDED( result ) );
	}

	void PEngine::BeginSprite( void )
	{
#ifdef PIX_EVENT_ENABLED
		D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"Begin Sprite" );
#endif
		// alpha rendering on
		_direct3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		_direct3dDevice->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
		_direct3dDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
		// depth buffering on
		_direct3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
		_direct3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_FALSE );
#ifdef PIX_EVENT_ENABLED
		D3DPERF_EndEvent();
#endif
	}

	void PEngine::EndSprite( void )
	{
#ifdef PIX_EVENT_ENABLED
		D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"End Sprite" );
#endif
		// alpha off
		_direct3dDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
		// depth off
		_direct3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
		_direct3dDevice->SetRenderState( D3DRS_ZENABLE, D3DZB_TRUE );
		_direct3dDevice->SetRenderState( D3DRS_ZFUNC, D3DCMP_LESSEQUAL );
#ifdef PIX_EVENT_ENABLED
		D3DPERF_EndEvent();
#endif
	}

	void PEngine::Present( void )
	{
		const RECT* noSourceRectangle = NULL;
		const RECT* noDestinationRectangle = NULL;
		const HWND useDefaultWindow = NULL;
		const RGNDATA* noDirtyRegion = NULL;
		HRESULT result = _direct3dDevice->Present( noSourceRectangle, noDestinationRectangle, useDefaultWindow, noDirtyRegion );
		assert( SUCCEEDED( result ) );;
	}

	IDirect3DDevice9* PEngine::Device( void )
	{
		return _direct3dDevice;
	}

	bool PEngine::CreateDevice( const HWND iMainWindow )
	{
		const UINT useDefaultDevice = D3DADAPTER_DEFAULT;
		const D3DDEVTYPE useHardwareRendering = D3DDEVTYPE_HAL;
		const DWORD useHardwareVertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
		D3DPRESENT_PARAMETERS presentationParameters = { 0 };
		{
			presentationParameters.BackBufferWidth = (int)GetSystemMetrics( SM_CXSCREEN );
			presentationParameters.BackBufferHeight = (int)GetSystemMetrics( SM_CYSCREEN );
			presentationParameters.BackBufferFormat = D3DFMT_X8R8G8B8;
			presentationParameters.BackBufferCount = 1;
			presentationParameters.MultiSampleType = D3DMULTISAMPLE_NONE;
			presentationParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
			presentationParameters.hDeviceWindow = iMainWindow;
			presentationParameters.Windowed = _settings->FullScreen ? FALSE : TRUE;
			presentationParameters.EnableAutoDepthStencil = TRUE;
			presentationParameters.AutoDepthStencilFormat = D3DFMT_D16;
			presentationParameters.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
		}
		HRESULT result = _direct3dInterface->CreateDevice( useDefaultDevice, useHardwareRendering,
			iMainWindow, useHardwareVertexProcessing, &presentationParameters, &_direct3dDevice );
		if( SUCCEEDED( result ) )
		{
			return true;
		}
		else
		{
			MessageBox( iMainWindow, "DirectX failed to create a Direct3D9 device", "No D3D9 Device", MB_OK | MB_ICONERROR );
			return false;
		}
	}

	bool PEngine::CreateInterface( const HWND iMainWindow )
	{
		_direct3dInterface = Direct3DCreate9( D3D_SDK_VERSION );
		if( _direct3dInterface )
		{
			return true;
		}
		else
		{
			MessageBox( iMainWindow, "DirectX failed to create a Direct3D9 interface", "No D3D9 Interface", MB_OK | MB_ICONERROR );
			return false;
		}
	}

	void PEngine::ShutDown( void )
	{
		if( _direct3dInterface )
		{
			if( _direct3dDevice )
			{
				_direct3dDevice->Release();
				_direct3dDevice = NULL;
			}
			_direct3dInterface->Release();
			_direct3dInterface = NULL;
		}
	}
}