#include "Exports/precompiled.h"

#include "Exports/Sprite.h"
#include "Exports/ShaderAssembly.h"
#include "Exports/Material.h"
#include "Exports/Texture.h"
#include "Exports/Mesh.h"
#include "Exports/Rect.h"
#include <assert.h>
#include <stdint.h>

#ifndef PIX_EVENT_ENABLED
#define PIX_EVENT_ENABLED
#endif

namespace Parrhasius
{
	Sprite::Sprite( const char* iTexPath, Rect iPosition ) :
		_data( NULL ),
		_vertexBuffer( NULL ),
		_vertexDeclaration( NULL ),
		_shaders( NULL ),
		_texture( NULL ),
		_texPath( iTexPath ),
		_position( iPosition ),
		_texCoords( Rect( 0, 0, 1.0f, 1.0f ) ),
		_color( D3DCOLOR_ARGB( 255, 255, 255, 255 ) )
	{}

	Sprite::~Sprite( void )
	{
		SAFE_RELEASE( _vertexBuffer );
		SAFE_RELEASE( _vertexDeclaration );
		SDELETE( _texture );
		SDELETE( _shaders );
	}

	void Sprite::Load( IDirect3DDevice9 *iGraphicsDevice )
	{
		if( iGraphicsDevice )
		{
			{ //Load Vertex
				DWORD usage = 0;
				{
					usage |= D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY;
					{
						D3DDEVICE_CREATION_PARAMETERS deviceCreationParameters;
						HRESULT result = iGraphicsDevice->GetCreationParameters( &deviceCreationParameters );
						if( SUCCEEDED( result ) )
						{
							DWORD vertexProcessingType = deviceCreationParameters.BehaviorFlags &
								(D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_SOFTWARE_VERTEXPROCESSING);
							usage |= (vertexProcessingType != D3DCREATE_SOFTWARE_VERTEXPROCESSING) ?
								0 : D3DUSAGE_SOFTWAREPROCESSING;
						}
					}
				}
				LoadVertexBuffer( iGraphicsDevice, usage );
			}

			{ // Load Shaders
				std::string errorMsg;
				void *pixelShader;
				void *vertexShader;
				LoadAndAllocateShaderProgram( "data/spritePS.shd", pixelShader, &errorMsg );
				LoadAndAllocateShaderProgram( "data/spriteVS.shd", vertexShader, &errorMsg );
				_shaders = new ShaderAssembly( vertexShader, pixelShader );
				_shaders->Load( iGraphicsDevice );
				D3DXHANDLE samplerHandle = _shaders->PixelConstantTable()->GetConstantByName( NULL, "g_color_sampler" );
				_shaders->SetSamplerRegister( samplerHandle );
			}

			{ //texture
				_texture = new Texture();
				_texture->Load( iGraphicsDevice, _texPath );
			}
		}
	}

	void Sprite::SetTexCoords( const Rect *iTexCoords )
	{
		_texCoords.x = iTexCoords->x;
		_texCoords.y = iTexCoords->y;
		_texCoords.width = iTexCoords->width;
		_texCoords.height = iTexCoords->height;

		if( _data != NULL )
		{
			_data[ 0 ].u = _texCoords.x;
			_data[ 0 ].v = _texCoords.y;
			_data[ 1 ].u = _texCoords.x + _texCoords.width;
			_data[ 1 ].v = _texCoords.y;
			_data[ 2 ].u = _texCoords.x;
			_data[ 2 ].v = _texCoords.y + _texCoords.height;
			_data[ 3 ].u = _texCoords.x + _texCoords.width;
			_data[ 3 ].v = _texCoords.y + _texCoords.height;
		}
	}

	void Sprite::SetPosition( const Rect *iPosition )
	{
		_position.x = iPosition->x;
		_position.y = iPosition->y;
		_position.width = iPosition->width;
		_position.height = iPosition->height;

		if( _data != NULL )
		{
			float halfWidth = _position.width * 0.5f;
			float halfHeight = _position.height * 0.5f;

			_data[ 0 ].x = _position.x - halfWidth;
			_data[ 0 ].y = _position.y + halfHeight;
			_data[ 0 ].z = 0;

			_data[ 1 ].x = _position.x + halfWidth;
			_data[ 1 ].y = _position.y + halfHeight;
			_data[ 1 ].z = 0;

			_data[ 2 ].x = _position.x - halfWidth;
			_data[ 2 ].y = _position.y - halfHeight;
			_data[ 2 ].z = 0;

			_data[ 3 ].x = _position.x + halfWidth;
			_data[ 3 ].y = _position.y - halfHeight;
			_data[ 3 ].z = 0;
		}
	}

	void Sprite::SetColor( D3DCOLOR iColor )
	{
		_color = iColor;
		if( _data != NULL )
		{
			_data[ 0 ].color = _color;
			_data[ 1 ].color = _color;
			_data[ 2 ].color = _color;
			_data[ 3 ].color = _color;
		}
	}

	void Sprite::LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage )
	{
		if( !iGraphicsDevice ) return;
		HRESULT result;
		result = iGraphicsDevice->CreateVertexDeclaration( s_vertexElements, &_vertexDeclaration );
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->SetVertexDeclaration( _vertexDeclaration );
		assert( SUCCEEDED( result ) );

		const unsigned int vertexCount = 4;
		result = iGraphicsDevice->CreateVertexBuffer(
			vertexCount * sizeof( sVertex ),
			iUsage,
			0,
			D3DPOOL_DEFAULT,
			&_vertexBuffer,
			NULL
			);
		assert( SUCCEEDED( result ) );

		const unsigned int bufferLock = 0;
		result = _vertexBuffer->Lock(
			0, // OffsetToLock
			0, // SizeToLock
			reinterpret_cast<void**>(&_data), // data
			0 // locking behavour
			);
		assert( SUCCEEDED( result ) );

		SetPosition( &_position );
		SetTexCoords( &_texCoords );
		SetColor( _color );

		result = _vertexBuffer->Unlock();
		assert( SUCCEEDED( result ) );
	}

	void Sprite::Draw( IDirect3DDevice9* iGraphicsDevice )
	{
#ifdef PIX_EVENT_ENABLED
		D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"DrawSprite" );
#endif
		_shaders->Activate( iGraphicsDevice );
		_texture->SetTexture( 0, iGraphicsDevice );

		// bind vetex buffer
		HRESULT result;
		result = iGraphicsDevice->SetStreamSource(
			0,					// StreamNumber
			_vertexBuffer,		// pStreamData
			0,					// OffsetInBytes
			sizeof( sVertex )	// Stride
			);
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->DrawPrimitive( D3DPT_TRIANGLESTRIP, 0, 2 );
		assert( SUCCEEDED( result ) );

#ifdef PIX_EVENT_ENABLED
		D3DPERF_EndEvent();
#endif
	}
}