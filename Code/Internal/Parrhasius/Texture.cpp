#include "Exports/precompiled.h"

#include "Exports/Texture.h"
#include <d3dx9.h>
#include <fstream>
#include <assert.h>

namespace Parrhasius
{
	Texture::Texture( void ) : _texture( NULL ) {}

	Texture::~Texture( void )
	{
		SAFE_RELEASE( _texture );
	}

	bool Texture::Load( IDirect3DDevice9 *iDevice, const char *iTexturePath )
	{
		HRESULT result = D3DXCreateTextureFromFile( iDevice, iTexturePath, &_texture );
		assert( SUCCEEDED( result ) );
		return true;
	}

	bool Texture::SetTexture( DWORD iRegister, IDirect3DDevice9 *iDevice )
	{
		HRESULT result = iDevice->SetTexture( iRegister, _texture );
		assert( SUCCEEDED( result ) );
		return true;
	}
}