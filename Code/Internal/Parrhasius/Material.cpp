#include "Exports/precompiled.h"
#include "Exports/MatL.h"
#include "Exports/Material.h"
#include "Exports/Texture.h"
#include <Lua/Exports/Includes.h>
#include <string>
#include <iostream>
#include <assert.h>
#include <string>

namespace Parrhasius
{
	bool LoadMaterial( const char* iFilename, Material *oMaterial )
	{
		if( !MatL::LoadAsset( iFilename, oMaterial ) )
		{
			return false;
		}
		return true;
	}

	Material::Material( void )
		: _shaderAssembly( NULL ),
		_texture( new Texture() )
	{}

	Material::~Material( void )
	{
		if( _shaderAssembly )
		{
			delete _shaderAssembly;
			_shaderAssembly = NULL;
		}
		if( _texture )
		{
			delete _texture;
			_texture = NULL;
		}
		std::map<const char*, float*>::iterator itr;
		for( itr = _constants.begin(); itr != _constants.end(); itr++ )
		{
			delete itr->second;
		}
	}

	void Material::Load( IDirect3DDevice9 *iGraphicsDevice, const char *iTexturePath )
	{
		_shaderAssembly->Load( iGraphicsDevice );
		_texture->Load( iGraphicsDevice, iTexturePath );
	}

	bool Material::LoadShaderAssembly( const char* iVsPath, const char* iPsPath )
	{
		void *compiledVertexShader;
		void *compiledPixelShader;
		std::string message;
		LoadAndAllocateShaderProgram( iVsPath, compiledVertexShader, &message ); //TODO: Log message
		LoadAndAllocateShaderProgram( iPsPath, compiledPixelShader, &message ); //TODO: Log message
		_shaderAssembly = new ShaderAssembly( compiledVertexShader, compiledPixelShader );

		return true;
	}

	ShaderAssembly* Material::Shaders( void )
	{
		return _shaderAssembly;
	}

	bool Material::SetFloatConstant( const char* key, float* iValue )
	{
		if( _constants.find( key ) != _constants.end() )
		{
			delete[] _constants[ key ];
		}
		_constants[ key ] = iValue;
		return true;
	}

	float* Material::GetFloatConstant( const char* key )
	{
		if( _constants.find( key ) == _constants.end() )
		{
			return NULL;
		}
		return _constants[ key ];
	}

	bool Material::SetTexture( IDirect3DDevice9 *iGraphicsDevice )
	{
		DWORD sampleRegister = _shaderAssembly->GetSamplerRegister();
		HRESULT result = _texture->SetTexture( sampleRegister, iGraphicsDevice );
		assert( SUCCEEDED( result ) );
		return true;
	}

	bool LoadAndAllocateShaderProgram( const char* i_path, void*& o_compiledShader, std::string* o_errorMessage )
	{
		bool wereThereErrors = false;

		// Load the compiled shader from disk
		o_compiledShader = NULL;
		HANDLE fileHandle = INVALID_HANDLE_VALUE;
		{
			// Open the file
			{
				const DWORD desiredAccess = FILE_GENERIC_READ;
				const DWORD otherProgramsCanStillReadTheFile = FILE_SHARE_READ;
				SECURITY_ATTRIBUTES* useDefaultSecurity = NULL;
				const DWORD onlySucceedIfFileExists = OPEN_EXISTING;
				const DWORD useDefaultAttributes = FILE_ATTRIBUTE_NORMAL;
				const HANDLE dontUseTemplateFile = NULL;
				fileHandle = CreateFile( i_path, desiredAccess, otherProgramsCanStillReadTheFile,
					useDefaultSecurity, onlySucceedIfFileExists, useDefaultAttributes, dontUseTemplateFile );
				if( fileHandle == INVALID_HANDLE_VALUE )
				{
					wereThereErrors = true;
					if( o_errorMessage )
					{
						//std::stringstream errorMessage;
						//errorMessage << "Windows failed to open the shader file: " << GetLastWindowsError();
						//*o_errorMessage = errorMessage.str();
					}
					goto OnExit;
				}
			}
			// Get the file's size
			size_t fileSize;
			{
				LARGE_INTEGER fileSize_integer;
				if( GetFileSizeEx( fileHandle, &fileSize_integer ) != FALSE )
				{
					// This is unsafe if the file's size is bigger than a size_t
					fileSize = static_cast<size_t>(fileSize_integer.QuadPart);
				}
				else
				{
					wereThereErrors = true;
					if( o_errorMessage )
					{
						//std::stringstream errorMessage;
						//errorMessage << "Windows failed to get the size of shader: " << GetLastWindowsError();
						//*o_errorMessage = errorMessage.str();
					}
					goto OnExit;
				}
			}
			// Read the file's contents into temporary memory
			o_compiledShader = malloc( fileSize );
			if( o_compiledShader )
			{
				DWORD bytesReadCount;
				OVERLAPPED* readSynchronously = NULL;
				if( ReadFile( fileHandle, o_compiledShader, fileSize,
					&bytesReadCount, readSynchronously ) == FALSE )
				{
					wereThereErrors = true;
					if( o_errorMessage )
					{
						//std::stringstream errorMessage;
						//errorMessage << "Windows failed to read the contents of shader: " << GetLastWindowsError();
						//*o_errorMessage = errorMessage.str();
					}
					goto OnExit;
				}
			}
			else
			{
				wereThereErrors = true;
				if( o_errorMessage )
				{
					//std::stringstream errorMessage;
					//errorMessage << "Failed to allocate " << fileSize << " bytes to read in the shader program " << i_path;
					//*o_errorMessage = errorMessage.str();
				}
				goto OnExit;
			}
		}

OnExit:

		if( wereThereErrors && o_compiledShader )
		{
			free( o_compiledShader );
			o_compiledShader = NULL;
		}
		if( fileHandle != INVALID_HANDLE_VALUE )
		{
			if( CloseHandle( fileHandle ) == FALSE )
			{
				if( !wereThereErrors && o_errorMessage )
				{
					//std::stringstream errorMessage;
					//errorMessage << "Windows failed to close the shader file handle: " << GetLastWindowsError();
					//*o_errorMessage = errorMessage.str();
				}
				wereThereErrors = true;
			}
			fileHandle = INVALID_HANDLE_VALUE;
		}

		return !wereThereErrors;
	}
}