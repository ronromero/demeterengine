#include "Exports/precompiled.h"

#include "Exports/Debug.h"
#include "Exports/ShaderAssembly.h"
#include "Exports/Material.h"
#include <vector>
#include <assert.h>
#include <stdint.h>

#ifndef PIX_EVENT_ENABLED
#define PIX_EVENT_ENABLED
#endif


namespace Parrhasius
{
	namespace
	{
		IDirect3DVertexBuffer9 *s_debugVertexBuffer = NULL;
		IDirect3DVertexDeclaration9 *s_debugVertexDeclaration = NULL;
		ShaderAssembly *s_debugShaders = NULL;
		sDebugVertex *s_debugData = NULL;
		D3DXHANDLE s_worldToView = NULL;
		D3DXHANDLE s_viewToScreen = NULL;
		std::vector<std::shared_ptr<sDebugVertex>> s_debugVertices;
	}

	void Debug::Initialize( IDirect3DDevice9 *iGraphicsDevice )
	{
		if( iGraphicsDevice )
		{
			{ //Load Vertex
				DWORD usage = 0;
				{
					usage |= D3DUSAGE_WRITEONLY;
					{
						D3DDEVICE_CREATION_PARAMETERS deviceCreationParameters;
						HRESULT result = iGraphicsDevice->GetCreationParameters( &deviceCreationParameters );
						if( SUCCEEDED( result ) )
						{
							DWORD vertexProcessingType = deviceCreationParameters.BehaviorFlags &
								(D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_SOFTWARE_VERTEXPROCESSING);
							usage |= (vertexProcessingType != D3DCREATE_SOFTWARE_VERTEXPROCESSING) ?
								0 : D3DUSAGE_SOFTWAREPROCESSING;
						}
					}
				}
				LoadVertexBuffer( iGraphicsDevice, usage );
			}

			{ // Load Shaders
				std::string errorMsg;
				void *pixelShader;
				void *vertexShader;
				LoadAndAllocateShaderProgram( "data/debugPS.shd", pixelShader, &errorMsg );
				LoadAndAllocateShaderProgram( "data/debugVS.shd", vertexShader, &errorMsg );
				s_debugShaders = new ShaderAssembly( vertexShader, pixelShader );
				s_debugShaders->Load( iGraphicsDevice );
				s_worldToView = s_debugShaders->SetVertexHandle( "g_transform_worldToView" );
				assert( s_worldToView );
				s_viewToScreen = s_debugShaders->SetVertexHandle( "g_transform_viewToScreen" );
				assert( s_viewToScreen );
			}
		}
	}

	void Debug::Release( void )
	{
		SAFE_RELEASE( s_debugVertexBuffer );
		SAFE_RELEASE( s_debugVertexDeclaration );
		SDELETE( s_debugShaders );
	}

	void Debug::LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage )
	{
		if( !iGraphicsDevice ) return;
		HRESULT result;
		result = iGraphicsDevice->CreateVertexDeclaration( s_debugVertexElements, &s_debugVertexDeclaration );
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->SetVertexDeclaration( s_debugVertexDeclaration );
		assert( SUCCEEDED( result ) );

		const unsigned int vertexCount = 512;
		result = iGraphicsDevice->CreateVertexBuffer(
			vertexCount * sizeof( sDebugVertex ),
			iUsage,
			0,
			D3DPOOL_DEFAULT,
			&s_debugVertexBuffer,
			NULL
			);
		assert( SUCCEEDED( result ) );
	}

	void Debug::AddLine( const D3DXVECTOR3 &start, const D3DXVECTOR3 &end, const D3DXCOLOR color )
	{
		sDebugVertex *vertexStart = new sDebugVertex;
		vertexStart->x = start.x;
		vertexStart->y = start.y;
		vertexStart->z = start.z;
		vertexStart->color = color;
		s_debugVertices.push_back( std::shared_ptr<sDebugVertex>( vertexStart ) );

		sDebugVertex *vertexEnd = new sDebugVertex;
		vertexEnd->x = end.x;
		vertexEnd->y = end.y;
		vertexEnd->z = end.z;
		vertexEnd->color = color;
		s_debugVertices.push_back( std::shared_ptr<sDebugVertex>( vertexEnd ) );
	}

	void Debug::DrawLines( IDirect3DDevice9 *iGraphicsDevice, const D3DXMATRIX *iView, const D3DXMATRIX *iProjection )
	{
		unsigned int maxVertices = s_debugVertices.size();
		if( maxVertices == 0 ) return;
		if( maxVertices > 512 ) maxVertices = 512;


#ifdef PIX_EVENT_ENABLED
		D3DPERF_BeginEvent( D3DCOLOR_XRGB( 0, 0, 0 ), L"Debug::DrawLines" );
#endif
		{
			HRESULT result = s_debugVertexBuffer->Lock( 0, 0,
				reinterpret_cast<void**>(&s_debugData), 0 );
			assert( SUCCEEDED( result ) );

			for( unsigned int i = 0; i < maxVertices; ++i )
			{
				s_debugData[ i ].x = s_debugVertices[ i ]->x;
				s_debugData[ i ].y = s_debugVertices[ i ]->y;
				s_debugData[ i ].z = s_debugVertices[ i ]->z;
				s_debugData[ i ].color = s_debugVertices[ i ]->color;
			}
			result = s_debugVertexBuffer->Unlock();
			assert( SUCCEEDED( result ) );
		}

		{
			s_debugShaders->Activate( iGraphicsDevice );

			assert( s_worldToView );
			s_debugShaders->VertexConstantTable()->SetMatrix( iGraphicsDevice, s_worldToView, iView );
			assert( s_viewToScreen );
			s_debugShaders->VertexConstantTable()->SetMatrix( iGraphicsDevice, s_viewToScreen, iProjection );
		}


		// bind vetex buffer
		HRESULT result = iGraphicsDevice->SetStreamSource(
			0,						// StreamNumber
			s_debugVertexBuffer,	// pStreamData
			0,						// OffsetInBytes
			sizeof( sDebugVertex )		// Stride
			);
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->DrawPrimitive( D3DPT_LINELIST, 0, maxVertices );
		assert( SUCCEEDED( result ) );

#ifdef PIX_EVENT_ENABLED
		D3DPERF_EndEvent();
#endif

		s_debugVertices.clear();
	}
}