#include "Exports/precompiled.h"

#include "Exports/Mesh.h"
#include "Exports/Debug.h"
#include <fstream>
#include <d3d.h>
#include <assert.h>

namespace Parrhasius
{
	Mesh::Mesh( void ) : numVertices( 0 ), numIndices( 0 ),
		_indexBuffer( NULL ), _vertexBuffer( NULL ), _vertexDeclaration( NULL )
	{}

	Mesh::~Mesh( void )
	{
		SAFE_RELEASE( _vertexBuffer );
		SAFE_RELEASE( _indexBuffer );
		SAFE_RELEASE( _vertexDeclaration );
	}

	void Mesh::Draw( IDirect3DDevice9* iGraphicsDevice )
	{
		// bind vetex buffer
		HRESULT result;
		result = iGraphicsDevice->SetStreamSource(
			0,				// StreamNumber
			_vertexBuffer,	// pStreamData
			0,				// OffsetInBytes
			sizeof( sVertex )	// Stride
			);
		assert( SUCCEEDED( result ) );

		// set indices to use
		result = iGraphicsDevice->SetIndices( _indexBuffer );
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->DrawIndexedPrimitive(
			D3DPT_TRIANGLELIST,
			0, // BaseVertexIndex
			0, // MinVertexIndex
			numVertices, // NumVertices
			0, // StartIndex
			numIndices / 3 // PrimCount
			);
		assert( SUCCEEDED( result ) );
	}

	void Mesh::Load( IDirect3DDevice9 *iGraphicsDevice, const char* iPath )
	{
		if( iGraphicsDevice )
		{
			DWORD usage = 0;
			{
				// Our code will only ever write to the buffer
				usage |= D3DUSAGE_WRITEONLY;
				// The type of vertex processing should match what was specified when the device interface was created with CreateDevice()
				{
					D3DDEVICE_CREATION_PARAMETERS deviceCreationParameters;
					HRESULT result = iGraphicsDevice->GetCreationParameters( &deviceCreationParameters );
					if( SUCCEEDED( result ) )
					{
						DWORD vertexProcessingType = deviceCreationParameters.BehaviorFlags &
							(D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_SOFTWARE_VERTEXPROCESSING);
						usage |= (vertexProcessingType != D3DCREATE_SOFTWARE_VERTEXPROCESSING) ?
							0 : D3DUSAGE_SOFTWAREPROCESSING;
					}
				}
			}
			_MeshData m;
			if( LoadMeshData( iPath, &m ) )
			{
				numVertices = m.numVertices;
				numIndices = m.numIndices;
				LoadVertexBuffer( iGraphicsDevice, usage, &m );
				LoadIndexBuffer( iGraphicsDevice, usage, &m );
			}
			else
			{
				//TODO: pass filename into error message
				MessageBox( NULL, "Could not find mesh", "Error", MB_OK | MB_ICONHAND );
				exit( 0 );
			}
		}
	}
	void Mesh::LoadIndexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage, _MeshData *iData )
	{
		HRESULT result;
		size_t indexBufferSize = iData->numIndices * sizeof( uint32_t );

		result = iGraphicsDevice->CreateIndexBuffer(
			indexBufferSize,
			iUsage,
			D3DFMT_INDEX32,
			D3DPOOL_DEFAULT,
			&_indexBuffer,
			NULL
			);
		assert( SUCCEEDED( result ) );

		uint32_t *indices = NULL;
		result = _indexBuffer->Lock(
			0,
			0,
			reinterpret_cast<void**>(&indices),
			0
			);
		assert( SUCCEEDED( result ) );

		std::memcpy( indices, iData->indexBuffer, indexBufferSize );

		result = _indexBuffer->Unlock();
		assert( SUCCEEDED( result ) );
	}
	void Mesh::LoadVertexBuffer( IDirect3DDevice9 *iGraphicsDevice, DWORD iUsage, _MeshData *iData )
	{
		if( !iGraphicsDevice ) return;
		HRESULT result;
		result = iGraphicsDevice->CreateVertexDeclaration( s_vertexElements, &_vertexDeclaration );
		assert( SUCCEEDED( result ) );

		result = iGraphicsDevice->SetVertexDeclaration( _vertexDeclaration );
		assert( SUCCEEDED( result ) );

		size_t vertexBufferSize = iData->numVertices * sizeof( sVertex );
		result = iGraphicsDevice->CreateVertexBuffer(
			vertexBufferSize,
			iUsage,
			0,
			D3DPOOL_DEFAULT,
			&_vertexBuffer,
			NULL
			);
		assert( SUCCEEDED( result ) );

		sVertex *data = NULL;
		const unsigned int bufferLock = 0;
		result = _vertexBuffer->Lock(
			0, // OffsetToLock
			0, // SizeToLock
			reinterpret_cast<void**>(&data), // data
			0 // locking behavour
			);
		assert( SUCCEEDED( result ) );

		std::memcpy( data, iData->vertexBuffer, vertexBufferSize );

		result = _vertexBuffer->Unlock();
		assert( SUCCEEDED( result ) );
	}

	_MeshData::_MeshData( void ) : numVertices( 0 ), numIndices( 0 ),
		vertexBuffer( NULL ), indexBuffer( NULL )
	{}
	_MeshData::~_MeshData( void )
	{
		if( vertexBuffer )
		{
			free( vertexBuffer );
			vertexBuffer = NULL;
		}
		if( indexBuffer )
		{
			free( indexBuffer );
			indexBuffer = NULL;
		}
	}
	bool _MeshData::Write( const char* meshPath )
	{
		std::ofstream ofs( meshPath, std::ios::binary );
		ofs.write( reinterpret_cast<char*>(&numVertices), sizeof( uint32_t ) * 2 );
		if( indexBuffer )
			ofs.write( reinterpret_cast<char*>(&indexBuffer[ 0 ]), sizeof( uint32_t ) * numIndices );
		if( vertexBuffer )
			ofs.write( reinterpret_cast<char*>(&vertexBuffer[ 0 ]), sizeof( sVertex ) * numVertices );
		ofs.close();

		return true;
	}
	bool LoadMeshData( const char* meshPath, _MeshData *oMesh )
	{
		if( !oMesh )
		{
			oMesh = new _MeshData();
		}
		std::ifstream ifs( meshPath, std::ios::binary );
		if( !ifs ) return false;

		ifs.read( reinterpret_cast<char*>(&oMesh->numVertices), sizeof( uint32_t ) * 2 );

		int indexBufferSize = sizeof( uint32_t ) * oMesh->numIndices;
		oMesh->indexBuffer = reinterpret_cast<uint32_t*>(malloc( indexBufferSize ));
		ifs.read( reinterpret_cast<char*>(oMesh->indexBuffer), indexBufferSize );

		int vertexBufferSize = sizeof( sVertex ) * oMesh->numVertices;
		oMesh->vertexBuffer = reinterpret_cast<sVertex*>(malloc( vertexBufferSize ));
		ifs.read( reinterpret_cast<char*>(oMesh->vertexBuffer), vertexBufferSize );

		ifs.close();

		return true;
	}

}